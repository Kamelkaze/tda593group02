package se.chalmers.cse.mdsd1617.yakindu;

public class Assignment3Complete {
	
	private class Booking {
		private long[] reservations = {-1,-1};
		private long reservationCount = 0;
	}
	
	public class Reservation {
		private ReservationStatus status;
		
		public Reservation () {
			this.status = ReservationStatus.FREE;
		}
	}
	
	private enum ReservationStatus{
		FREE, ASSIGNED, CHECKEDIN
	}
	
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;
	private final int MAX_ROOMS = 2;
	
	private Booking[] bookings = {new Booking(), new Booking()};
	private Reservation[] reservations = {new Reservation(), new Reservation()};
	
	private ReservationStatus[] reservationStatuses = {ReservationStatus.FREE, ReservationStatus.FREE};
	
	private boolean[] roomsFree = {true,true};
	
	public long initiateBooking() {
		return ++currentBookingId;
	}

	public boolean addRoomToBooking(long bookingId) {
		if (!bookingIdValid(bookingId)) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		} else {
			++currentReservationNumber;
			return true;
		}
	}
	
	public void assignReservationWithId (long bookingNumber) {
		Booking booking = bookings[(int) bookingNumber];
		booking.reservations[(int) booking.reservationCount] = firstAvailableReservation();
		booking.reservationCount++;
	}
	
	private boolean reservationBelongsToBooking (long bookingNumber, long reservationNumber) {
		Booking booking = bookings[(int) bookingNumber];
		for (int i = 0; i < booking.reservations.length; i++) {
			if (booking.reservations[i] == reservationNumber) {
				return true;
			}
		}
		return false;
	}
	
	private long firstAssignedReservation (long bookingNumber) {
		Booking booking = bookings[(int) bookingNumber];
		for (int i = 0; i < booking.reservations.length; i++) {
			if (reservationStatuses[(int) booking.reservations[i]] == ReservationStatus.ASSIGNED){
				return booking.reservations[i];
			} 
		}
		return -1;
	}
	
	private long firstCheckedInReservation (long bookingNumber) {
		Booking booking = bookings[(int) bookingNumber];
		for (int i = 0; i < booking.reservations.length; i++) {
			if (reservationStatuses[(int) booking.reservations[i]] == ReservationStatus.CHECKEDIN){
				return booking.reservations[i];
			} 
		}
		return -1;
	}
	
	public long firstAvailableReservation () {
		for (int i = 0; i < reservations.length; i++) {
			if (reservationStatuses[i] == ReservationStatus.FREE){
				return i;
			} 
		}
		return -1;
	}
	
	public long firstAvailableRoom () {
		for (int i = 0; i < roomsFree.length; i++) {
			if (roomsFree[i]){
				return i;
			} 
		}
		return -1;
	}
	
	public boolean room0assigned() {
		return bookings[1].reservations[1] == 0;
	}
	
	public void assignRoom (long roomNumber) {
		roomsFree[(int) roomNumber] = false;
	}
	
	public void checkOutRoom (long roomNumber) {
		roomsFree[(int) roomNumber] = true;
	}
	
	public boolean confirmBooking(long bookingId) {
		return bookingIdValid(bookingId);
	}
	
	public void confirmReservation(long reservationNumber) {
		reservationStatuses[(int) reservationNumber] = ReservationStatus.ASSIGNED;
	}
	
	public void checkInReservation (long reservationNumber) {
			reservationStatuses[(int) reservationNumber] = ReservationStatus.CHECKEDIN;
	}
	
	public void checkOutReservation (long reservationNumber) {
		currentReservationNumber--;
		reservationStatuses[(int) reservationNumber] = ReservationStatus.FREE;
	}
	
	public boolean reservationCheckedIn (long reservationNumber) {
		return reservationStatuses[(int) reservationNumber] == ReservationStatus.CHECKEDIN;
	}
	
	public boolean reservationCheckedOut (long reservationNumber) {
		return reservationStatuses[(int) reservationNumber] == ReservationStatus.FREE;
	}
	
	public boolean checkInBooking(long bookingId) {
		return bookingIdValid(bookingId);
	}
	
	public boolean intiateCheckout(long bookingId) {
		return bookingIdValid(bookingId);
	}
	
	public boolean payDuringCheckout(long bookingId) {
		return bookingIdValid(bookingId);
	}
	
	private boolean bookingIdValid (long bookingId) {
		if (bookingId < 1 || bookingId > currentBookingId) {
			return false;
		}
		return true;
	}
}
