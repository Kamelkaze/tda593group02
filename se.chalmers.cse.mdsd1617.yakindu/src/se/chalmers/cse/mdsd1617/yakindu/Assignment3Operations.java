package se.chalmers.cse.mdsd1617.yakindu;

public class Assignment3Operations {
	private long counter = 0;
	private boolean toggled = false;
	
	public void increaseCounter() {
		if (!isToggled()) {
			if (counter == 4) {
				counter = 0;
			} else {
				counter++;
			}
		}
	}
	
	public long getCounter() {
		return counter;
	}
	
	public void toggleCounter() {
		toggled = !toggled;
	}
	
	public boolean isToggled() {
		return toggled;
	}
}
