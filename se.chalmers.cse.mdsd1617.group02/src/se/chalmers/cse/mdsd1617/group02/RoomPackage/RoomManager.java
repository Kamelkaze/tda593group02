/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import org.eclipse.emf.common.util.EList;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRoomTypeManager <em>Room Type Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends IRoomAdministrationProvides, IRoomHotelCustomerProvides, IRoomBookingManagerProvides, IRoomStartupProvides, IRoomHotelReceptionistProvides {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoomManager_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Manager</em>' reference.
	 * @see #setRoomTypeManager(IRoomTypeRoomManagerProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoomManager_RoomTypeManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeRoomManagerProvides getRoomTypeManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRoomTypeManager <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Manager</em>' reference.
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	void setRoomTypeManager(IRoomTypeRoomManagerProvides value);

} // RoomManager
