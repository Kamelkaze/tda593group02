/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getIRoomHotelCustomerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomHotelCustomerProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IRoom> getAllRooms();

} // IRoomHotelCustomerProvides
