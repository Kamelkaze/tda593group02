/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.Group02Package;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.Room;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.impl.Group02PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomPackagePackageImpl extends EPackageImpl implements RoomPackagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomAdministrationProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomHotelCustomerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomBookingManagerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomHotelReceptionistProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomStartupProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roomStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomPackagePackageImpl() {
		super(eNS_URI, RoomPackageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomPackagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomPackagePackage init() {
		if (isInited) return (RoomPackagePackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI);

		// Obtain or create and register package
		RoomPackagePackageImpl theRoomPackagePackage = (RoomPackagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomPackagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomPackagePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group02PackageImpl theGroup02Package = (Group02PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) instanceof Group02PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) : Group02Package.eINSTANCE);
		RoomTypePackagePackageImpl theRoomTypePackagePackage = (RoomTypePackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI) instanceof RoomTypePackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI) : RoomTypePackagePackage.eINSTANCE);
		BookingPackagePackageImpl theBookingPackagePackage = (BookingPackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackagePackage.eNS_URI) instanceof BookingPackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackagePackage.eNS_URI) : BookingPackagePackage.eINSTANCE);

		// Create package meta-data objects
		theRoomPackagePackage.createPackageContents();
		theGroup02Package.createPackageContents();
		theRoomTypePackagePackage.createPackageContents();
		theBookingPackagePackage.createPackageContents();

		// Initialize created meta-data
		theRoomPackagePackage.initializePackageContents();
		theGroup02Package.initializePackageContents();
		theRoomTypePackagePackage.initializePackageContents();
		theBookingPackagePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomPackagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomPackagePackage.eNS_URI, theRoomPackagePackage);
		return theRoomPackagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Number() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Status() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_RoomType() {
		return (EReference)roomEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoom() {
		return iRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetNumber() {
		return iRoomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetStatus() {
		return iRoomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetType() {
		return iRoomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomManager() {
		return roomManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomManager_Rooms() {
		return (EReference)roomManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomManager_RoomTypeManager() {
		return (EReference)roomManagerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomAdministrationProvides() {
		return iRoomAdministrationProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__RoomOfRoomTypeExists__String() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__AddRoom__int_String() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__ChangeRoomTypeOfRoom__int_String() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__RemoveRoom__int() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__BlockRoom__int() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomAdministrationProvides__UnblockRoom__int() {
		return iRoomAdministrationProvidesEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomHotelCustomerProvides() {
		return iRoomHotelCustomerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHotelCustomerProvides__GetAllRooms() {
		return iRoomHotelCustomerProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomBookingManagerProvides() {
		return iRoomBookingManagerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingManagerProvides__GetNumberOfBookableRoomsOfType__String() {
		return iRoomBookingManagerProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingManagerProvides__OccupyRoomOfType__String() {
		return iRoomBookingManagerProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingManagerProvides__GetRooms__EList() {
		return iRoomBookingManagerProvidesEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingManagerProvides__OccupyRooms__EList() {
		return iRoomBookingManagerProvidesEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingManagerProvides__MarkRoomAsFree__int() {
		return iRoomBookingManagerProvidesEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomHotelReceptionistProvides() {
		return iRoomHotelReceptionistProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHotelReceptionistProvides__GetFreeRoomsOfType__String() {
		return iRoomHotelReceptionistProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomStartupProvides() {
		return iRoomStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomStartupProvides__RemoveAllRooms() {
		return iRoomStartupProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomStartupProvides__AddRoom__int_String() {
		return iRoomStartupProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoomStatus() {
		return roomStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomPackageFactory getRoomPackageFactory() {
		return (RoomPackageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__NUMBER);
		createEAttribute(roomEClass, ROOM__STATUS);
		createEReference(roomEClass, ROOM__ROOM_TYPE);

		iRoomEClass = createEClass(IROOM);
		createEOperation(iRoomEClass, IROOM___GET_NUMBER);
		createEOperation(iRoomEClass, IROOM___GET_STATUS);
		createEOperation(iRoomEClass, IROOM___GET_TYPE);

		roomManagerEClass = createEClass(ROOM_MANAGER);
		createEReference(roomManagerEClass, ROOM_MANAGER__ROOMS);
		createEReference(roomManagerEClass, ROOM_MANAGER__ROOM_TYPE_MANAGER);

		iRoomAdministrationProvidesEClass = createEClass(IROOM_ADMINISTRATION_PROVIDES);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___ROOM_OF_ROOM_TYPE_EXISTS__STRING);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___ADD_ROOM__INT_STRING);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___REMOVE_ROOM__INT);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___BLOCK_ROOM__INT);
		createEOperation(iRoomAdministrationProvidesEClass, IROOM_ADMINISTRATION_PROVIDES___UNBLOCK_ROOM__INT);

		iRoomHotelCustomerProvidesEClass = createEClass(IROOM_HOTEL_CUSTOMER_PROVIDES);
		createEOperation(iRoomHotelCustomerProvidesEClass, IROOM_HOTEL_CUSTOMER_PROVIDES___GET_ALL_ROOMS);

		iRoomBookingManagerProvidesEClass = createEClass(IROOM_BOOKING_MANAGER_PROVIDES);
		createEOperation(iRoomBookingManagerProvidesEClass, IROOM_BOOKING_MANAGER_PROVIDES___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING);
		createEOperation(iRoomBookingManagerProvidesEClass, IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOM_OF_TYPE__STRING);
		createEOperation(iRoomBookingManagerProvidesEClass, IROOM_BOOKING_MANAGER_PROVIDES___GET_ROOMS__ELIST);
		createEOperation(iRoomBookingManagerProvidesEClass, IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOMS__ELIST);
		createEOperation(iRoomBookingManagerProvidesEClass, IROOM_BOOKING_MANAGER_PROVIDES___MARK_ROOM_AS_FREE__INT);

		iRoomHotelReceptionistProvidesEClass = createEClass(IROOM_HOTEL_RECEPTIONIST_PROVIDES);
		createEOperation(iRoomHotelReceptionistProvidesEClass, IROOM_HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS_OF_TYPE__STRING);

		iRoomStartupProvidesEClass = createEClass(IROOM_STARTUP_PROVIDES);
		createEOperation(iRoomStartupProvidesEClass, IROOM_STARTUP_PROVIDES___REMOVE_ALL_ROOMS);
		createEOperation(iRoomStartupProvidesEClass, IROOM_STARTUP_PROVIDES___ADD_ROOM__INT_STRING);

		// Create enums
		roomStatusEEnum = createEEnum(ROOM_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomTypePackagePackage theRoomTypePackagePackage = (RoomTypePackagePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomEClass.getESuperTypes().add(this.getIRoom());
		roomManagerEClass.getESuperTypes().add(this.getIRoomAdministrationProvides());
		roomManagerEClass.getESuperTypes().add(this.getIRoomHotelCustomerProvides());
		roomManagerEClass.getESuperTypes().add(this.getIRoomBookingManagerProvides());
		roomManagerEClass.getESuperTypes().add(this.getIRoomStartupProvides());
		roomManagerEClass.getESuperTypes().add(this.getIRoomHotelReceptionistProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_Number(), ecorePackage.getEInt(), "number", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_Status(), this.getRoomStatus(), "status", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoom_RoomType(), theRoomTypePackagePackage.getIRoomType(), null, "roomType", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomEClass, IRoom.class, "IRoom", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoom__GetNumber(), ecorePackage.getEInt(), "getNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetStatus(), this.getRoomStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetType(), theRoomTypePackagePackage.getIRoomType(), "getType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomManagerEClass, RoomManager.class, "RoomManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomManager_Rooms(), this.getRoom(), null, "rooms", null, 0, -1, RoomManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomManager_RoomTypeManager(), theRoomTypePackagePackage.getIRoomTypeRoomManagerProvides(), null, "roomTypeManager", null, 1, 1, RoomManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomAdministrationProvidesEClass, IRoomAdministrationProvides.class, "IRoomAdministrationProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIRoomAdministrationProvides__RoomOfRoomTypeExists__String(), ecorePackage.getEBoolean(), "roomOfRoomTypeExists", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "type", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomAdministrationProvides__AddRoom__int_String(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomAdministrationProvides__ChangeRoomTypeOfRoom__int_String(), ecorePackage.getEBoolean(), "changeRoomTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomAdministrationProvides__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomAdministrationProvides__BlockRoom__int(), ecorePackage.getEBoolean(), "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomAdministrationProvides__UnblockRoom__int(), ecorePackage.getEBoolean(), "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomHotelCustomerProvidesEClass, IRoomHotelCustomerProvides.class, "IRoomHotelCustomerProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomHotelCustomerProvides__GetAllRooms(), this.getIRoom(), "getAllRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomBookingManagerProvidesEClass, IRoomBookingManagerProvides.class, "IRoomBookingManagerProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomBookingManagerProvides__GetNumberOfBookableRoomsOfType__String(), ecorePackage.getEInt(), "getNumberOfBookableRoomsOfType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingManagerProvides__OccupyRoomOfType__String(), this.getIRoom(), "occupyRoomOfType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingManagerProvides__GetRooms__EList(), this.getIRoom(), "getRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumbers", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingManagerProvides__OccupyRooms__EList(), ecorePackage.getEBoolean(), "occupyRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumbers", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingManagerProvides__MarkRoomAsFree__int(), ecorePackage.getEBoolean(), "markRoomAsFree", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomHotelReceptionistProvidesEClass, IRoomHotelReceptionistProvides.class, "IRoomHotelReceptionistProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomHotelReceptionistProvides__GetFreeRoomsOfType__String(), this.getIRoom(), "getFreeRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomStartupProvidesEClass, IRoomStartupProvides.class, "IRoomStartupProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomStartupProvides__RemoveAllRooms(), null, "removeAllRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomStartupProvides__AddRoom__int_String(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(roomStatusEEnum, RoomStatus.class, "RoomStatus");
		addEEnumLiteral(roomStatusEEnum, RoomStatus.FREE);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.OCCUPIED);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.BLOCKED);
	}

} //RoomPackagePackageImpl
