/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Booking Manager Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getIRoomBookingManagerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomBookingManagerProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int getNumberOfBookableRoomsOfType(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	IRoom occupyRoomOfType(String roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomNumbersMany="true" roomNumbersOrdered="false"
	 * @generated
	 */
	EList<IRoom> getRooms(EList<Integer> roomNumbers);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumbersMany="true" roomNumbersOrdered="false"
	 * @generated
	 */
	boolean occupyRooms(EList<Integer> roomNumbers);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	boolean markRoomAsFree(int roomNumber);

} // IRoomBookingManagerProvides
