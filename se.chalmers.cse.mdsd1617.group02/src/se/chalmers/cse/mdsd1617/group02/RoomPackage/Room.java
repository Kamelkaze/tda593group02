/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getNumber <em>Number</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getRoomType <em>Room Type</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends IRoom {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoom_Number()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus
	 * @see #setStatus(RoomStatus)
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoom_Status()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomStatus getStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(RoomStatus value);

	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' reference.
	 * @see #setRoomType(IRoomType)
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getRoom_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomType getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getRoomType <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(IRoomType value);

} // Room
