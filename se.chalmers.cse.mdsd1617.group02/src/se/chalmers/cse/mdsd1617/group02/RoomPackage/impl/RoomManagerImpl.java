/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.Room;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl#getRoomTypeManager <em>Room Type Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomManagerImpl extends MinimalEObjectImpl.Container implements RoomManager {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getRoomTypeManager() <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeRoomManagerProvides roomTypeManager;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomManagerImpl(IRoomTypeRoomManagerProvides roomTypeManager) {
		super();
		setRoomTypeManager(roomTypeManager);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackagePackage.Literals.ROOM_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<Room>(Room.class, this, RoomPackagePackage.ROOM_MANAGER__ROOMS);
		}
		return rooms;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Private helper method for getting a room of a room number.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Room getRoom(int roomNumber) {
		EList<Room> rooms = getRooms();
		
		for (Room room : rooms) {
			if (room.getNumber() == roomNumber) {
				return room;
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeRoomManagerProvides getRoomTypeManager() {
		if (roomTypeManager != null && roomTypeManager.eIsProxy()) {
			InternalEObject oldRoomTypeManager = (InternalEObject)roomTypeManager;
			roomTypeManager = (IRoomTypeRoomManagerProvides)eResolveProxy(oldRoomTypeManager);
			if (roomTypeManager != oldRoomTypeManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
			}
		}
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeRoomManagerProvides basicGetRoomTypeManager() {
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeManager(IRoomTypeRoomManagerProvides newRoomTypeManager) {
		IRoomTypeRoomManagerProvides oldRoomTypeManager = roomTypeManager;
		roomTypeManager = newRoomTypeManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns true if there is at least one room of the given room type, otherwise false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean roomOfRoomTypeExists(String type) {
		for (Room room : getRooms()) {
			if (room.getRoomType().getName().equalsIgnoreCase(type)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room with the given room number and room type.
	 * @return false if a room with the specified room number already exists or the given room type doesn't exist,
	 *         otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomNumber, String roomType) {
		Room room = getRoom(roomNumber);
		
		if (room != null) {
			return false;
		}
		
		IRoomType type = roomTypeManager.getRoomType(roomType);
		
		if (type == null) {
			return false;
		}
		
		rooms.add(new RoomImpl(roomNumber, type));
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Changes the room type of a room.
	 * @return false if no room with the given room number exists, or if the given room type doesn't exists,
	 *         otherwise true. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeOfRoom(int roomNumber, String roomType) {
		Room room = getRoom(roomNumber);
		
		if (room == null) {
			return false;
		}
		
		IRoomType type = roomTypeManager.getRoomType(roomType);
		
		if (type == null) {
			return false;
		}
		
		room.setRoomType(type);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes a room.
	 * @return false if no room with the given room number exists, otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		Room room = getRoom(roomNumber);
		
		if (room == null) {
			return false;
		} else if (room.getStatus().equals(RoomStatus.OCCUPIED)){
			return false;
		}
		
		return rooms.remove(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Blocks a room. A room can only be blocked if it is currently marked as FREE.
	 * @return false if no room with the given room number exists, or the room is occupied. Otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNumber) {
		Room room = getRoom(roomNumber);
		
		if (room == null || room.getStatus() != RoomStatus.FREE) {
			return false;
		}
		
		room.setStatus(RoomStatus.BLOCKED);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Unblocks a room. A room can only be unblocked if it is currently marked as BLOCKED.
	 * @return false if no room with the given room number exists, or the room is not currently blocked. Otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNumber) {
		Room room = getRoom(roomNumber);
		
		if (room == null || room.getStatus() != RoomStatus.BLOCKED) {
			return false;
		}
		
		room.setStatus(RoomStatus.FREE);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all added rooms.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> getAllRooms() {
		EList<IRoom> rooms = new BasicEList<>();
		for (Room room : getRooms()) {
			rooms.add(room);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the number of bookable rooms of a given type. Total number of rooms - number of blocked rooms.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNumberOfBookableRoomsOfType(String roomType) {
		int count = 0;
				
		for (Room room : getRooms()) {
			if (room.getRoomType().getName().equalsIgnoreCase(roomType) && !(room.getStatus() == RoomStatus.BLOCKED)) {
				count++;
			}
		}
				
		return count;
	}


	/**
	 * <!-- begin-user-doc -->
	 * Marks a room of the given room type as occupied.
	 * @return the room that was marked, or null if there are no free rooms of the given room type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Room occupyRoomOfType(String roomType) {
		Room room = getFreeRoomOfType(roomType);
		
		if (room == null) {
			return null;
		}
		
		room.setStatus(RoomStatus.OCCUPIED);
		
		return room;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of corresponding rooms for the given room numbers.
	 * @return null if any room in the list of room numbers doesn't exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> getRooms(EList<Integer> roomNumbers) {
		EList<IRoom> foundRooms = new BasicEList<>();
		
		for (int roomNumber : roomNumbers) {
			Room room = getRoom(roomNumber);
			if (room == null) {
				return null;
			}
			foundRooms.add(room);
		}
		
		return foundRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Occupies all the corresponding rooms for the given room numbers. If any room in the list of room numbers
	 * doesn't exist or is not free, none of the rooms will be marked as free.
	 * @return false if any room in the list of room numbers doesn't exist or is not free, otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean occupyRooms(EList<Integer> roomNumbers) {
		EList<Room> rooms = new BasicEList<>();
		
		for (int roomNumber : roomNumbers) {
			Room room = getRoom(roomNumber);
			
			if (room == null || room.getStatus() != RoomStatus.FREE) {
				return false;
			}
			
			rooms.add(room);
		}
		
		for (Room room : rooms) {
			room.setStatus(RoomStatus.OCCUPIED);
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the first free room of the given room type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Room getFreeRoomOfType(String roomType) {
		for (Room room : getRooms()) {
			if (room.getRoomType().getName().equalsIgnoreCase(roomType)
					&& room.getStatus() == RoomStatus.FREE) {
				return room;
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Marks a room with a given room number as free.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean markRoomAsFree(int roomNumber) {
		Room room = getRoom(roomNumber);
		
		if (room == null || room.getStatus() != RoomStatus.OCCUPIED) {
			return false;
		}
		
		room.setStatus(RoomStatus.FREE);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes all added rooms.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeAllRooms() {
		getRooms().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all free rooms of a given room type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> getFreeRoomsOfType(String roomType) {
		EList<IRoom> rooms = new BasicEList<>();
		
		for (Room room : getRooms()) {
			if (room.getRoomType().getName().equalsIgnoreCase(roomType)
					&& room.getStatus() == RoomStatus.FREE) {
				rooms.add(room);
			}
		}
		
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackagePackage.ROOM_MANAGER__ROOMS:
				return getRooms();
			case RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER:
				if (resolve) return getRoomTypeManager();
				return basicGetRoomTypeManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackagePackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeRoomManagerProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackagePackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				return;
			case RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeRoomManagerProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackagePackage.ROOM_MANAGER__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case RoomPackagePackage.ROOM_MANAGER__ROOM_TYPE_MANAGER:
				return roomTypeManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IRoomHotelCustomerProvides.class) {
			switch (baseOperationID) {
				case RoomPackagePackage.IROOM_HOTEL_CUSTOMER_PROVIDES___GET_ALL_ROOMS: return RoomPackagePackage.ROOM_MANAGER___GET_ALL_ROOMS;
				default: return -1;
			}
		}
		if (baseClass == IRoomBookingManagerProvides.class) {
			switch (baseOperationID) {
				case RoomPackagePackage.IROOM_BOOKING_MANAGER_PROVIDES___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING: return RoomPackagePackage.ROOM_MANAGER___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING;
				case RoomPackagePackage.IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOM_OF_TYPE__STRING: return RoomPackagePackage.ROOM_MANAGER___OCCUPY_ROOM_OF_TYPE__STRING;
				case RoomPackagePackage.IROOM_BOOKING_MANAGER_PROVIDES___GET_ROOMS__ELIST: return RoomPackagePackage.ROOM_MANAGER___GET_ROOMS__ELIST;
				case RoomPackagePackage.IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOMS__ELIST: return RoomPackagePackage.ROOM_MANAGER___OCCUPY_ROOMS__ELIST;
				case RoomPackagePackage.IROOM_BOOKING_MANAGER_PROVIDES___MARK_ROOM_AS_FREE__INT: return RoomPackagePackage.ROOM_MANAGER___MARK_ROOM_AS_FREE__INT;
				default: return -1;
			}
		}
		if (baseClass == IRoomStartupProvides.class) {
			switch (baseOperationID) {
				case RoomPackagePackage.IROOM_STARTUP_PROVIDES___REMOVE_ALL_ROOMS: return RoomPackagePackage.ROOM_MANAGER___REMOVE_ALL_ROOMS;
				case RoomPackagePackage.IROOM_STARTUP_PROVIDES___ADD_ROOM__INT_STRING: return RoomPackagePackage.ROOM_MANAGER___ADD_ROOM__INT_STRING_1;
				default: return -1;
			}
		}
		if (baseClass == IRoomHotelReceptionistProvides.class) {
			switch (baseOperationID) {
				case RoomPackagePackage.IROOM_HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS_OF_TYPE__STRING: return RoomPackagePackage.ROOM_MANAGER___GET_FREE_ROOMS_OF_TYPE__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackagePackage.ROOM_MANAGER___ROOM_OF_ROOM_TYPE_EXISTS__STRING:
				return roomOfRoomTypeExists((String)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___ADD_ROOM__INT_STRING_1:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackagePackage.ROOM_MANAGER___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING:
				return changeRoomTypeOfRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case RoomPackagePackage.ROOM_MANAGER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___GET_ALL_ROOMS:
				return getAllRooms();
			case RoomPackagePackage.ROOM_MANAGER___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING:
				return getNumberOfBookableRoomsOfType((String)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___OCCUPY_ROOM_OF_TYPE__STRING:
				return occupyRoomOfType((String)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___GET_ROOMS__ELIST:
				return getRooms((EList<Integer>)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___OCCUPY_ROOMS__ELIST:
				return occupyRooms((EList<Integer>)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___MARK_ROOM_AS_FREE__INT:
				return markRoomAsFree((Integer)arguments.get(0));
			case RoomPackagePackage.ROOM_MANAGER___REMOVE_ALL_ROOMS:
				removeAllRooms();
				return null;
			case RoomPackagePackage.ROOM_MANAGER___GET_FREE_ROOMS_OF_TYPE__STRING:
				return getFreeRoomsOfType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomManagerImpl
