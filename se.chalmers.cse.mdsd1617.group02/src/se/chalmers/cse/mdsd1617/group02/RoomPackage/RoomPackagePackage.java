/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RoomPackage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group02/RoomPackage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group02.RoomPackage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackagePackage eINSTANCE = se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom <em>IRoom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoom()
	 * @generated
	 */
	int IROOM = 1;

	/**
	 * The number of structural features of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_NUMBER = 0;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_STATUS = 1;

	/**
	 * The operation id for the '<em>Get Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_TYPE = 2;

	/**
	 * The number of operations of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 0;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NUMBER = IROOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__STATUS = IROOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_TYPE = IROOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = IROOM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_NUMBER = IROOM___GET_NUMBER;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_STATUS = IROOM___GET_STATUS;

	/**
	 * The operation id for the '<em>Get Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_TYPE = IROOM___GET_TYPE;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = IROOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides <em>IRoom Administration Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomAdministrationProvides()
	 * @generated
	 */
	int IROOM_ADMINISTRATION_PROVIDES = 3;

	/**
	 * The number of structural features of the '<em>IRoom Administration Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Room Of Room Type Exists</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___ROOM_OF_ROOM_TYPE_EXISTS__STRING = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___ADD_ROOM__INT_STRING = 1;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___REMOVE_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___BLOCK_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES___UNBLOCK_ROOM__INT = 5;

	/**
	 * The number of operations of the '<em>IRoom Administration Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl <em>Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 2;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOMS = IROOM_ADMINISTRATION_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOM_TYPE_MANAGER = IROOM_ADMINISTRATION_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = IROOM_ADMINISTRATION_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Room Of Room Type Exists</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ROOM_OF_ROOM_TYPE_EXISTS__STRING = IROOM_ADMINISTRATION_PROVIDES___ROOM_OF_ROOM_TYPE_EXISTS__STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_STRING = IROOM_ADMINISTRATION_PROVIDES___ADD_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = IROOM_ADMINISTRATION_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM__INT = IROOM_ADMINISTRATION_PROVIDES___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__INT = IROOM_ADMINISTRATION_PROVIDES___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__INT = IROOM_ADMINISTRATION_PROVIDES___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ALL_ROOMS = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Number Of Bookable Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Occupy Room Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___OCCUPY_ROOM_OF_TYPE__STRING = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ROOMS__ELIST = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Occupy Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___OCCUPY_ROOMS__ELIST = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Mark Room As Free</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___MARK_ROOM_AS_FREE__INT = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Remove All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ALL_ROOMS = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_STRING_1 = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_FREE_ROOMS_OF_TYPE__STRING = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = IROOM_ADMINISTRATION_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides <em>IRoom Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomHotelCustomerProvides()
	 * @generated
	 */
	int IROOM_HOTEL_CUSTOMER_PROVIDES = 4;

	/**
	 * The number of structural features of the '<em>IRoom Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_CUSTOMER_PROVIDES___GET_ALL_ROOMS = 0;

	/**
	 * The number of operations of the '<em>IRoom Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides <em>IRoom Booking Manager Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomBookingManagerProvides()
	 * @generated
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES = 5;

	/**
	 * The number of structural features of the '<em>IRoom Booking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Number Of Bookable Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING = 0;

	/**
	 * The operation id for the '<em>Occupy Room Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOM_OF_TYPE__STRING = 1;

	/**
	 * The operation id for the '<em>Get Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES___GET_ROOMS__ELIST = 2;

	/**
	 * The operation id for the '<em>Occupy Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOMS__ELIST = 3;

	/**
	 * The operation id for the '<em>Mark Room As Free</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES___MARK_ROOM_AS_FREE__INT = 4;

	/**
	 * The number of operations of the '<em>IRoom Booking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_MANAGER_PROVIDES_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides <em>IRoom Hotel Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomHotelReceptionistProvides()
	 * @generated
	 */
	int IROOM_HOTEL_RECEPTIONIST_PROVIDES = 6;

	/**
	 * The number of structural features of the '<em>IRoom Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS_OF_TYPE__STRING = 0;

	/**
	 * The number of operations of the '<em>IRoom Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides <em>IRoom Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomStartupProvides()
	 * @generated
	 */
	int IROOM_STARTUP_PROVIDES = 7;

	/**
	 * The number of structural features of the '<em>IRoom Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Remove All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_STARTUP_PROVIDES___REMOVE_ALL_ROOMS = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_STARTUP_PROVIDES___ADD_ROOM__INT_STRING = 1;

	/**
	 * The number of operations of the '<em>IRoom Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_STARTUP_PROVIDES_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus <em>Room Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoomStatus()
	 * @generated
	 */
	int ROOM_STATUS = 8;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getNumber()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Number();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Status();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.Room#getRoomType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_RoomType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom <em>IRoom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom
	 * @generated
	 */
	EClass getIRoom();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getNumber() <em>Get Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getNumber()
	 * @generated
	 */
	EOperation getIRoom__GetNumber();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getStatus() <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getStatus()
	 * @generated
	 */
	EOperation getIRoom__GetStatus();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getType() <em>Get Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom#getType()
	 * @generated
	 */
	EOperation getIRoom__GetType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRooms()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_Rooms();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRoomTypeManager <em>Room Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager#getRoomTypeManager()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_RoomTypeManager();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides <em>IRoom Administration Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Administration Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides
	 * @generated
	 */
	EClass getIRoomAdministrationProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#roomOfRoomTypeExists(java.lang.String) <em>Room Of Room Type Exists</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Room Of Room Type Exists</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#roomOfRoomTypeExists(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__RoomOfRoomTypeExists__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#changeRoomTypeOfRoom(int, java.lang.String) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#changeRoomTypeOfRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__ChangeRoomTypeOfRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomAdministrationProvides__UnblockRoom__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides <em>IRoom Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Hotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides
	 * @generated
	 */
	EClass getIRoomHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides#getAllRooms() <em>Get All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides#getAllRooms()
	 * @generated
	 */
	EOperation getIRoomHotelCustomerProvides__GetAllRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides <em>IRoom Booking Manager Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Booking Manager Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides
	 * @generated
	 */
	EClass getIRoomBookingManagerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#getNumberOfBookableRoomsOfType(java.lang.String) <em>Get Number Of Bookable Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Bookable Rooms Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#getNumberOfBookableRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomBookingManagerProvides__GetNumberOfBookableRoomsOfType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#occupyRoomOfType(java.lang.String) <em>Occupy Room Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy Room Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#occupyRoomOfType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomBookingManagerProvides__OccupyRoomOfType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#getRooms(org.eclipse.emf.common.util.EList) <em>Get Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#getRooms(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomBookingManagerProvides__GetRooms__EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#occupyRooms(org.eclipse.emf.common.util.EList) <em>Occupy Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#occupyRooms(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomBookingManagerProvides__OccupyRooms__EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#markRoomAsFree(int) <em>Mark Room As Free</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Mark Room As Free</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides#markRoomAsFree(int)
	 * @generated
	 */
	EOperation getIRoomBookingManagerProvides__MarkRoomAsFree__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides <em>IRoom Hotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Hotel Receptionist Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides
	 * @generated
	 */
	EClass getIRoomHotelReceptionistProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides#getFreeRoomsOfType(java.lang.String) <em>Get Free Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides#getFreeRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomHotelReceptionistProvides__GetFreeRoomsOfType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides <em>IRoom Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides
	 * @generated
	 */
	EClass getIRoomStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides#removeAllRooms() <em>Remove All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides#removeAllRooms()
	 * @generated
	 */
	EOperation getIRoomStartupProvides__RemoveAllRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomStartupProvides__AddRoom__int_String();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus <em>Room Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Room Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus
	 * @generated
	 */
	EEnum getRoomStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomPackageFactory getRoomPackageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__NUMBER = eINSTANCE.getRoom_Number();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__STATUS = eINSTANCE.getRoom_Status();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOM_TYPE = eINSTANCE.getRoom_RoomType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom <em>IRoom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoom()
		 * @generated
		 */
		EClass IROOM = eINSTANCE.getIRoom();

		/**
		 * The meta object literal for the '<em><b>Get Number</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_NUMBER = eINSTANCE.getIRoom__GetNumber();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_STATUS = eINSTANCE.getIRoom__GetStatus();

		/**
		 * The meta object literal for the '<em><b>Get Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_TYPE = eINSTANCE.getIRoom__GetType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl <em>Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOMS = eINSTANCE.getRoomManager_Rooms();

		/**
		 * The meta object literal for the '<em><b>Room Type Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOM_TYPE_MANAGER = eINSTANCE.getRoomManager_RoomTypeManager();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides <em>IRoom Administration Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomAdministrationProvides()
		 * @generated
		 */
		EClass IROOM_ADMINISTRATION_PROVIDES = eINSTANCE.getIRoomAdministrationProvides();

		/**
		 * The meta object literal for the '<em><b>Room Of Room Type Exists</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___ROOM_OF_ROOM_TYPE_EXISTS__STRING = eINSTANCE.getIRoomAdministrationProvides__RoomOfRoomTypeExists__String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___ADD_ROOM__INT_STRING = eINSTANCE.getIRoomAdministrationProvides__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = eINSTANCE.getIRoomAdministrationProvides__ChangeRoomTypeOfRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___REMOVE_ROOM__INT = eINSTANCE.getIRoomAdministrationProvides__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___BLOCK_ROOM__INT = eINSTANCE.getIRoomAdministrationProvides__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ADMINISTRATION_PROVIDES___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomAdministrationProvides__UnblockRoom__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides <em>IRoom Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomHotelCustomerProvides()
		 * @generated
		 */
		EClass IROOM_HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIRoomHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HOTEL_CUSTOMER_PROVIDES___GET_ALL_ROOMS = eINSTANCE.getIRoomHotelCustomerProvides__GetAllRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides <em>IRoom Booking Manager Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomBookingManagerProvides()
		 * @generated
		 */
		EClass IROOM_BOOKING_MANAGER_PROVIDES = eINSTANCE.getIRoomBookingManagerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Bookable Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_MANAGER_PROVIDES___GET_NUMBER_OF_BOOKABLE_ROOMS_OF_TYPE__STRING = eINSTANCE.getIRoomBookingManagerProvides__GetNumberOfBookableRoomsOfType__String();

		/**
		 * The meta object literal for the '<em><b>Occupy Room Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOM_OF_TYPE__STRING = eINSTANCE.getIRoomBookingManagerProvides__OccupyRoomOfType__String();

		/**
		 * The meta object literal for the '<em><b>Get Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_MANAGER_PROVIDES___GET_ROOMS__ELIST = eINSTANCE.getIRoomBookingManagerProvides__GetRooms__EList();

		/**
		 * The meta object literal for the '<em><b>Occupy Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_MANAGER_PROVIDES___OCCUPY_ROOMS__ELIST = eINSTANCE.getIRoomBookingManagerProvides__OccupyRooms__EList();

		/**
		 * The meta object literal for the '<em><b>Mark Room As Free</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_MANAGER_PROVIDES___MARK_ROOM_AS_FREE__INT = eINSTANCE.getIRoomBookingManagerProvides__MarkRoomAsFree__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides <em>IRoom Hotel Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomHotelReceptionistProvides()
		 * @generated
		 */
		EClass IROOM_HOTEL_RECEPTIONIST_PROVIDES = eINSTANCE.getIRoomHotelReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS_OF_TYPE__STRING = eINSTANCE.getIRoomHotelReceptionistProvides__GetFreeRoomsOfType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides <em>IRoom Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getIRoomStartupProvides()
		 * @generated
		 */
		EClass IROOM_STARTUP_PROVIDES = eINSTANCE.getIRoomStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Remove All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_STARTUP_PROVIDES___REMOVE_ALL_ROOMS = eINSTANCE.getIRoomStartupProvides__RemoveAllRooms();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_STARTUP_PROVIDES___ADD_ROOM__INT_STRING = eINSTANCE.getIRoomStartupProvides__AddRoom__int_String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus <em>Room Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus
		 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl#getRoomStatus()
		 * @generated
		 */
		EEnum ROOM_STATUS = eINSTANCE.getRoomStatus();

	}

} //RoomPackagePackage
