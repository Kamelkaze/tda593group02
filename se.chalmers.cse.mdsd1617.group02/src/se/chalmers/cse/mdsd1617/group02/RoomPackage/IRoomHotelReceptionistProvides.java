/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage#getIRoomHotelReceptionistProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomHotelReceptionistProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	EList<IRoom> getFreeRoomsOfType(String roomType);

} // IRoomHotelReceptionistProvides
