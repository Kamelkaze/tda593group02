/**
 */
package se.chalmers.cse.mdsd1617.group02;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides#getRoomManager <em>Room Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelAdministratorProvides()
 * @model
 * @generated
 */
public interface HotelAdministratorProvides extends IHotelAdministratorProvides {
	/**
	 * Returns the value of the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Manager</em>' reference.
	 * @see #setRoomTypeManager(IRoomTypeAdministratorProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelAdministratorProvides_RoomTypeManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeAdministratorProvides getRoomTypeManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides#getRoomTypeManager <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Manager</em>' reference.
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	void setRoomTypeManager(IRoomTypeAdministratorProvides value);

	/**
	 * Returns the value of the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Manager</em>' reference.
	 * @see #setRoomManager(IRoomAdministrationProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelAdministratorProvides_RoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomAdministrationProvides getRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides#getRoomManager <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Manager</em>' reference.
	 * @see #getRoomManager()
	 * @generated
	 */
	void setRoomManager(IRoomAdministrationProvides value);

} // HotelAdministratorProvides
