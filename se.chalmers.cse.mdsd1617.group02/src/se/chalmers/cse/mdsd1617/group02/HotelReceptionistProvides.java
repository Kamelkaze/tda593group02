/**
 */
package se.chalmers.cse.mdsd1617.group02;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelReceptionistProvides()
 * @model
 * @generated
 */
public interface HotelReceptionistProvides extends HotelCustomerProvides, IHotelReceptionistProvides {
	/**
	 * Returns the value of the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Manager</em>' reference.
	 * @see #setRoomManager(IRoomHotelReceptionistProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelReceptionistProvides_RoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomHotelReceptionistProvides getRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides#getRoomManager <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Manager</em>' reference.
	 * @see #getRoomManager()
	 * @generated
	 */
	void setRoomManager(IRoomHotelReceptionistProvides value);

	/**
	 * Returns the value of the '<em><b>Booking Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Manager</em>' reference.
	 * @see #setBookingManager(IBookingReceptionistProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelReceptionistProvides_BookingManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingReceptionistProvides getBookingManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides#getBookingManager <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Manager</em>' reference.
	 * @see #getBookingManager()
	 * @generated
	 */
	void setBookingManager(IBookingReceptionistProvides value);

} // HotelReceptionistProvides
