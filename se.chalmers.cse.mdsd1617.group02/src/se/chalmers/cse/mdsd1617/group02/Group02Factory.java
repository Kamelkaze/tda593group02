/**
 */
package se.chalmers.cse.mdsd1617.group02;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package
 * @generated
 */
public interface Group02Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group02Factory eINSTANCE = se.chalmers.cse.mdsd1617.group02.impl.Group02FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Hotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Startup Provides</em>'.
	 * @generated
	 */
	HotelStartupProvides createHotelStartupProvides();

	/**
	 * Returns a new object of class '<em>Hotel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel</em>'.
	 * @generated
	 */
	Hotel createHotel();

	/**
	 * Returns a new object of class '<em>Room Type DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Room Type DTO</em>'.
	 * @generated
	 */
	RoomTypeDTO createRoomTypeDTO();

	/**
	 * Returns a new object of class '<em>Free Room Types DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Free Room Types DTO</em>'.
	 * @generated
	 */
	FreeRoomTypesDTO createFreeRoomTypesDTO();

	/**
	 * Returns a new object of class '<em>Available Room DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Available Room DTO</em>'.
	 * @generated
	 */
	AvailableRoomDTO createAvailableRoomDTO();

	/**
	 * Returns a new object of class '<em>Booking DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Booking DTO</em>'.
	 * @generated
	 */
	BookingDTO createBookingDTO();

	/**
	 * Returns a new object of class '<em>Booking Room Reservations DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Booking Room Reservations DTO</em>'.
	 * @generated
	 */
	BookingRoomReservationsDTO createBookingRoomReservationsDTO();

	/**
	 * Returns a new object of class '<em>Hotel Receptionist Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Receptionist Provides</em>'.
	 * @generated
	 */
	HotelReceptionistProvides createHotelReceptionistProvides();

	/**
	 * Returns a new object of class '<em>Hotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Customer Provides</em>'.
	 * @generated
	 */
	HotelCustomerProvides createHotelCustomerProvides();

	/**
	 * Returns a new object of class '<em>Hotel Administrator Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Administrator Provides</em>'.
	 * @generated
	 */
	HotelAdministratorProvides createHotelAdministratorProvides();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Group02Package getGroup02Package();

} //Group02Factory
