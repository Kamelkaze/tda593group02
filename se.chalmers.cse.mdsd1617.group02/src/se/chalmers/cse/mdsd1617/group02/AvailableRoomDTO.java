/**
 */
package se.chalmers.cse.mdsd1617.group02;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Available Room DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.AvailableRoomDTO#getRoomNumber <em>Room Number</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.AvailableRoomDTO#getRoomType <em>Room Type</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getAvailableRoomDTO()
 * @model
 * @generated
 */
public interface AvailableRoomDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Number</em>' attribute.
	 * @see #setRoomNumber(int)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getAvailableRoomDTO_RoomNumber()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomNumber();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.AvailableRoomDTO#getRoomNumber <em>Room Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Number</em>' attribute.
	 * @see #getRoomNumber()
	 * @generated
	 */
	void setRoomNumber(int value);

	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' attribute.
	 * @see #setRoomType(String)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getAvailableRoomDTO_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.AvailableRoomDTO#getRoomType <em>Room Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' attribute.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(String value);

} // AvailableRoomDTO
