/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.Group02Package;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage;

import se.chalmers.cse.mdsd1617.group02.impl.Group02PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypePackagePackageImpl extends EPackageImpl implements RoomTypePackagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeRoomManagerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeBookingManagerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeAdministratorProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeStartupProvidesEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomTypePackagePackageImpl() {
		super(eNS_URI, RoomTypePackageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomTypePackagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomTypePackagePackage init() {
		if (isInited) return (RoomTypePackagePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI);

		// Obtain or create and register package
		RoomTypePackagePackageImpl theRoomTypePackagePackage = (RoomTypePackagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomTypePackagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomTypePackagePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group02PackageImpl theGroup02Package = (Group02PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) instanceof Group02PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) : Group02Package.eINSTANCE);
		RoomPackagePackageImpl theRoomPackagePackage = (RoomPackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI) instanceof RoomPackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI) : RoomPackagePackage.eINSTANCE);
		BookingPackagePackageImpl theBookingPackagePackage = (BookingPackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackagePackage.eNS_URI) instanceof BookingPackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackagePackage.eNS_URI) : BookingPackagePackage.eINSTANCE);

		// Create package meta-data objects
		theRoomTypePackagePackage.createPackageContents();
		theGroup02Package.createPackageContents();
		theRoomPackagePackage.createPackageContents();
		theBookingPackagePackage.createPackageContents();

		// Initialize created meta-data
		theRoomTypePackagePackage.initializePackageContents();
		theGroup02Package.initializePackageContents();
		theRoomPackagePackage.initializePackageContents();
		theBookingPackagePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomTypePackagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomTypePackagePackage.eNS_URI, theRoomTypePackagePackage);
		return theRoomTypePackagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Name() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_NumberOfBeds() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Price() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Features() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomType__SetFeatures__EList() {
		return roomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomType() {
		return iRoomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetName() {
		return iRoomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetFeatures() {
		return iRoomTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetNumberOfBeds() {
		return iRoomTypeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetPrice() {
		return iRoomTypeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeManager() {
		return roomTypeManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeManager_RoomTypes() {
		return (EReference)roomTypeManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeRoomManagerProvides() {
		return iRoomTypeRoomManagerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeRoomManagerProvides__GetRoomType__String() {
		return iRoomTypeRoomManagerProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeBookingManagerProvides() {
		return iRoomTypeBookingManagerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeBookingManagerProvides__GetRoomType__String() {
		return iRoomTypeBookingManagerProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeAdministratorProvides() {
		return iRoomTypeAdministratorProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeAdministratorProvides__AddRoomType__String_int_double_EList() {
		return iRoomTypeAdministratorProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeAdministratorProvides__GetAllRoomTypes() {
		return iRoomTypeAdministratorProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeAdministratorProvides__UpdateRoomType__String_String_int_double_EList() {
		return iRoomTypeAdministratorProvidesEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeAdministratorProvides__RemoveRoomType__String() {
		return iRoomTypeAdministratorProvidesEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeStartupProvides() {
		return iRoomTypeStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeStartupProvides__RemoveAllRoomTypes() {
		return iRoomTypeStartupProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeStartupProvides__AddRoomType__String_int_double_EList() {
		return iRoomTypeStartupProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackageFactory getRoomTypePackageFactory() {
		return (RoomTypePackageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomTypeEClass = createEClass(ROOM_TYPE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NAME);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NUMBER_OF_BEDS);
		createEAttribute(roomTypeEClass, ROOM_TYPE__PRICE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__FEATURES);
		createEOperation(roomTypeEClass, ROOM_TYPE___SET_FEATURES__ELIST);

		iRoomTypeEClass = createEClass(IROOM_TYPE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_NAME);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_FEATURES);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_NUMBER_OF_BEDS);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_PRICE);

		roomTypeManagerEClass = createEClass(ROOM_TYPE_MANAGER);
		createEReference(roomTypeManagerEClass, ROOM_TYPE_MANAGER__ROOM_TYPES);

		iRoomTypeRoomManagerProvidesEClass = createEClass(IROOM_TYPE_ROOM_MANAGER_PROVIDES);
		createEOperation(iRoomTypeRoomManagerProvidesEClass, IROOM_TYPE_ROOM_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING);

		iRoomTypeBookingManagerProvidesEClass = createEClass(IROOM_TYPE_BOOKING_MANAGER_PROVIDES);
		createEOperation(iRoomTypeBookingManagerProvidesEClass, IROOM_TYPE_BOOKING_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING);

		iRoomTypeAdministratorProvidesEClass = createEClass(IROOM_TYPE_ADMINISTRATOR_PROVIDES);
		createEOperation(iRoomTypeAdministratorProvidesEClass, IROOM_TYPE_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST);
		createEOperation(iRoomTypeAdministratorProvidesEClass, IROOM_TYPE_ADMINISTRATOR_PROVIDES___GET_ALL_ROOM_TYPES);
		createEOperation(iRoomTypeAdministratorProvidesEClass, IROOM_TYPE_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST);
		createEOperation(iRoomTypeAdministratorProvidesEClass, IROOM_TYPE_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING);

		iRoomTypeStartupProvidesEClass = createEClass(IROOM_TYPE_STARTUP_PROVIDES);
		createEOperation(iRoomTypeStartupProvidesEClass, IROOM_TYPE_STARTUP_PROVIDES___REMOVE_ALL_ROOM_TYPES);
		createEOperation(iRoomTypeStartupProvidesEClass, IROOM_TYPE_STARTUP_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomTypeEClass.getESuperTypes().add(this.getIRoomType());
		roomTypeManagerEClass.getESuperTypes().add(this.getIRoomTypeRoomManagerProvides());
		roomTypeManagerEClass.getESuperTypes().add(this.getIRoomTypeStartupProvides());
		roomTypeManagerEClass.getESuperTypes().add(this.getIRoomTypeBookingManagerProvides());
		roomTypeManagerEClass.getESuperTypes().add(this.getIRoomTypeAdministratorProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomType_Name(), ecorePackage.getEString(), "name", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_NumberOfBeds(), ecorePackage.getEInt(), "numberOfBeds", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Features(), ecorePackage.getEString(), "features", null, 0, -1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		EOperation op = initEOperation(getRoomType__SetFeatures__EList(), null, "setFeatures", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeEClass, IRoomType.class, "IRoomType", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomType__GetName(), ecorePackage.getEString(), "getName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetFeatures(), ecorePackage.getEString(), "getFeatures", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetNumberOfBeds(), ecorePackage.getEInt(), "getNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetPrice(), ecorePackage.getEDouble(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomTypeManagerEClass, RoomTypeManager.class, "RoomTypeManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeManager_RoomTypes(), this.getRoomType(), null, "roomTypes", null, 0, -1, RoomTypeManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomTypeRoomManagerProvidesEClass, IRoomTypeRoomManagerProvides.class, "IRoomTypeRoomManagerProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomTypeRoomManagerProvides__GetRoomType__String(), this.getIRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeBookingManagerProvidesEClass, IRoomTypeBookingManagerProvides.class, "IRoomTypeBookingManagerProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomTypeBookingManagerProvides__GetRoomType__String(), this.getIRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeAdministratorProvidesEClass, IRoomTypeAdministratorProvides.class, "IRoomTypeAdministratorProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomTypeAdministratorProvides__AddRoomType__String_int_double_EList(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTypeAdministratorProvides__GetAllRoomTypes(), this.getIRoomType(), "getAllRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeAdministratorProvides__UpdateRoomType__String_String_int_double_EList(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeAdministratorProvides__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeStartupProvidesEClass, IRoomTypeStartupProvides.class, "IRoomTypeStartupProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomTypeStartupProvides__RemoveAllRoomTypes(), null, "removeAllRoomTypes", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeStartupProvides__AddRoomType__String_int_double_EList(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);
	}

} //RoomTypePackagePackageImpl
