/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage
 * @generated
 */
public class RoomTypePackageSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RoomTypePackagePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackageSwitch() {
		if (modelPackage == null) {
			modelPackage = RoomTypePackagePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RoomTypePackagePackage.ROOM_TYPE: {
				RoomType roomType = (RoomType)theEObject;
				T result = caseRoomType(roomType);
				if (result == null) result = caseIRoomType(roomType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.IROOM_TYPE: {
				IRoomType iRoomType = (IRoomType)theEObject;
				T result = caseIRoomType(iRoomType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER: {
				RoomTypeManager roomTypeManager = (RoomTypeManager)theEObject;
				T result = caseRoomTypeManager(roomTypeManager);
				if (result == null) result = caseIRoomTypeRoomManagerProvides(roomTypeManager);
				if (result == null) result = caseIRoomTypeStartupProvides(roomTypeManager);
				if (result == null) result = caseIRoomTypeBookingManagerProvides(roomTypeManager);
				if (result == null) result = caseIRoomTypeAdministratorProvides(roomTypeManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.IROOM_TYPE_ROOM_MANAGER_PROVIDES: {
				IRoomTypeRoomManagerProvides iRoomTypeRoomManagerProvides = (IRoomTypeRoomManagerProvides)theEObject;
				T result = caseIRoomTypeRoomManagerProvides(iRoomTypeRoomManagerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.IROOM_TYPE_BOOKING_MANAGER_PROVIDES: {
				IRoomTypeBookingManagerProvides iRoomTypeBookingManagerProvides = (IRoomTypeBookingManagerProvides)theEObject;
				T result = caseIRoomTypeBookingManagerProvides(iRoomTypeBookingManagerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.IROOM_TYPE_ADMINISTRATOR_PROVIDES: {
				IRoomTypeAdministratorProvides iRoomTypeAdministratorProvides = (IRoomTypeAdministratorProvides)theEObject;
				T result = caseIRoomTypeAdministratorProvides(iRoomTypeAdministratorProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RoomTypePackagePackage.IROOM_TYPE_STARTUP_PROVIDES: {
				IRoomTypeStartupProvides iRoomTypeStartupProvides = (IRoomTypeStartupProvides)theEObject;
				T result = caseIRoomTypeStartupProvides(iRoomTypeStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomType(RoomType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomType(IRoomType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Type Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Type Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomTypeManager(RoomTypeManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Type Room Manager Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Type Room Manager Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomTypeRoomManagerProvides(IRoomTypeRoomManagerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Type Booking Manager Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Type Booking Manager Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomTypeBookingManagerProvides(IRoomTypeBookingManagerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Type Administrator Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Type Administrator Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomTypeAdministratorProvides(IRoomTypeAdministratorProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Type Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Type Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomTypeStartupProvides(IRoomTypeStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RoomTypePackageSwitch
