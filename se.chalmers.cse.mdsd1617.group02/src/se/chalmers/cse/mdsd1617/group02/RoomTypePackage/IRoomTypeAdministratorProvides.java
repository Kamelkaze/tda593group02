/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage#getIRoomTypeAdministratorProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeAdministratorProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" numberOfBedsRequired="true" numberOfBedsOrdered="false" priceRequired="true" priceOrdered="false" featuresMany="true" featuresOrdered="false"
	 * @generated
	 */
	boolean addRoomType(String name, int numberOfBeds, double price, EList<String> features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IRoomType> getAllRoomTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" newNameRequired="true" newNameOrdered="false" numberOfBedsRequired="true" numberOfBedsOrdered="false" priceRequired="true" priceOrdered="false" featuresMany="true" featuresOrdered="false"
	 * @generated
	 */
	boolean updateRoomType(String name, String newName, int numberOfBeds, double price, EList<String> features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	boolean removeRoomType(String name);

} // IRoomTypeAdministratorProvides
