/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Booking Manager Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage#getIRoomTypeBookingManagerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeBookingManagerProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	IRoomType getRoomType(String name);

} // IRoomTypeBookingManagerProvides
