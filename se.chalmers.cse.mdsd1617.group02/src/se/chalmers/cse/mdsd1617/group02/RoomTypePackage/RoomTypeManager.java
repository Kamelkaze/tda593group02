/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Type Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage#getRoomTypeManager()
 * @model
 * @generated
 */
public interface RoomTypeManager extends IRoomTypeRoomManagerProvides, IRoomTypeStartupProvides, IRoomTypeBookingManagerProvides, IRoomTypeAdministratorProvides {
	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage#getRoomTypeManager_RoomTypes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

} // RoomTypeManager
