/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory
 * @model kind="package"
 * @generated
 */
public interface RoomTypePackagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RoomTypePackage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group02/RoomTypePackage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group02.RoomTypePackage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomTypePackagePackage eINSTANCE = se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType <em>IRoom Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomType()
	 * @generated
	 */
	int IROOM_TYPE = 1;

	/**
	 * The number of structural features of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NAME = 0;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_FEATURES = 1;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NUMBER_OF_BEDS = 2;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_PRICE = 3;

	/**
	 * The number of operations of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = IROOM_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUMBER_OF_BEDS = IROOM_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = IROOM_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Features</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__FEATURES = IROOM_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = IROOM_TYPE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NAME = IROOM_TYPE___GET_NAME;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_FEATURES = IROOM_TYPE___GET_FEATURES;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NUMBER_OF_BEDS = IROOM_TYPE___GET_NUMBER_OF_BEDS;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_PRICE = IROOM_TYPE___GET_PRICE;

	/**
	 * The operation id for the '<em>Set Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___SET_FEATURES__ELIST = IROOM_TYPE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = IROOM_TYPE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides <em>IRoom Type Room Manager Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeRoomManagerProvides()
	 * @generated
	 */
	int IROOM_TYPE_ROOM_MANAGER_PROVIDES = 3;

	/**
	 * The number of structural features of the '<em>IRoom Type Room Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ROOM_MANAGER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ROOM_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING = 0;

	/**
	 * The number of operations of the '<em>IRoom Type Room Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeManagerImpl <em>Room Type Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getRoomTypeManager()
	 * @generated
	 */
	int ROOM_TYPE_MANAGER = 2;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER__ROOM_TYPES = IROOM_TYPE_ROOM_MANAGER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Room Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER_FEATURE_COUNT = IROOM_TYPE_ROOM_MANAGER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___GET_ROOM_TYPE__STRING = IROOM_TYPE_ROOM_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Remove All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___REMOVE_ALL_ROOM_TYPES = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___GET_ROOM_TYPE__STRING_1 = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST_1 = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___GET_ALL_ROOM_TYPES = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Room Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER_OPERATION_COUNT = IROOM_TYPE_ROOM_MANAGER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides <em>IRoom Type Booking Manager Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeBookingManagerProvides()
	 * @generated
	 */
	int IROOM_TYPE_BOOKING_MANAGER_PROVIDES = 4;

	/**
	 * The number of structural features of the '<em>IRoom Type Booking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_BOOKING_MANAGER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_BOOKING_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING = 0;

	/**
	 * The number of operations of the '<em>IRoom Type Booking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_BOOKING_MANAGER_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides <em>IRoom Type Administrator Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeAdministratorProvides()
	 * @generated
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES = 5;

	/**
	 * The number of structural features of the '<em>IRoom Type Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST = 0;

	/**
	 * The operation id for the '<em>Get All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES___GET_ALL_ROOM_TYPES = 1;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST = 2;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = 3;

	/**
	 * The number of operations of the '<em>IRoom Type Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_ADMINISTRATOR_PROVIDES_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides <em>IRoom Type Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeStartupProvides()
	 * @generated
	 */
	int IROOM_TYPE_STARTUP_PROVIDES = 6;

	/**
	 * The number of structural features of the '<em>IRoom Type Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Remove All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_STARTUP_PROVIDES___REMOVE_ALL_ROOM_TYPES = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_STARTUP_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST = 1;

	/**
	 * The number of operations of the '<em>IRoom Type Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_STARTUP_PROVIDES_OPERATION_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getNumberOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumberOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Features</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#getFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Features();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#setFeatures(org.eclipse.emf.common.util.EList) <em>Set Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Features</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType#setFeatures(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getRoomType__SetFeatures__EList();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType <em>IRoom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType
	 * @generated
	 */
	EClass getIRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getName()
	 * @generated
	 */
	EOperation getIRoomType__GetName();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getFeatures() <em>Get Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Features</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getFeatures()
	 * @generated
	 */
	EOperation getIRoomType__GetFeatures();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getNumberOfBeds() <em>Get Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Beds</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getNumberOfBeds()
	 * @generated
	 */
	EOperation getIRoomType__GetNumberOfBeds();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType#getPrice()
	 * @generated
	 */
	EOperation getIRoomType__GetPrice();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager <em>Room Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager
	 * @generated
	 */
	EClass getRoomTypeManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Types</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager#getRoomTypes()
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	EReference getRoomTypeManager_RoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides <em>IRoom Type Room Manager Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Room Manager Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides
	 * @generated
	 */
	EClass getIRoomTypeRoomManagerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides#getRoomType(java.lang.String) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides#getRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeRoomManagerProvides__GetRoomType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides <em>IRoom Type Booking Manager Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Booking Manager Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides
	 * @generated
	 */
	EClass getIRoomTypeBookingManagerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides#getRoomType(java.lang.String) <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides#getRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeBookingManagerProvides__GetRoomType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides <em>IRoom Type Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Administrator Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides
	 * @generated
	 */
	EClass getIRoomTypeAdministratorProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#addRoomType(java.lang.String, int, double, org.eclipse.emf.common.util.EList) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#addRoomType(java.lang.String, int, double, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeAdministratorProvides__AddRoomType__String_int_double_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#getAllRoomTypes() <em>Get All Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#getAllRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeAdministratorProvides__GetAllRoomTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#updateRoomType(java.lang.String, java.lang.String, int, double, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#updateRoomType(java.lang.String, java.lang.String, int, double, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeAdministratorProvides__UpdateRoomType__String_String_int_double_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeAdministratorProvides__RemoveRoomType__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides <em>IRoom Type Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides
	 * @generated
	 */
	EClass getIRoomTypeStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides#removeAllRoomTypes() <em>Remove All Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove All Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides#removeAllRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeStartupProvides__RemoveAllRoomTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides#addRoomType(java.lang.String, int, double, org.eclipse.emf.common.util.EList) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides#addRoomType(java.lang.String, int, double, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeStartupProvides__AddRoomType__String_int_double_EList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomTypePackageFactory getRoomTypePackageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUMBER_OF_BEDS = eINSTANCE.getRoomType_NumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__FEATURES = eINSTANCE.getRoomType_Features();

		/**
		 * The meta object literal for the '<em><b>Set Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE___SET_FEATURES__ELIST = eINSTANCE.getRoomType__SetFeatures__EList();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType <em>IRoom Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomType()
		 * @generated
		 */
		EClass IROOM_TYPE = eINSTANCE.getIRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NAME = eINSTANCE.getIRoomType__GetName();

		/**
		 * The meta object literal for the '<em><b>Get Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_FEATURES = eINSTANCE.getIRoomType__GetFeatures();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NUMBER_OF_BEDS = eINSTANCE.getIRoomType__GetNumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_PRICE = eINSTANCE.getIRoomType__GetPrice();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeManagerImpl <em>Room Type Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getRoomTypeManager()
		 * @generated
		 */
		EClass ROOM_TYPE_MANAGER = eINSTANCE.getRoomTypeManager();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_MANAGER__ROOM_TYPES = eINSTANCE.getRoomTypeManager_RoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides <em>IRoom Type Room Manager Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeRoomManagerProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeRoomManagerProvides()
		 * @generated
		 */
		EClass IROOM_TYPE_ROOM_MANAGER_PROVIDES = eINSTANCE.getIRoomTypeRoomManagerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_ROOM_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeRoomManagerProvides__GetRoomType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides <em>IRoom Type Booking Manager Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeBookingManagerProvides()
		 * @generated
		 */
		EClass IROOM_TYPE_BOOKING_MANAGER_PROVIDES = eINSTANCE.getIRoomTypeBookingManagerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_BOOKING_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeBookingManagerProvides__GetRoomType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides <em>IRoom Type Administrator Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeAdministratorProvides()
		 * @generated
		 */
		EClass IROOM_TYPE_ADMINISTRATOR_PROVIDES = eINSTANCE.getIRoomTypeAdministratorProvides();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST = eINSTANCE.getIRoomTypeAdministratorProvides__AddRoomType__String_int_double_EList();

		/**
		 * The meta object literal for the '<em><b>Get All Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_ADMINISTRATOR_PROVIDES___GET_ALL_ROOM_TYPES = eINSTANCE.getIRoomTypeAdministratorProvides__GetAllRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST = eINSTANCE.getIRoomTypeAdministratorProvides__UpdateRoomType__String_String_int_double_EList();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeAdministratorProvides__RemoveRoomType__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides <em>IRoom Type Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl#getIRoomTypeStartupProvides()
		 * @generated
		 */
		EClass IROOM_TYPE_STARTUP_PROVIDES = eINSTANCE.getIRoomTypeStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Remove All Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_STARTUP_PROVIDES___REMOVE_ALL_ROOM_TYPES = eINSTANCE.getIRoomTypeStartupProvides__RemoveAllRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_STARTUP_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST = eINSTANCE.getIRoomTypeStartupProvides__AddRoomType__String_int_double_EList();

	}

} //RoomTypePackagePackage
