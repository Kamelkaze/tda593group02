/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Type Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypeManagerImpl#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeManagerImpl extends MinimalEObjectImpl.Container implements RoomTypeManager {
	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackagePackage.Literals.ROOM_TYPE_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectResolvingEList<RoomType>(RoomType.class, this, RoomTypePackagePackage.ROOM_TYPE_MANAGER__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the room type with the given name, or null if no such room type exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoomType getRoomType(String name) {
		for (RoomType r : getRoomTypes()) {
			if (r.getName().equalsIgnoreCase(name)) {
				return r;
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes all added room types.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeAllRoomTypes() {
		getRoomTypes().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room type with the given information, if no room type with the same name already exists.
	 * @returns true if room type was added, otherwise false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, int numberOfBeds, double price, EList<String> features) {
		if (getRoomType(name) != null) {
			return false;
		}
		
		getRoomTypes().add(new RoomTypeImpl(name, numberOfBeds, price, features));
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all existing room types in the system
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomType> getAllRoomTypes() {
		EList<IRoomType> roomTypes = new BasicEList<>();
		for (RoomType roomType : getRoomTypes()) {
			roomTypes.add(roomType);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Updates the information for a room type.
	 * @return false no room type with the given name exists, otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String name, String newName, int numberOfBeds, double price, EList<String> features) {
		RoomType roomType = (RoomType) getRoomType(name);
		
		if (roomType == null) {
			return false;
		}
		
		roomType.setName(newName);
		roomType.setNumberOfBeds(numberOfBeds);
		roomType.setPrice(price);
		roomType.setFeatures(features);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes the room type with the given name.
	 * @return false if no room type with the given name exists, otherwise true.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String name) {
		EList<RoomType> roomTypes = getRoomTypes();
		
		for (RoomType roomType : roomTypes) {
			if (roomType.getName().equalsIgnoreCase(name)) {
				roomTypes.remove(roomType);
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER__ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IRoomTypeStartupProvides.class) {
			switch (baseOperationID) {
				case RoomTypePackagePackage.IROOM_TYPE_STARTUP_PROVIDES___REMOVE_ALL_ROOM_TYPES: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___REMOVE_ALL_ROOM_TYPES;
				case RoomTypePackagePackage.IROOM_TYPE_STARTUP_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST;
				default: return -1;
			}
		}
		if (baseClass == IRoomTypeBookingManagerProvides.class) {
			switch (baseOperationID) {
				case RoomTypePackagePackage.IROOM_TYPE_BOOKING_MANAGER_PROVIDES___GET_ROOM_TYPE__STRING: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___GET_ROOM_TYPE__STRING_1;
				default: return -1;
			}
		}
		if (baseClass == IRoomTypeAdministratorProvides.class) {
			switch (baseOperationID) {
				case RoomTypePackagePackage.IROOM_TYPE_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST_1;
				case RoomTypePackagePackage.IROOM_TYPE_ADMINISTRATOR_PROVIDES___GET_ALL_ROOM_TYPES: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___GET_ALL_ROOM_TYPES;
				case RoomTypePackagePackage.IROOM_TYPE_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST;
				case RoomTypePackagePackage.IROOM_TYPE_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING: return RoomTypePackagePackage.ROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___GET_ROOM_TYPE__STRING_1:
				return getRoomType((String)arguments.get(0));
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___REMOVE_ALL_ROOM_TYPES:
				removeAllRoomTypes();
				return null;
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST_1:
				return addRoomType((String)arguments.get(0), (Integer)arguments.get(1), (Double)arguments.get(2), (EList<String>)arguments.get(3));
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___GET_ALL_ROOM_TYPES:
				return getAllRoomTypes();
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Double)arguments.get(3), (EList<String>)arguments.get(4));
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeManagerImpl
