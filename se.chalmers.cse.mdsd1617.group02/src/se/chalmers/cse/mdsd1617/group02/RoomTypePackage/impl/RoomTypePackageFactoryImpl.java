/**
 */
package se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypePackageFactoryImpl extends EFactoryImpl implements RoomTypePackageFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RoomTypePackageFactory init() {
		try {
			RoomTypePackageFactory theRoomTypePackageFactory = (RoomTypePackageFactory)EPackage.Registry.INSTANCE.getEFactory(RoomTypePackagePackage.eNS_URI);
			if (theRoomTypePackageFactory != null) {
				return theRoomTypePackageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RoomTypePackageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RoomTypePackagePackage.ROOM_TYPE: return createRoomType();
			case RoomTypePackagePackage.ROOM_TYPE_MANAGER: return createRoomTypeManager();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType createRoomType() {
		RoomTypeImpl roomType = new RoomTypeImpl();
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeManager createRoomTypeManager() {
		RoomTypeManagerImpl roomTypeManager = new RoomTypeManagerImpl();
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackagePackage getRoomTypePackagePackage() {
		return (RoomTypePackagePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RoomTypePackagePackage getPackage() {
		return RoomTypePackagePackage.eINSTANCE;
	}

} //RoomTypePackageFactoryImpl
