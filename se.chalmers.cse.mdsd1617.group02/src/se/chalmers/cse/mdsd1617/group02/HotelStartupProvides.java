/**
 */
package se.chalmers.cse.mdsd1617.group02;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {

	/**
	 * Returns the value of the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Manager</em>' reference.
	 * @see #setRoomTypeManager(IRoomTypeStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelStartupProvides_RoomTypeManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeStartupProvides getRoomTypeManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getRoomTypeManager <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Manager</em>' reference.
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	void setRoomTypeManager(IRoomTypeStartupProvides value);

	/**
	 * Returns the value of the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Manager</em>' reference.
	 * @see #setRoomManager(IRoomStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelStartupProvides_RoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomStartupProvides getRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getRoomManager <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Manager</em>' reference.
	 * @see #getRoomManager()
	 * @generated
	 */
	void setRoomManager(IRoomStartupProvides value);

	/**
	 * Returns the value of the '<em><b>Booking Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Manager</em>' reference.
	 * @see #setBookingManager(IBookingStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelStartupProvides_BookingManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingStartupProvides getBookingManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelStartupProvides#getBookingManager <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Manager</em>' reference.
	 * @see #getBookingManager()
	 * @generated
	 */
	void setBookingManager(IBookingStartupProvides value);
} // HotelStartupProvides
