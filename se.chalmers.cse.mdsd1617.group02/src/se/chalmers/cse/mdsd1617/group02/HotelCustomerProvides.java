/**
 */
package se.chalmers.cse.mdsd1617.group02;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides#getHotelBookingManager <em>Hotel Booking Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides#getHotelRoomManager <em>Hotel Room Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelCustomerProvides()
 * @model
 * @generated
 */
public interface HotelCustomerProvides extends IHotelCustomerProvides {
	/**
	 * Returns the value of the '<em><b>Hotel Booking Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hotel Booking Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hotel Booking Manager</em>' reference.
	 * @see #setHotelBookingManager(IBookingCustomerProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelCustomerProvides_HotelBookingManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingCustomerProvides getHotelBookingManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides#getHotelBookingManager <em>Hotel Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hotel Booking Manager</em>' reference.
	 * @see #getHotelBookingManager()
	 * @generated
	 */
	void setHotelBookingManager(IBookingCustomerProvides value);

	/**
	 * Returns the value of the '<em><b>Hotel Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hotel Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hotel Room Manager</em>' reference.
	 * @see #setHotelRoomManager(IRoomHotelCustomerProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotelCustomerProvides_HotelRoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomHotelCustomerProvides getHotelRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides#getHotelRoomManager <em>Hotel Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hotel Room Manager</em>' reference.
	 * @see #getHotelRoomManager()
	 * @generated
	 */
	void setHotelRoomManager(IRoomHotelCustomerProvides value);

} // HotelCustomerProvides
