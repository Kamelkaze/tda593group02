/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "BookingPackage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group02/BookingPackage.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group02.BookingPackage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackagePackage eINSTANCE = se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation <em>IRoom Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIRoomReservation()
	 * @generated
	 */
	int IROOM_RESERVATION = 3;

	/**
	 * The number of structural features of the '<em>IRoom Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Checked In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION___GET_CHECKED_IN = 0;

	/**
	 * The operation id for the '<em>Get Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION___GET_CHECKED_OUT = 1;

	/**
	 * The operation id for the '<em>Get IExtras</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION___GET_IEXTRAS = 2;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION___GET_ROOM_TYPE = 3;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION___GET_ROOM = 4;

	/**
	 * The number of operations of the '<em>IRoom Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_RESERVATION_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getRoomReservation()
	 * @generated
	 */
	int ROOM_RESERVATION = 0;

	/**
	 * The feature id for the '<em><b>Checked In</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__CHECKED_IN = IROOM_RESERVATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Checked Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__CHECKED_OUT = IROOM_RESERVATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extras</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__EXTRAS = IROOM_RESERVATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__ROOM = IROOM_RESERVATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION__ROOM_TYPE = IROOM_RESERVATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_FEATURE_COUNT = IROOM_RESERVATION_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Checked In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_CHECKED_IN = IROOM_RESERVATION___GET_CHECKED_IN;

	/**
	 * The operation id for the '<em>Get Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_CHECKED_OUT = IROOM_RESERVATION___GET_CHECKED_OUT;

	/**
	 * The operation id for the '<em>Get IExtras</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_IEXTRAS = IROOM_RESERVATION___GET_IEXTRAS;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_ROOM_TYPE = IROOM_RESERVATION___GET_ROOM_TYPE;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___GET_ROOM = IROOM_RESERVATION___GET_ROOM;

	/**
	 * The operation id for the '<em>Check Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___CHECK_OUT = IROOM_RESERVATION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___ADD_EXTRA__STRING_DOUBLE = IROOM_RESERVATION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION___CHECK_IN__IROOM = IROOM_RESERVATION_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Room Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_RESERVATION_OPERATION_COUNT = IROOM_RESERVATION_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra <em>IExtra</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIExtra()
	 * @generated
	 */
	int IEXTRA = 2;

	/**
	 * The number of structural features of the '<em>IExtra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTRA_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTRA___GET_DESCRIPTION = 0;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTRA___GET_PRICE = 1;

	/**
	 * The number of operations of the '<em>IExtra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTRA_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.ExtraImpl <em>Extra</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.ExtraImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getExtra()
	 * @generated
	 */
	int EXTRA = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__DESCRIPTION = IEXTRA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__PRICE = IEXTRA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_FEATURE_COUNT = IEXTRA_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA___GET_DESCRIPTION = IEXTRA___GET_DESCRIPTION;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA___GET_PRICE = IEXTRA___GET_PRICE;

	/**
	 * The number of operations of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_OPERATION_COUNT = IEXTRA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides <em>IBooking Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingCustomerProvides()
	 * @generated
	 */
	int IBOOKING_CUSTOMER_PROVIDES = 10;

	/**
	 * The number of structural features of the '<em>IBooking Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Get Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___GET_RESERVATIONS__DATE_DATE = 1;

	/**
	 * The operation id for the '<em>Add Room Before Confirmation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___CHECK_IN_ROOM__INT_STRING = 4;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_ROOM__INT_INT = 5;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_BOOKING__INT = 6;

	/**
	 * The operation id for the '<em>Mark Booking As Paid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES___MARK_BOOKING_AS_PAID__INT = 7;

	/**
	 * The number of operations of the '<em>IBooking Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBookingManager()
	 * @generated
	 */
	int BOOKING_MANAGER = 4;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__BOOKINGS = IBOOKING_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__ROOM_MANAGER = IBOOKING_CUSTOMER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__ROOM_TYPE_MANAGER = IBOOKING_CUSTOMER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_FEATURE_COUNT = IBOOKING_CUSTOMER_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_RESERVATIONS__DATE_DATE = IBOOKING_CUSTOMER_PROVIDES___GET_RESERVATIONS__DATE_DATE;

	/**
	 * The operation id for the '<em>Add Room Before Confirmation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING = IBOOKING_CUSTOMER_PROVIDES___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CONFIRM_BOOKING__INT = IBOOKING_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_ROOM__INT_STRING = IBOOKING_CUSTOMER_PROVIDES___CHECK_IN_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_OUT_ROOM__INT_INT = IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_OUT_BOOKING__INT = IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_BOOKING__INT;

	/**
	 * The operation id for the '<em>Mark Booking As Paid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___MARK_BOOKING_AS_PAID__INT = IBOOKING_CUSTOMER_PROVIDES___MARK_BOOKING_AS_PAID__INT;

	/**
	 * The operation id for the '<em>Remove All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___REMOVE_ALL_BOOKINGS = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_BOOKING__INT_ELIST = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Reserved Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_RESERVED_ROOM_TYPES__INT = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_ROOM__INT_STRING = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___REMOVE_ROOM__INT_STRING = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Update Time Period</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___UPDATE_TIME_PERIOD__INT_DATE_DATE = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CANCEL_BOOKING__INT = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_CONFIRMED_BOOKINGS = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__DATE = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_OUTS__STRING_STRING = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The number of operations of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_OPERATION_COUNT = IBOOKING_CUSTOMER_PROVIDES_OPERATION_COUNT + 12;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking <em>IBooking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBooking()
	 * @generated
	 */
	int IBOOKING = 8;

	/**
	 * The number of structural features of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_ID = 0;

	/**
	 * The operation id for the '<em>Get Start Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_START_DATE = 1;

	/**
	 * The operation id for the '<em>Get End Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_END_DATE = 2;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_STATUS = 3;

	/**
	 * The operation id for the '<em>Get IReservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_IRESERVATIONS = 4;

	/**
	 * The operation id for the '<em>Get ICustomer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_ICUSTOMER = 5;

	/**
	 * The number of operations of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_OPERATION_COUNT = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ID = IBOOKING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__START_DATE = IBOOKING_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__END_DATE = IBOOKING_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__STATUS = IBOOKING_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Reservations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVATIONS = IBOOKING_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Customer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__CUSTOMER = IBOOKING_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = IBOOKING_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_ID = IBOOKING___GET_ID;

	/**
	 * The operation id for the '<em>Get Start Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_START_DATE = IBOOKING___GET_START_DATE;

	/**
	 * The operation id for the '<em>Get End Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_END_DATE = IBOOKING___GET_END_DATE;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_STATUS = IBOOKING___GET_STATUS;

	/**
	 * The operation id for the '<em>Get IReservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_IRESERVATIONS = IBOOKING___GET_IRESERVATIONS;

	/**
	 * The operation id for the '<em>Get ICustomer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_ICUSTOMER = IBOOKING___GET_ICUSTOMER;

	/**
	 * The operation id for the '<em>Add Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_RESERVATION__IROOMTYPE = IBOOKING_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Remove Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___REMOVE_RESERVATION__STRING = IBOOKING_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Not Checked In Reservation Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_NOT_CHECKED_IN_RESERVATION_OF_TYPE__STRING = IBOOKING_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Not Checked In Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_NOT_CHECKED_IN_RESERVATIONS = IBOOKING_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Check Out Reservation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_RESERVATION__INT = IBOOKING_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Out Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_RESERVATIONS = IBOOKING_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Cancel</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CANCEL = IBOOKING_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_OCCUPIED_ROOMS__DATE_DATE = IBOOKING_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_CHECK_INS__DATE_DATE = IBOOKING_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_CHECK_OUTS__DATE_DATE = IBOOKING_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_EXTRA__INT_STRING_DOUBLE = IBOOKING_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Confirm</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CONFIRM = IBOOKING_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_IN__ELIST = IBOOKING_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Get Reserved Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_RESERVED_ROOM_TYPES = IBOOKING_OPERATION_COUNT + 13;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = IBOOKING_OPERATION_COUNT + 14;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer <em>ICustomer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getICustomer()
	 * @generated
	 */
	int ICUSTOMER = 7;

	/**
	 * The number of structural features of the '<em>ICustomer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICUSTOMER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get First Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICUSTOMER___GET_FIRST_NAME = 0;

	/**
	 * The operation id for the '<em>Get Last Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICUSTOMER___GET_LAST_NAME = 1;

	/**
	 * The number of operations of the '<em>ICustomer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICUSTOMER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CustomerImpl <em>Customer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CustomerImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCustomer()
	 * @generated
	 */
	int CUSTOMER = 6;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__FIRST_NAME = ICUSTOMER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER__LAST_NAME = ICUSTOMER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_FEATURE_COUNT = ICUSTOMER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get First Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___GET_FIRST_NAME = ICUSTOMER___GET_FIRST_NAME;

	/**
	 * The operation id for the '<em>Get Last Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER___GET_LAST_NAME = ICUSTOMER___GET_LAST_NAME;

	/**
	 * The number of operations of the '<em>Customer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOMER_OPERATION_COUNT = ICUSTOMER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl <em>Checking Out DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckingOutDTO()
	 * @generated
	 */
	int CHECKING_OUT_DTO = 9;

	/**
	 * The feature id for the '<em><b>Total Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKING_OUT_DTO__TOTAL_PRICE = 0;

	/**
	 * The feature id for the '<em><b>Room Numbers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKING_OUT_DTO__ROOM_NUMBERS = 1;

	/**
	 * The number of structural features of the '<em>Checking Out DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKING_OUT_DTO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Checking Out DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECKING_OUT_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides <em>IBooking Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingReceptionistProvides()
	 * @generated
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES = 11;

	/**
	 * The number of structural features of the '<em>IBooking Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST = 0;

	/**
	 * The operation id for the '<em>Get Reserved Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___GET_RESERVED_ROOM_TYPES__INT = 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING = 3;

	/**
	 * The operation id for the '<em>Update Time Period</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_DATE_DATE = 4;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = 5;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___GET_CONFIRMED_BOOKINGS = 6;

	/**
	 * The operation id for the '<em>List Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE = 7;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING = 8;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING = 9;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = 10;

	/**
	 * The number of operations of the '<em>IBooking Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_RECEPTIONIST_PROVIDES_OPERATION_COUNT = 11;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.OccupiedRoomDTOImpl <em>Occupied Room DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.OccupiedRoomDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getOccupiedRoomDTO()
	 * @generated
	 */
	int OCCUPIED_ROOM_DTO = 12;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCUPIED_ROOM_DTO__ROOM_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCUPIED_ROOM_DTO__BOOKING_ID = 1;

	/**
	 * The number of structural features of the '<em>Occupied Room DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCUPIED_ROOM_DTO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Occupied Room DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCUPIED_ROOM_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckInDTOImpl <em>Check In DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckInDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckInDTO()
	 * @generated
	 */
	int CHECK_IN_DTO = 13;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_DTO__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_DTO__ROOM_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_DTO__TIME = 2;

	/**
	 * The number of structural features of the '<em>Check In DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_DTO_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Check In DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_IN_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckOutDTOImpl <em>Check Out DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckOutDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckOutDTO()
	 * @generated
	 */
	int CHECK_OUT_DTO = 14;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_DTO__TIME = 0;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_DTO__ROOM_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_DTO__BOOKING_ID = 2;

	/**
	 * The number of structural features of the '<em>Check Out DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_DTO_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Check Out DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OUT_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_1Impl <em>Copy Of Check In DTO 1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_1Impl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_1()
	 * @generated
	 */
	int COPY_OF_CHECK_IN_DTO_1 = 15;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_1__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_1__ROOM_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_1__TIME = 2;

	/**
	 * The number of structural features of the '<em>Copy Of Check In DTO 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_1_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Copy Of Check In DTO 1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_1_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_2Impl <em>Copy Of Check In DTO 2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_2Impl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_2()
	 * @generated
	 */
	int COPY_OF_CHECK_IN_DTO_2 = 16;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_2__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_2__ROOM_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_2__TIME = 2;

	/**
	 * The number of structural features of the '<em>Copy Of Check In DTO 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_2_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Copy Of Check In DTO 2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_2_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_3Impl <em>Copy Of Check In DTO 3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_3Impl
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_3()
	 * @generated
	 */
	int COPY_OF_CHECK_IN_DTO_3 = 17;

	/**
	 * The feature id for the '<em><b>Booking Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_3__BOOKING_ID = 0;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_3__ROOM_NUMBER = 1;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_3__TIME = 2;

	/**
	 * The number of structural features of the '<em>Copy Of Check In DTO 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_3_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Copy Of Check In DTO 3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_OF_CHECK_IN_DTO_3_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides <em>IBooking Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingStartupProvides()
	 * @generated
	 */
	int IBOOKING_STARTUP_PROVIDES = 18;

	/**
	 * The number of structural features of the '<em>IBooking Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Remove All Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_STARTUP_PROVIDES___REMOVE_ALL_BOOKINGS = 0;

	/**
	 * The number of operations of the '<em>IBooking Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus <em>Booking Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBookingStatus()
	 * @generated
	 */
	int BOOKING_STATUS = 19;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation <em>Room Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation
	 * @generated
	 */
	EClass getRoomReservation();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getCheckedIn <em>Checked In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked In</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getCheckedIn()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_CheckedIn();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getCheckedOut <em>Checked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked Out</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getCheckedOut()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EAttribute getRoomReservation_CheckedOut();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getExtras <em>Extras</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extras</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getExtras()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Extras();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getRoom()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_Room();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#getRoomType()
	 * @see #getRoomReservation()
	 * @generated
	 */
	EReference getRoomReservation_RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#checkOut() <em>Check Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#checkOut()
	 * @generated
	 */
	EOperation getRoomReservation__CheckOut();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#addExtra(java.lang.String, double) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#addExtra(java.lang.String, double)
	 * @generated
	 */
	EOperation getRoomReservation__AddExtra__String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#checkIn(se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom) <em>Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation#checkIn(se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom)
	 * @generated
	 */
	EOperation getRoomReservation__CheckIn__IRoom();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra
	 * @generated
	 */
	EClass getExtra();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra#getDescription()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra#getPrice()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Price();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra <em>IExtra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IExtra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra
	 * @generated
	 */
	EClass getIExtra();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra#getDescription() <em>Get Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Description</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra#getDescription()
	 * @generated
	 */
	EOperation getIExtra__GetDescription();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra#getPrice()
	 * @generated
	 */
	EOperation getIExtra__GetPrice();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation <em>IRoom Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation
	 * @generated
	 */
	EClass getIRoomReservation();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getCheckedIn() <em>Get Checked In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getCheckedIn()
	 * @generated
	 */
	EOperation getIRoomReservation__GetCheckedIn();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getCheckedOut() <em>Get Checked Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked Out</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getCheckedOut()
	 * @generated
	 */
	EOperation getIRoomReservation__GetCheckedOut();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getIExtras() <em>Get IExtras</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get IExtras</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getIExtras()
	 * @generated
	 */
	EOperation getIRoomReservation__GetIExtras();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getRoomType() <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getRoomType()
	 * @generated
	 */
	EOperation getIRoomReservation__GetRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getRoom() <em>Get Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation#getRoom()
	 * @generated
	 */
	EOperation getIRoomReservation__GetRoom();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager <em>Booking Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager
	 * @generated
	 */
	EClass getBookingManager();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bookings</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getBookings()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_Bookings();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomManager()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_RoomManager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomTypeManager <em>Room Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomTypeManager()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_RoomTypeManager();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getId()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Id();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getStartDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_StartDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Date</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getEndDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_EndDate();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getStatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Status();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getReservations <em>Reservations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reservations</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getReservations()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Reservations();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCustomer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Customer</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCustomer()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Customer();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#addReservation(se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType) <em>Add Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#addReservation(se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType)
	 * @generated
	 */
	EOperation getBooking__AddReservation__IRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#removeReservation(java.lang.String) <em>Remove Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#removeReservation(java.lang.String)
	 * @generated
	 */
	EOperation getBooking__RemoveReservation__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getNotCheckedInReservationOfType(java.lang.String) <em>Get Not Checked In Reservation Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Not Checked In Reservation Of Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getNotCheckedInReservationOfType(java.lang.String)
	 * @generated
	 */
	EOperation getBooking__GetNotCheckedInReservationOfType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getNotCheckedInReservations() <em>Get Not Checked In Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Not Checked In Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getNotCheckedInReservations()
	 * @generated
	 */
	EOperation getBooking__GetNotCheckedInReservations();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkOutReservation(int) <em>Check Out Reservation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Reservation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkOutReservation(int)
	 * @generated
	 */
	EOperation getBooking__CheckOutReservation__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkOutReservations() <em>Check Out Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkOutReservations()
	 * @generated
	 */
	EOperation getBooking__CheckOutReservations();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#cancel() <em>Cancel</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#cancel()
	 * @generated
	 */
	EOperation getBooking__Cancel();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getOccupiedRooms(java.util.Date, java.util.Date) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getOccupiedRooms(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooking__GetOccupiedRooms__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCheckIns(java.util.Date, java.util.Date) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooking__GetCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCheckOuts(java.util.Date, java.util.Date) <em>Get Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getBooking__GetCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#addExtra(int, java.lang.String, double) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#addExtra(int, java.lang.String, double)
	 * @generated
	 */
	EOperation getBooking__AddExtra__int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#confirm() <em>Confirm</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#confirm()
	 * @generated
	 */
	EOperation getBooking__Confirm();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkIn(org.eclipse.emf.common.util.EList) <em>Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#checkIn(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBooking__CheckIn__EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getReservedRoomTypes() <em>Get Reserved Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reserved Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking#getReservedRoomTypes()
	 * @generated
	 */
	EOperation getBooking__GetReservedRoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer <em>Customer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Customer</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer
	 * @generated
	 */
	EClass getCustomer();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer#getFirstName()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer#getLastName()
	 * @see #getCustomer()
	 * @generated
	 */
	EAttribute getCustomer_LastName();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer <em>ICustomer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ICustomer</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer
	 * @generated
	 */
	EClass getICustomer();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer#getFirstName() <em>Get First Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get First Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer#getFirstName()
	 * @generated
	 */
	EOperation getICustomer__GetFirstName();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer#getLastName() <em>Get Last Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Last Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer#getLastName()
	 * @generated
	 */
	EOperation getICustomer__GetLastName();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking <em>IBooking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking
	 * @generated
	 */
	EClass getIBooking();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getId() <em>Get Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Id</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getId()
	 * @generated
	 */
	EOperation getIBooking__GetId();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getStartDate() <em>Get Start Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Start Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getStartDate()
	 * @generated
	 */
	EOperation getIBooking__GetStartDate();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getEndDate() <em>Get End Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get End Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getEndDate()
	 * @generated
	 */
	EOperation getIBooking__GetEndDate();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getStatus() <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getStatus()
	 * @generated
	 */
	EOperation getIBooking__GetStatus();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getIReservations() <em>Get IReservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get IReservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getIReservations()
	 * @generated
	 */
	EOperation getIBooking__GetIReservations();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getICustomer() <em>Get ICustomer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get ICustomer</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking#getICustomer()
	 * @generated
	 */
	EOperation getIBooking__GetICustomer();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO <em>Checking Out DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Checking Out DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO
	 * @generated
	 */
	EClass getCheckingOutDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getTotalPrice <em>Total Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getTotalPrice()
	 * @see #getCheckingOutDTO()
	 * @generated
	 */
	EAttribute getCheckingOutDTO_TotalPrice();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getRoomNumbers <em>Room Numbers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Room Numbers</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getRoomNumbers()
	 * @see #getCheckingOutDTO()
	 * @generated
	 */
	EAttribute getCheckingOutDTO_RoomNumbers();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides <em>IBooking Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides
	 * @generated
	 */
	EClass getIBookingCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#getReservations(java.util.Date, java.util.Date) <em>Get Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservations</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#getReservations(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__GetReservations__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#addRoomBeforeConfirmation(int, java.lang.String) <em>Add Room Before Confirmation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Before Confirmation</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#addRoomBeforeConfirmation(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__AddRoomBeforeConfirmation__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkInRoom(int, java.lang.String) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkInRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__CheckInRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#checkOutBooking(int)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#markBookingAsPaid(int) <em>Mark Booking As Paid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Mark Booking As Paid</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides#markBookingAsPaid(int)
	 * @generated
	 */
	EOperation getIBookingCustomerProvides__MarkBookingAsPaid__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides <em>IBooking Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Receptionist Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides
	 * @generated
	 */
	EClass getIBookingReceptionistProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#checkInBooking(int, org.eclipse.emf.common.util.EList) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#checkInBooking(int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__CheckInBooking__int_EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#getReservedRoomTypes(int) <em>Get Reserved Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reserved Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#getReservedRoomTypes(int)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__GetReservedRoomTypes__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#removeRoom(int, java.lang.String) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#removeRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__RemoveRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#updateTimePeriod(int, java.util.Date, java.util.Date) <em>Update Time Period</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Time Period</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#updateTimePeriod(int, java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__UpdateTimePeriod__int_Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#cancelBooking(int)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#getConfirmedBookings() <em>Get Confirmed Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Confirmed Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#getConfirmedBookings()
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__GetConfirmedBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listOccupiedRooms(java.util.Date) <em>List Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listOccupiedRooms(java.util.Date)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__ListOccupiedRooms__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__ListCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listCheckOuts(java.lang.String, java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#listCheckOuts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__ListCheckOuts__String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#addExtraCost(int, int, java.lang.String, double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides#addExtraCost(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIBookingReceptionistProvides__AddExtraCost__int_int_String_double();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO <em>Occupied Room DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Occupied Room DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO
	 * @generated
	 */
	EClass getOccupiedRoomDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO#getRoomNumber()
	 * @see #getOccupiedRoomDTO()
	 * @generated
	 */
	EAttribute getOccupiedRoomDTO_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO#getBookingId()
	 * @see #getOccupiedRoomDTO()
	 * @generated
	 */
	EAttribute getOccupiedRoomDTO_BookingId();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO <em>Check In DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check In DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO
	 * @generated
	 */
	EClass getCheckInDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getBookingId()
	 * @see #getCheckInDTO()
	 * @generated
	 */
	EAttribute getCheckInDTO_BookingId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getRoomNumber()
	 * @see #getCheckInDTO()
	 * @generated
	 */
	EAttribute getCheckInDTO_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO#getTime()
	 * @see #getCheckInDTO()
	 * @generated
	 */
	EAttribute getCheckInDTO_Time();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO <em>Check Out DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check Out DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO
	 * @generated
	 */
	EClass getCheckOutDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getTime()
	 * @see #getCheckOutDTO()
	 * @generated
	 */
	EAttribute getCheckOutDTO_Time();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getRoomNumber()
	 * @see #getCheckOutDTO()
	 * @generated
	 */
	EAttribute getCheckOutDTO_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO#getBookingId()
	 * @see #getCheckOutDTO()
	 * @generated
	 */
	EAttribute getCheckOutDTO_BookingId();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1 <em>Copy Of Check In DTO 1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Of Check In DTO 1</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1
	 * @generated
	 */
	EClass getCopyOf_CheckInDTO_1();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getBookingId()
	 * @see #getCopyOf_CheckInDTO_1()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_1_BookingId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getRoomNumber()
	 * @see #getCopyOf_CheckInDTO_1()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_1_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1#getTime()
	 * @see #getCopyOf_CheckInDTO_1()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_1_Time();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2 <em>Copy Of Check In DTO 2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Of Check In DTO 2</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2
	 * @generated
	 */
	EClass getCopyOf_CheckInDTO_2();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getBookingId()
	 * @see #getCopyOf_CheckInDTO_2()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_2_BookingId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getRoomNumber()
	 * @see #getCopyOf_CheckInDTO_2()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_2_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2#getTime()
	 * @see #getCopyOf_CheckInDTO_2()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_2_Time();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3 <em>Copy Of Check In DTO 3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Of Check In DTO 3</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3
	 * @generated
	 */
	EClass getCopyOf_CheckInDTO_3();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getBookingId <em>Booking Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Id</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getBookingId()
	 * @see #getCopyOf_CheckInDTO_3()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_3_BookingId();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getRoomNumber()
	 * @see #getCopyOf_CheckInDTO_3()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_3_RoomNumber();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3#getTime()
	 * @see #getCopyOf_CheckInDTO_3()
	 * @generated
	 */
	EAttribute getCopyOf_CheckInDTO_3_Time();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides <em>IBooking Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides
	 * @generated
	 */
	EClass getIBookingStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides#removeAllBookings() <em>Remove All Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove All Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides#removeAllBookings()
	 * @generated
	 */
	EOperation getIBookingStartupProvides__RemoveAllBookings();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus <em>Booking Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Booking Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus
	 * @generated
	 */
	EEnum getBookingStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingPackageFactory getBookingPackageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl <em>Room Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getRoomReservation()
		 * @generated
		 */
		EClass ROOM_RESERVATION = eINSTANCE.getRoomReservation();

		/**
		 * The meta object literal for the '<em><b>Checked In</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__CHECKED_IN = eINSTANCE.getRoomReservation_CheckedIn();

		/**
		 * The meta object literal for the '<em><b>Checked Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_RESERVATION__CHECKED_OUT = eINSTANCE.getRoomReservation_CheckedOut();

		/**
		 * The meta object literal for the '<em><b>Extras</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__EXTRAS = eINSTANCE.getRoomReservation_Extras();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__ROOM = eINSTANCE.getRoomReservation_Room();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_RESERVATION__ROOM_TYPE = eINSTANCE.getRoomReservation_RoomType();

		/**
		 * The meta object literal for the '<em><b>Check Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___CHECK_OUT = eINSTANCE.getRoomReservation__CheckOut();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___ADD_EXTRA__STRING_DOUBLE = eINSTANCE.getRoomReservation__AddExtra__String_double();

		/**
		 * The meta object literal for the '<em><b>Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_RESERVATION___CHECK_IN__IROOM = eINSTANCE.getRoomReservation__CheckIn__IRoom();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.ExtraImpl <em>Extra</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.ExtraImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getExtra()
		 * @generated
		 */
		EClass EXTRA = eINSTANCE.getExtra();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__DESCRIPTION = eINSTANCE.getExtra_Description();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__PRICE = eINSTANCE.getExtra_Price();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra <em>IExtra</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIExtra()
		 * @generated
		 */
		EClass IEXTRA = eINSTANCE.getIExtra();

		/**
		 * The meta object literal for the '<em><b>Get Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEXTRA___GET_DESCRIPTION = eINSTANCE.getIExtra__GetDescription();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEXTRA___GET_PRICE = eINSTANCE.getIExtra__GetPrice();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation <em>IRoom Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIRoomReservation()
		 * @generated
		 */
		EClass IROOM_RESERVATION = eINSTANCE.getIRoomReservation();

		/**
		 * The meta object literal for the '<em><b>Get Checked In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_RESERVATION___GET_CHECKED_IN = eINSTANCE.getIRoomReservation__GetCheckedIn();

		/**
		 * The meta object literal for the '<em><b>Get Checked Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_RESERVATION___GET_CHECKED_OUT = eINSTANCE.getIRoomReservation__GetCheckedOut();

		/**
		 * The meta object literal for the '<em><b>Get IExtras</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_RESERVATION___GET_IEXTRAS = eINSTANCE.getIRoomReservation__GetIExtras();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_RESERVATION___GET_ROOM_TYPE = eINSTANCE.getIRoomReservation__GetRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_RESERVATION___GET_ROOM = eINSTANCE.getIRoomReservation__GetRoom();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBookingManager()
		 * @generated
		 */
		EClass BOOKING_MANAGER = eINSTANCE.getBookingManager();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__BOOKINGS = eINSTANCE.getBookingManager_Bookings();

		/**
		 * The meta object literal for the '<em><b>Room Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__ROOM_MANAGER = eINSTANCE.getBookingManager_RoomManager();

		/**
		 * The meta object literal for the '<em><b>Room Type Manager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__ROOM_TYPE_MANAGER = eINSTANCE.getBookingManager_RoomTypeManager();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__ID = eINSTANCE.getBooking_Id();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__START_DATE = eINSTANCE.getBooking_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__END_DATE = eINSTANCE.getBooking_EndDate();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__STATUS = eINSTANCE.getBooking_Status();

		/**
		 * The meta object literal for the '<em><b>Reservations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__RESERVATIONS = eINSTANCE.getBooking_Reservations();

		/**
		 * The meta object literal for the '<em><b>Customer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__CUSTOMER = eINSTANCE.getBooking_Customer();

		/**
		 * The meta object literal for the '<em><b>Add Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_RESERVATION__IROOMTYPE = eINSTANCE.getBooking__AddReservation__IRoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___REMOVE_RESERVATION__STRING = eINSTANCE.getBooking__RemoveReservation__String();

		/**
		 * The meta object literal for the '<em><b>Get Not Checked In Reservation Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_NOT_CHECKED_IN_RESERVATION_OF_TYPE__STRING = eINSTANCE.getBooking__GetNotCheckedInReservationOfType__String();

		/**
		 * The meta object literal for the '<em><b>Get Not Checked In Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_NOT_CHECKED_IN_RESERVATIONS = eINSTANCE.getBooking__GetNotCheckedInReservations();

		/**
		 * The meta object literal for the '<em><b>Check Out Reservation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___CHECK_OUT_RESERVATION__INT = eINSTANCE.getBooking__CheckOutReservation__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___CHECK_OUT_RESERVATIONS = eINSTANCE.getBooking__CheckOutReservations();

		/**
		 * The meta object literal for the '<em><b>Cancel</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___CANCEL = eINSTANCE.getBooking__Cancel();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_OCCUPIED_ROOMS__DATE_DATE = eINSTANCE.getBooking__GetOccupiedRooms__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_CHECK_INS__DATE_DATE = eINSTANCE.getBooking__GetCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Get Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_CHECK_OUTS__DATE_DATE = eINSTANCE.getBooking__GetCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_EXTRA__INT_STRING_DOUBLE = eINSTANCE.getBooking__AddExtra__int_String_double();

		/**
		 * The meta object literal for the '<em><b>Confirm</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___CONFIRM = eINSTANCE.getBooking__Confirm();

		/**
		 * The meta object literal for the '<em><b>Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___CHECK_IN__ELIST = eINSTANCE.getBooking__CheckIn__EList();

		/**
		 * The meta object literal for the '<em><b>Get Reserved Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___GET_RESERVED_ROOM_TYPES = eINSTANCE.getBooking__GetReservedRoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CustomerImpl <em>Customer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CustomerImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCustomer()
		 * @generated
		 */
		EClass CUSTOMER = eINSTANCE.getCustomer();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__FIRST_NAME = eINSTANCE.getCustomer_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOMER__LAST_NAME = eINSTANCE.getCustomer_LastName();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer <em>ICustomer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getICustomer()
		 * @generated
		 */
		EClass ICUSTOMER = eINSTANCE.getICustomer();

		/**
		 * The meta object literal for the '<em><b>Get First Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ICUSTOMER___GET_FIRST_NAME = eINSTANCE.getICustomer__GetFirstName();

		/**
		 * The meta object literal for the '<em><b>Get Last Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ICUSTOMER___GET_LAST_NAME = eINSTANCE.getICustomer__GetLastName();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking <em>IBooking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBooking()
		 * @generated
		 */
		EClass IBOOKING = eINSTANCE.getIBooking();

		/**
		 * The meta object literal for the '<em><b>Get Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_ID = eINSTANCE.getIBooking__GetId();

		/**
		 * The meta object literal for the '<em><b>Get Start Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_START_DATE = eINSTANCE.getIBooking__GetStartDate();

		/**
		 * The meta object literal for the '<em><b>Get End Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_END_DATE = eINSTANCE.getIBooking__GetEndDate();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_STATUS = eINSTANCE.getIBooking__GetStatus();

		/**
		 * The meta object literal for the '<em><b>Get IReservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_IRESERVATIONS = eINSTANCE.getIBooking__GetIReservations();

		/**
		 * The meta object literal for the '<em><b>Get ICustomer</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_ICUSTOMER = eINSTANCE.getIBooking__GetICustomer();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl <em>Checking Out DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckingOutDTO()
		 * @generated
		 */
		EClass CHECKING_OUT_DTO = eINSTANCE.getCheckingOutDTO();

		/**
		 * The meta object literal for the '<em><b>Total Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECKING_OUT_DTO__TOTAL_PRICE = eINSTANCE.getCheckingOutDTO_TotalPrice();

		/**
		 * The meta object literal for the '<em><b>Room Numbers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECKING_OUT_DTO__ROOM_NUMBERS = eINSTANCE.getCheckingOutDTO_RoomNumbers();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides <em>IBooking Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingCustomerProvides()
		 * @generated
		 */
		EClass IBOOKING_CUSTOMER_PROVIDES = eINSTANCE.getIBookingCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBookingCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___GET_RESERVATIONS__DATE_DATE = eINSTANCE.getIBookingCustomerProvides__GetReservations__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Room Before Confirmation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING = eINSTANCE.getIBookingCustomerProvides__AddRoomBeforeConfirmation__int_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIBookingCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___CHECK_IN_ROOM__INT_STRING = eINSTANCE.getIBookingCustomerProvides__CheckInRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getIBookingCustomerProvides__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_BOOKING__INT = eINSTANCE.getIBookingCustomerProvides__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>Mark Booking As Paid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_CUSTOMER_PROVIDES___MARK_BOOKING_AS_PAID__INT = eINSTANCE.getIBookingCustomerProvides__MarkBookingAsPaid__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides <em>IBooking Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingReceptionistProvides()
		 * @generated
		 */
		EClass IBOOKING_RECEPTIONIST_PROVIDES = eINSTANCE.getIBookingReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST = eINSTANCE.getIBookingReceptionistProvides__CheckInBooking__int_EList();

		/**
		 * The meta object literal for the '<em><b>Get Reserved Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___GET_RESERVED_ROOM_TYPES__INT = eINSTANCE.getIBookingReceptionistProvides__GetReservedRoomTypes__int();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING = eINSTANCE.getIBookingReceptionistProvides__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING = eINSTANCE.getIBookingReceptionistProvides__RemoveRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Update Time Period</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_DATE_DATE = eINSTANCE.getIBookingReceptionistProvides__UpdateTimePeriod__int_Date_Date();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = eINSTANCE.getIBookingReceptionistProvides__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Confirmed Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___GET_CONFIRMED_BOOKINGS = eINSTANCE.getIBookingReceptionistProvides__GetConfirmedBookings();

		/**
		 * The meta object literal for the '<em><b>List Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE = eINSTANCE.getIBookingReceptionistProvides__ListOccupiedRooms__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getIBookingReceptionistProvides__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING = eINSTANCE.getIBookingReceptionistProvides__ListCheckOuts__String_String();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE = eINSTANCE.getIBookingReceptionistProvides__AddExtraCost__int_int_String_double();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.OccupiedRoomDTOImpl <em>Occupied Room DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.OccupiedRoomDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getOccupiedRoomDTO()
		 * @generated
		 */
		EClass OCCUPIED_ROOM_DTO = eINSTANCE.getOccupiedRoomDTO();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCCUPIED_ROOM_DTO__ROOM_NUMBER = eINSTANCE.getOccupiedRoomDTO_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCCUPIED_ROOM_DTO__BOOKING_ID = eINSTANCE.getOccupiedRoomDTO_BookingId();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckInDTOImpl <em>Check In DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckInDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckInDTO()
		 * @generated
		 */
		EClass CHECK_IN_DTO = eINSTANCE.getCheckInDTO();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN_DTO__BOOKING_ID = eINSTANCE.getCheckInDTO_BookingId();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN_DTO__ROOM_NUMBER = eINSTANCE.getCheckInDTO_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_IN_DTO__TIME = eINSTANCE.getCheckInDTO_Time();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckOutDTOImpl <em>Check Out DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckOutDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCheckOutDTO()
		 * @generated
		 */
		EClass CHECK_OUT_DTO = eINSTANCE.getCheckOutDTO();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT_DTO__TIME = eINSTANCE.getCheckOutDTO_Time();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT_DTO__ROOM_NUMBER = eINSTANCE.getCheckOutDTO_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHECK_OUT_DTO__BOOKING_ID = eINSTANCE.getCheckOutDTO_BookingId();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_1Impl <em>Copy Of Check In DTO 1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_1Impl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_1()
		 * @generated
		 */
		EClass COPY_OF_CHECK_IN_DTO_1 = eINSTANCE.getCopyOf_CheckInDTO_1();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_1__BOOKING_ID = eINSTANCE.getCopyOf_CheckInDTO_1_BookingId();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_1__ROOM_NUMBER = eINSTANCE.getCopyOf_CheckInDTO_1_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_1__TIME = eINSTANCE.getCopyOf_CheckInDTO_1_Time();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_2Impl <em>Copy Of Check In DTO 2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_2Impl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_2()
		 * @generated
		 */
		EClass COPY_OF_CHECK_IN_DTO_2 = eINSTANCE.getCopyOf_CheckInDTO_2();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_2__BOOKING_ID = eINSTANCE.getCopyOf_CheckInDTO_2_BookingId();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_2__ROOM_NUMBER = eINSTANCE.getCopyOf_CheckInDTO_2_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_2__TIME = eINSTANCE.getCopyOf_CheckInDTO_2_Time();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_3Impl <em>Copy Of Check In DTO 3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CopyOf_CheckInDTO_3Impl
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getCopyOf_CheckInDTO_3()
		 * @generated
		 */
		EClass COPY_OF_CHECK_IN_DTO_3 = eINSTANCE.getCopyOf_CheckInDTO_3();

		/**
		 * The meta object literal for the '<em><b>Booking Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_3__BOOKING_ID = eINSTANCE.getCopyOf_CheckInDTO_3_BookingId();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_3__ROOM_NUMBER = eINSTANCE.getCopyOf_CheckInDTO_3_RoomNumber();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_OF_CHECK_IN_DTO_3__TIME = eINSTANCE.getCopyOf_CheckInDTO_3_Time();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides <em>IBooking Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getIBookingStartupProvides()
		 * @generated
		 */
		EClass IBOOKING_STARTUP_PROVIDES = eINSTANCE.getIBookingStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Remove All Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_STARTUP_PROVIDES___REMOVE_ALL_BOOKINGS = eINSTANCE.getIBookingStartupProvides__RemoveAllBookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus <em>Booking Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus
		 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackagePackageImpl#getBookingStatus()
		 * @generated
		 */
		EEnum BOOKING_STATUS = eINSTANCE.getBookingStatus();

	}

} //BookingPackagePackage
