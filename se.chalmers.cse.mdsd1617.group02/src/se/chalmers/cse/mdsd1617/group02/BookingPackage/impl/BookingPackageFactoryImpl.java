/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.*;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BookingPackageFactoryImpl extends EFactoryImpl implements BookingPackageFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BookingPackageFactory init() {
		try {
			BookingPackageFactory theBookingPackageFactory = (BookingPackageFactory)EPackage.Registry.INSTANCE.getEFactory(BookingPackagePackage.eNS_URI);
			if (theBookingPackageFactory != null) {
				return theBookingPackageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BookingPackageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingPackageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BookingPackagePackage.ROOM_RESERVATION: return createRoomReservation();
			case BookingPackagePackage.EXTRA: return createExtra();
			case BookingPackagePackage.BOOKING_MANAGER: return createBookingManager();
			case BookingPackagePackage.BOOKING: return createBooking();
			case BookingPackagePackage.CUSTOMER: return createCustomer();
			case BookingPackagePackage.CHECKING_OUT_DTO: return createCheckingOutDTO();
			case BookingPackagePackage.OCCUPIED_ROOM_DTO: return createOccupiedRoomDTO();
			case BookingPackagePackage.CHECK_IN_DTO: return createCheckInDTO();
			case BookingPackagePackage.CHECK_OUT_DTO: return createCheckOutDTO();
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_1: return createCopyOf_CheckInDTO_1();
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_2: return createCopyOf_CheckInDTO_2();
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_3: return createCopyOf_CheckInDTO_3();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case BookingPackagePackage.BOOKING_STATUS:
				return createBookingStatusFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case BookingPackagePackage.BOOKING_STATUS:
				return convertBookingStatusToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomReservation createRoomReservation() {
		RoomReservationImpl roomReservation = new RoomReservationImpl();
		return roomReservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Extra createExtra() {
		ExtraImpl extra = new ExtraImpl();
		return extra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager createBookingManager() {
		BookingManagerImpl bookingManager = new BookingManagerImpl();
		return bookingManager;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BookingManager createBookingManager(IRoomTypeBookingManagerProvides roomTypeManager,
			IRoomBookingManagerProvides roomManager) {
		BookingManagerImpl bookingManager = new BookingManagerImpl(roomTypeManager, roomManager);
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Booking createBooking() {
		BookingImpl booking = new BookingImpl();
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer createCustomer() {
		CustomerImpl customer = new CustomerImpl();
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckingOutDTO createCheckingOutDTO() {
		CheckingOutDTOImpl checkingOutDTO = new CheckingOutDTOImpl();
		return checkingOutDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OccupiedRoomDTO createOccupiedRoomDTO() {
		OccupiedRoomDTOImpl occupiedRoomDTO = new OccupiedRoomDTOImpl();
		return occupiedRoomDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckInDTO createCheckInDTO() {
		CheckInDTOImpl checkInDTO = new CheckInDTOImpl();
		return checkInDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckOutDTO createCheckOutDTO() {
		CheckOutDTOImpl checkOutDTO = new CheckOutDTOImpl();
		return checkOutDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyOf_CheckInDTO_1 createCopyOf_CheckInDTO_1() {
		CopyOf_CheckInDTO_1Impl copyOf_CheckInDTO_1 = new CopyOf_CheckInDTO_1Impl();
		return copyOf_CheckInDTO_1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyOf_CheckInDTO_2 createCopyOf_CheckInDTO_2() {
		CopyOf_CheckInDTO_2Impl copyOf_CheckInDTO_2 = new CopyOf_CheckInDTO_2Impl();
		return copyOf_CheckInDTO_2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyOf_CheckInDTO_3 createCopyOf_CheckInDTO_3() {
		CopyOf_CheckInDTO_3Impl copyOf_CheckInDTO_3 = new CopyOf_CheckInDTO_3Impl();
		return copyOf_CheckInDTO_3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingStatus createBookingStatusFromString(EDataType eDataType, String initialValue) {
		BookingStatus result = BookingStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBookingStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingPackagePackage getBookingPackagePackage() {
		return (BookingPackagePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BookingPackagePackage getPackage() {
		return BookingPackagePackage.eINSTANCE;
	}

} //BookingPackageFactoryImpl
