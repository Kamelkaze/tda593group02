/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Checking Out DTO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl#getTotalPrice <em>Total Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.CheckingOutDTOImpl#getRoomNumbers <em>Room Numbers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CheckingOutDTOImpl extends MinimalEObjectImpl.Container implements CheckingOutDTO {
	/**
	 * The default value of the '{@link #getTotalPrice() <em>Total Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPrice()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_PRICE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalPrice() <em>Total Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPrice()
	 * @generated
	 * @ordered
	 */
	protected double totalPrice = TOTAL_PRICE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomNumbers() <em>Room Numbers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNumbers()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> roomNumbers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CheckingOutDTOImpl() {
		super();
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected CheckingOutDTOImpl(double price, EList<Integer> list) {
		super();
		setTotalPrice(price);
		roomNumbers = list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackagePackage.Literals.CHECKING_OUT_DTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalPrice(double newTotalPrice) {
		double oldTotalPrice = totalPrice;
		totalPrice = newTotalPrice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.CHECKING_OUT_DTO__TOTAL_PRICE, oldTotalPrice, totalPrice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getRoomNumbers() {
		if (roomNumbers == null) {
			roomNumbers = new EDataTypeUniqueEList<Integer>(Integer.class, this, BookingPackagePackage.CHECKING_OUT_DTO__ROOM_NUMBERS);
		}
		return roomNumbers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackagePackage.CHECKING_OUT_DTO__TOTAL_PRICE:
				return getTotalPrice();
			case BookingPackagePackage.CHECKING_OUT_DTO__ROOM_NUMBERS:
				return getRoomNumbers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackagePackage.CHECKING_OUT_DTO__TOTAL_PRICE:
				setTotalPrice((Double)newValue);
				return;
			case BookingPackagePackage.CHECKING_OUT_DTO__ROOM_NUMBERS:
				getRoomNumbers().clear();
				getRoomNumbers().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.CHECKING_OUT_DTO__TOTAL_PRICE:
				setTotalPrice(TOTAL_PRICE_EDEFAULT);
				return;
			case BookingPackagePackage.CHECKING_OUT_DTO__ROOM_NUMBERS:
				getRoomNumbers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.CHECKING_OUT_DTO__TOTAL_PRICE:
				return totalPrice != TOTAL_PRICE_EDEFAULT;
			case BookingPackagePackage.CHECKING_OUT_DTO__ROOM_NUMBERS:
				return roomNumbers != null && !roomNumbers.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (totalPrice: ");
		result.append(totalPrice);
		result.append(", roomNumbers: ");
		result.append(roomNumbers);
		result.append(')');
		return result.toString();
	}

} //CheckingOutDTOImpl
