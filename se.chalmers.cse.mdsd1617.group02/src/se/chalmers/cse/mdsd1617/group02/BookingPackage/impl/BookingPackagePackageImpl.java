/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_1;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_2;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CopyOf_CheckInDTO_3;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation;

import se.chalmers.cse.mdsd1617.group02.Group02Package;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackagePackage;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.impl.RoomPackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackagePackage;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.impl.RoomTypePackagePackageImpl;

import se.chalmers.cse.mdsd1617.group02.impl.Group02PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BookingPackagePackageImpl extends EPackageImpl implements BookingPackagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomReservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extraEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iExtraEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomReservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iCustomerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkingOutDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingCustomerProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingReceptionistProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass occupiedRoomDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkInDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkOutDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyOf_CheckInDTO_1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyOf_CheckInDTO_2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyOf_CheckInDTO_3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingStartupProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum bookingStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BookingPackagePackageImpl() {
		super(eNS_URI, BookingPackageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BookingPackagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BookingPackagePackage init() {
		if (isInited) return (BookingPackagePackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackagePackage.eNS_URI);

		// Obtain or create and register package
		BookingPackagePackageImpl theBookingPackagePackage = (BookingPackagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BookingPackagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BookingPackagePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group02PackageImpl theGroup02Package = (Group02PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) instanceof Group02PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group02Package.eNS_URI) : Group02Package.eINSTANCE);
		RoomTypePackagePackageImpl theRoomTypePackagePackage = (RoomTypePackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI) instanceof RoomTypePackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI) : RoomTypePackagePackage.eINSTANCE);
		RoomPackagePackageImpl theRoomPackagePackage = (RoomPackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI) instanceof RoomPackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI) : RoomPackagePackage.eINSTANCE);

		// Create package meta-data objects
		theBookingPackagePackage.createPackageContents();
		theGroup02Package.createPackageContents();
		theRoomTypePackagePackage.createPackageContents();
		theRoomPackagePackage.createPackageContents();

		// Initialize created meta-data
		theBookingPackagePackage.initializePackageContents();
		theGroup02Package.initializePackageContents();
		theRoomTypePackagePackage.initializePackageContents();
		theRoomPackagePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBookingPackagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BookingPackagePackage.eNS_URI, theBookingPackagePackage);
		return theBookingPackagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomReservation() {
		return roomReservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomReservation_CheckedIn() {
		return (EAttribute)roomReservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomReservation_CheckedOut() {
		return (EAttribute)roomReservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomReservation_Extras() {
		return (EReference)roomReservationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomReservation_Room() {
		return (EReference)roomReservationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomReservation_RoomType() {
		return (EReference)roomReservationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomReservation__CheckOut() {
		return roomReservationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomReservation__AddExtra__String_double() {
		return roomReservationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomReservation__CheckIn__IRoom() {
		return roomReservationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtra() {
		return extraEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_Description() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExtra_Price() {
		return (EAttribute)extraEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIExtra() {
		return iExtraEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIExtra__GetDescription() {
		return iExtraEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIExtra__GetPrice() {
		return iExtraEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomReservation() {
		return iRoomReservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomReservation__GetCheckedIn() {
		return iRoomReservationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomReservation__GetCheckedOut() {
		return iRoomReservationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomReservation__GetIExtras() {
		return iRoomReservationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomReservation__GetRoomType() {
		return iRoomReservationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomReservation__GetRoom() {
		return iRoomReservationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingManager() {
		return bookingManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingManager_Bookings() {
		return (EReference)bookingManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingManager_RoomManager() {
		return (EReference)bookingManagerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingManager_RoomTypeManager() {
		return (EReference)bookingManagerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooking() {
		return bookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_Id() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_StartDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_EndDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_Status() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_Reservations() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_Customer() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__AddReservation__IRoomType() {
		return bookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__RemoveReservation__String() {
		return bookingEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetNotCheckedInReservationOfType__String() {
		return bookingEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetNotCheckedInReservations() {
		return bookingEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__CheckOutReservation__int() {
		return bookingEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__CheckOutReservations() {
		return bookingEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__Cancel() {
		return bookingEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetOccupiedRooms__Date_Date() {
		return bookingEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetCheckIns__Date_Date() {
		return bookingEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetCheckOuts__Date_Date() {
		return bookingEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__AddExtra__int_String_double() {
		return bookingEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__Confirm() {
		return bookingEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__CheckIn__EList() {
		return bookingEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__GetReservedRoomTypes() {
		return bookingEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomer() {
		return customerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCustomer_FirstName() {
		return (EAttribute)customerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCustomer_LastName() {
		return (EAttribute)customerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getICustomer() {
		return iCustomerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getICustomer__GetFirstName() {
		return iCustomerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getICustomer__GetLastName() {
		return iCustomerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBooking() {
		return iBookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetId() {
		return iBookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetStartDate() {
		return iBookingEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetEndDate() {
		return iBookingEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetStatus() {
		return iBookingEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetIReservations() {
		return iBookingEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetICustomer() {
		return iBookingEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckingOutDTO() {
		return checkingOutDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckingOutDTO_TotalPrice() {
		return (EAttribute)checkingOutDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckingOutDTO_RoomNumbers() {
		return (EAttribute)checkingOutDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookingCustomerProvides() {
		return iBookingCustomerProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__InitiateBooking__String_String_String_String() {
		return iBookingCustomerProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__GetReservations__Date_Date() {
		return iBookingCustomerProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__AddRoomBeforeConfirmation__int_String() {
		return iBookingCustomerProvidesEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__ConfirmBooking__int() {
		return iBookingCustomerProvidesEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__CheckInRoom__int_String() {
		return iBookingCustomerProvidesEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__CheckOutRoom__int_int() {
		return iBookingCustomerProvidesEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__CheckOutBooking__int() {
		return iBookingCustomerProvidesEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingCustomerProvides__MarkBookingAsPaid__int() {
		return iBookingCustomerProvidesEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookingReceptionistProvides() {
		return iBookingReceptionistProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__CheckInBooking__int_EList() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__GetReservedRoomTypes__int() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__AddRoom__int_String() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__RemoveRoom__int_String() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__UpdateTimePeriod__int_Date_Date() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__CancelBooking__int() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__GetConfirmedBookings() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__ListOccupiedRooms__Date() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__ListCheckIns__String_String() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__ListCheckOuts__String_String() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingReceptionistProvides__AddExtraCost__int_int_String_double() {
		return iBookingReceptionistProvidesEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOccupiedRoomDTO() {
		return occupiedRoomDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOccupiedRoomDTO_RoomNumber() {
		return (EAttribute)occupiedRoomDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOccupiedRoomDTO_BookingId() {
		return (EAttribute)occupiedRoomDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckInDTO() {
		return checkInDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckInDTO_BookingId() {
		return (EAttribute)checkInDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckInDTO_RoomNumber() {
		return (EAttribute)checkInDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckInDTO_Time() {
		return (EAttribute)checkInDTOEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheckOutDTO() {
		return checkOutDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOutDTO_Time() {
		return (EAttribute)checkOutDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOutDTO_RoomNumber() {
		return (EAttribute)checkOutDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCheckOutDTO_BookingId() {
		return (EAttribute)checkOutDTOEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyOf_CheckInDTO_1() {
		return copyOf_CheckInDTO_1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_1_BookingId() {
		return (EAttribute)copyOf_CheckInDTO_1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_1_RoomNumber() {
		return (EAttribute)copyOf_CheckInDTO_1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_1_Time() {
		return (EAttribute)copyOf_CheckInDTO_1EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyOf_CheckInDTO_2() {
		return copyOf_CheckInDTO_2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_2_BookingId() {
		return (EAttribute)copyOf_CheckInDTO_2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_2_RoomNumber() {
		return (EAttribute)copyOf_CheckInDTO_2EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_2_Time() {
		return (EAttribute)copyOf_CheckInDTO_2EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyOf_CheckInDTO_3() {
		return copyOf_CheckInDTO_3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_3_BookingId() {
		return (EAttribute)copyOf_CheckInDTO_3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_3_RoomNumber() {
		return (EAttribute)copyOf_CheckInDTO_3EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCopyOf_CheckInDTO_3_Time() {
		return (EAttribute)copyOf_CheckInDTO_3EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookingStartupProvides() {
		return iBookingStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingStartupProvides__RemoveAllBookings() {
		return iBookingStartupProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBookingStatus() {
		return bookingStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingPackageFactory getBookingPackageFactory() {
		return (BookingPackageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomReservationEClass = createEClass(ROOM_RESERVATION);
		createEAttribute(roomReservationEClass, ROOM_RESERVATION__CHECKED_IN);
		createEAttribute(roomReservationEClass, ROOM_RESERVATION__CHECKED_OUT);
		createEReference(roomReservationEClass, ROOM_RESERVATION__EXTRAS);
		createEReference(roomReservationEClass, ROOM_RESERVATION__ROOM);
		createEReference(roomReservationEClass, ROOM_RESERVATION__ROOM_TYPE);
		createEOperation(roomReservationEClass, ROOM_RESERVATION___CHECK_OUT);
		createEOperation(roomReservationEClass, ROOM_RESERVATION___ADD_EXTRA__STRING_DOUBLE);
		createEOperation(roomReservationEClass, ROOM_RESERVATION___CHECK_IN__IROOM);

		extraEClass = createEClass(EXTRA);
		createEAttribute(extraEClass, EXTRA__DESCRIPTION);
		createEAttribute(extraEClass, EXTRA__PRICE);

		iExtraEClass = createEClass(IEXTRA);
		createEOperation(iExtraEClass, IEXTRA___GET_DESCRIPTION);
		createEOperation(iExtraEClass, IEXTRA___GET_PRICE);

		iRoomReservationEClass = createEClass(IROOM_RESERVATION);
		createEOperation(iRoomReservationEClass, IROOM_RESERVATION___GET_CHECKED_IN);
		createEOperation(iRoomReservationEClass, IROOM_RESERVATION___GET_CHECKED_OUT);
		createEOperation(iRoomReservationEClass, IROOM_RESERVATION___GET_IEXTRAS);
		createEOperation(iRoomReservationEClass, IROOM_RESERVATION___GET_ROOM_TYPE);
		createEOperation(iRoomReservationEClass, IROOM_RESERVATION___GET_ROOM);

		bookingManagerEClass = createEClass(BOOKING_MANAGER);
		createEReference(bookingManagerEClass, BOOKING_MANAGER__BOOKINGS);
		createEReference(bookingManagerEClass, BOOKING_MANAGER__ROOM_MANAGER);
		createEReference(bookingManagerEClass, BOOKING_MANAGER__ROOM_TYPE_MANAGER);

		bookingEClass = createEClass(BOOKING);
		createEAttribute(bookingEClass, BOOKING__ID);
		createEAttribute(bookingEClass, BOOKING__START_DATE);
		createEAttribute(bookingEClass, BOOKING__END_DATE);
		createEAttribute(bookingEClass, BOOKING__STATUS);
		createEReference(bookingEClass, BOOKING__RESERVATIONS);
		createEReference(bookingEClass, BOOKING__CUSTOMER);
		createEOperation(bookingEClass, BOOKING___ADD_RESERVATION__IROOMTYPE);
		createEOperation(bookingEClass, BOOKING___REMOVE_RESERVATION__STRING);
		createEOperation(bookingEClass, BOOKING___GET_NOT_CHECKED_IN_RESERVATION_OF_TYPE__STRING);
		createEOperation(bookingEClass, BOOKING___GET_NOT_CHECKED_IN_RESERVATIONS);
		createEOperation(bookingEClass, BOOKING___CHECK_OUT_RESERVATION__INT);
		createEOperation(bookingEClass, BOOKING___CHECK_OUT_RESERVATIONS);
		createEOperation(bookingEClass, BOOKING___CANCEL);
		createEOperation(bookingEClass, BOOKING___GET_OCCUPIED_ROOMS__DATE_DATE);
		createEOperation(bookingEClass, BOOKING___GET_CHECK_INS__DATE_DATE);
		createEOperation(bookingEClass, BOOKING___GET_CHECK_OUTS__DATE_DATE);
		createEOperation(bookingEClass, BOOKING___ADD_EXTRA__INT_STRING_DOUBLE);
		createEOperation(bookingEClass, BOOKING___CONFIRM);
		createEOperation(bookingEClass, BOOKING___CHECK_IN__ELIST);
		createEOperation(bookingEClass, BOOKING___GET_RESERVED_ROOM_TYPES);

		customerEClass = createEClass(CUSTOMER);
		createEAttribute(customerEClass, CUSTOMER__FIRST_NAME);
		createEAttribute(customerEClass, CUSTOMER__LAST_NAME);

		iCustomerEClass = createEClass(ICUSTOMER);
		createEOperation(iCustomerEClass, ICUSTOMER___GET_FIRST_NAME);
		createEOperation(iCustomerEClass, ICUSTOMER___GET_LAST_NAME);

		iBookingEClass = createEClass(IBOOKING);
		createEOperation(iBookingEClass, IBOOKING___GET_ID);
		createEOperation(iBookingEClass, IBOOKING___GET_START_DATE);
		createEOperation(iBookingEClass, IBOOKING___GET_END_DATE);
		createEOperation(iBookingEClass, IBOOKING___GET_STATUS);
		createEOperation(iBookingEClass, IBOOKING___GET_IRESERVATIONS);
		createEOperation(iBookingEClass, IBOOKING___GET_ICUSTOMER);

		checkingOutDTOEClass = createEClass(CHECKING_OUT_DTO);
		createEAttribute(checkingOutDTOEClass, CHECKING_OUT_DTO__TOTAL_PRICE);
		createEAttribute(checkingOutDTOEClass, CHECKING_OUT_DTO__ROOM_NUMBERS);

		iBookingCustomerProvidesEClass = createEClass(IBOOKING_CUSTOMER_PROVIDES);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___GET_RESERVATIONS__DATE_DATE);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___CHECK_IN_ROOM__INT_STRING);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_ROOM__INT_INT);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___CHECK_OUT_BOOKING__INT);
		createEOperation(iBookingCustomerProvidesEClass, IBOOKING_CUSTOMER_PROVIDES___MARK_BOOKING_AS_PAID__INT);

		iBookingReceptionistProvidesEClass = createEClass(IBOOKING_RECEPTIONIST_PROVIDES);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___GET_RESERVED_ROOM_TYPES__INT);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_DATE_DATE);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___GET_CONFIRMED_BOOKINGS);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING);
		createEOperation(iBookingReceptionistProvidesEClass, IBOOKING_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE);

		occupiedRoomDTOEClass = createEClass(OCCUPIED_ROOM_DTO);
		createEAttribute(occupiedRoomDTOEClass, OCCUPIED_ROOM_DTO__ROOM_NUMBER);
		createEAttribute(occupiedRoomDTOEClass, OCCUPIED_ROOM_DTO__BOOKING_ID);

		checkInDTOEClass = createEClass(CHECK_IN_DTO);
		createEAttribute(checkInDTOEClass, CHECK_IN_DTO__BOOKING_ID);
		createEAttribute(checkInDTOEClass, CHECK_IN_DTO__ROOM_NUMBER);
		createEAttribute(checkInDTOEClass, CHECK_IN_DTO__TIME);

		checkOutDTOEClass = createEClass(CHECK_OUT_DTO);
		createEAttribute(checkOutDTOEClass, CHECK_OUT_DTO__TIME);
		createEAttribute(checkOutDTOEClass, CHECK_OUT_DTO__ROOM_NUMBER);
		createEAttribute(checkOutDTOEClass, CHECK_OUT_DTO__BOOKING_ID);

		copyOf_CheckInDTO_1EClass = createEClass(COPY_OF_CHECK_IN_DTO_1);
		createEAttribute(copyOf_CheckInDTO_1EClass, COPY_OF_CHECK_IN_DTO_1__BOOKING_ID);
		createEAttribute(copyOf_CheckInDTO_1EClass, COPY_OF_CHECK_IN_DTO_1__ROOM_NUMBER);
		createEAttribute(copyOf_CheckInDTO_1EClass, COPY_OF_CHECK_IN_DTO_1__TIME);

		copyOf_CheckInDTO_2EClass = createEClass(COPY_OF_CHECK_IN_DTO_2);
		createEAttribute(copyOf_CheckInDTO_2EClass, COPY_OF_CHECK_IN_DTO_2__BOOKING_ID);
		createEAttribute(copyOf_CheckInDTO_2EClass, COPY_OF_CHECK_IN_DTO_2__ROOM_NUMBER);
		createEAttribute(copyOf_CheckInDTO_2EClass, COPY_OF_CHECK_IN_DTO_2__TIME);

		copyOf_CheckInDTO_3EClass = createEClass(COPY_OF_CHECK_IN_DTO_3);
		createEAttribute(copyOf_CheckInDTO_3EClass, COPY_OF_CHECK_IN_DTO_3__BOOKING_ID);
		createEAttribute(copyOf_CheckInDTO_3EClass, COPY_OF_CHECK_IN_DTO_3__ROOM_NUMBER);
		createEAttribute(copyOf_CheckInDTO_3EClass, COPY_OF_CHECK_IN_DTO_3__TIME);

		iBookingStartupProvidesEClass = createEClass(IBOOKING_STARTUP_PROVIDES);
		createEOperation(iBookingStartupProvidesEClass, IBOOKING_STARTUP_PROVIDES___REMOVE_ALL_BOOKINGS);

		// Create enums
		bookingStatusEEnum = createEEnum(BOOKING_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomPackagePackage theRoomPackagePackage = (RoomPackagePackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackagePackage.eNS_URI);
		RoomTypePackagePackage theRoomTypePackagePackage = (RoomTypePackagePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackagePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomReservationEClass.getESuperTypes().add(this.getIRoomReservation());
		extraEClass.getESuperTypes().add(this.getIExtra());
		bookingManagerEClass.getESuperTypes().add(this.getIBookingCustomerProvides());
		bookingManagerEClass.getESuperTypes().add(this.getIBookingStartupProvides());
		bookingManagerEClass.getESuperTypes().add(this.getIBookingReceptionistProvides());
		bookingEClass.getESuperTypes().add(this.getIBooking());
		customerEClass.getESuperTypes().add(this.getICustomer());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomReservationEClass, RoomReservation.class, "RoomReservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomReservation_CheckedIn(), ecorePackage.getEDate(), "checkedIn", null, 1, 1, RoomReservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomReservation_CheckedOut(), ecorePackage.getEDate(), "checkedOut", null, 1, 1, RoomReservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomReservation_Extras(), this.getExtra(), null, "extras", null, 0, -1, RoomReservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomReservation_Room(), theRoomPackagePackage.getIRoom(), null, "room", null, 1, 1, RoomReservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomReservation_RoomType(), theRoomTypePackagePackage.getIRoomType(), null, "roomType", null, 1, 1, RoomReservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getRoomReservation__CheckOut(), ecorePackage.getEDouble(), "checkOut", 1, 1, IS_UNIQUE, !IS_ORDERED);

		EOperation op = initEOperation(getRoomReservation__AddExtra__String_double(), null, "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getRoomReservation__CheckIn__IRoom(), null, "checkIn", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackagePackage.getIRoom(), "room", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(extraEClass, Extra.class, "Extra", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExtra_Description(), ecorePackage.getEString(), "description", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getExtra_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, Extra.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iExtraEClass, IExtra.class, "IExtra", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIExtra__GetDescription(), ecorePackage.getEString(), "getDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIExtra__GetPrice(), ecorePackage.getEDouble(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomReservationEClass, IRoomReservation.class, "IRoomReservation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomReservation__GetCheckedIn(), ecorePackage.getEDate(), "getCheckedIn", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomReservation__GetCheckedOut(), ecorePackage.getEDate(), "getCheckedOut", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomReservation__GetIExtras(), this.getIExtra(), "getIExtras", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomReservation__GetRoomType(), theRoomTypePackagePackage.getIRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomReservation__GetRoom(), theRoomPackagePackage.getIRoom(), "getRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(bookingManagerEClass, BookingManager.class, "BookingManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingManager_Bookings(), this.getBooking(), null, "bookings", null, 0, -1, BookingManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookingManager_RoomManager(), theRoomPackagePackage.getIRoomBookingManagerProvides(), null, "roomManager", null, 1, 1, BookingManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookingManager_RoomTypeManager(), theRoomTypePackagePackage.getIRoomTypeBookingManagerProvides(), null, "roomTypeManager", null, 1, 1, BookingManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingEClass, Booking.class, "Booking", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooking_Id(), ecorePackage.getEInt(), "id", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_StartDate(), ecorePackage.getEDate(), "startDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_EndDate(), ecorePackage.getEDate(), "endDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_Status(), this.getBookingStatus(), "status", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBooking_Reservations(), this.getRoomReservation(), null, "reservations", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBooking_Customer(), this.getCustomer(), null, "customer", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getBooking__AddReservation__IRoomType(), null, "addReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackagePackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__RemoveReservation__String(), ecorePackage.getEBoolean(), "removeReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__GetNotCheckedInReservationOfType__String(), this.getRoomReservation(), "getNotCheckedInReservationOfType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBooking__GetNotCheckedInReservations(), this.getRoomReservation(), "getNotCheckedInReservations", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__CheckOutReservation__int(), ecorePackage.getEDouble(), "checkOutReservation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBooking__CheckOutReservations(), this.getCheckingOutDTO(), "checkOutReservations", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBooking__Cancel(), ecorePackage.getEBoolean(), "cancel", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__GetOccupiedRooms__Date_Date(), theRoomPackagePackage.getIRoom(), "getOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__GetCheckIns__Date_Date(), this.getRoomReservation(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__GetCheckOuts__Date_Date(), this.getRoomReservation(), "getCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__AddExtra__int_String_double(), ecorePackage.getEBoolean(), "addExtra", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBooking__Confirm(), ecorePackage.getEBoolean(), "confirm", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getBooking__CheckIn__EList(), ecorePackage.getEBoolean(), "checkIn", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackagePackage.getIRoom(), "rooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBooking__GetReservedRoomTypes(), theRoomTypePackagePackage.getIRoomType(), "getReservedRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(customerEClass, Customer.class, "Customer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCustomer_FirstName(), ecorePackage.getEString(), "firstName", null, 1, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCustomer_LastName(), ecorePackage.getEString(), "lastName", null, 1, 1, Customer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iCustomerEClass, ICustomer.class, "ICustomer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getICustomer__GetFirstName(), ecorePackage.getEString(), "getFirstName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getICustomer__GetLastName(), ecorePackage.getEString(), "getLastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iBookingEClass, IBooking.class, "IBooking", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIBooking__GetId(), ecorePackage.getEInt(), "getId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetStartDate(), ecorePackage.getEDate(), "getStartDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetEndDate(), ecorePackage.getEDate(), "getEndDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetStatus(), this.getBookingStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetIReservations(), this.getIRoomReservation(), "getIReservations", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetICustomer(), this.getICustomer(), "getICustomer", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(checkingOutDTOEClass, CheckingOutDTO.class, "CheckingOutDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCheckingOutDTO_TotalPrice(), ecorePackage.getEDouble(), "totalPrice", null, 1, 1, CheckingOutDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckingOutDTO_RoomNumbers(), ecorePackage.getEInt(), "roomNumbers", null, 0, -1, CheckingOutDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookingCustomerProvidesEClass, IBookingCustomerProvides.class, "IBookingCustomerProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBookingCustomerProvides__InitiateBooking__String_String_String_String(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__GetReservations__Date_Date(), this.getIRoomReservation(), "getReservations", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__AddRoomBeforeConfirmation__int_String(), ecorePackage.getEBoolean(), "addRoomBeforeConfirmation", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__ConfirmBooking__int(), ecorePackage.getEBoolean(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__CheckInRoom__int_String(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__CheckOutRoom__int_int(), ecorePackage.getEDouble(), "checkOutRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__CheckOutBooking__int(), ecorePackage.getEDouble(), "checkOutBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingCustomerProvides__MarkBookingAsPaid__int(), ecorePackage.getEBoolean(), "markBookingAsPaid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iBookingReceptionistProvidesEClass, IBookingReceptionistProvides.class, "IBookingReceptionistProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBookingReceptionistProvides__CheckInBooking__int_EList(), ecorePackage.getEBoolean(), "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumbers", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__GetReservedRoomTypes__int(), theRoomTypePackagePackage.getIRoomType(), "getReservedRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__AddRoom__int_String(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__RemoveRoom__int_String(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__UpdateTimePeriod__int_Date_Date(), ecorePackage.getEBoolean(), "updateTimePeriod", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__CancelBooking__int(), ecorePackage.getEBoolean(), "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookingReceptionistProvides__GetConfirmedBookings(), this.getIBooking(), "getConfirmedBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__ListOccupiedRooms__Date(), this.getOccupiedRoomDTO(), "listOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDate(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__ListCheckIns__String_String(), this.getCheckInDTO(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__ListCheckOuts__String_String(), this.getCheckOutDTO(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingReceptionistProvides__AddExtraCost__int_int_String_double(), ecorePackage.getEBoolean(), "addExtraCost", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingId", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(occupiedRoomDTOEClass, OccupiedRoomDTO.class, "OccupiedRoomDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOccupiedRoomDTO_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, OccupiedRoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getOccupiedRoomDTO_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, OccupiedRoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(checkInDTOEClass, CheckInDTO.class, "CheckInDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCheckInDTO_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CheckInDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckInDTO_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, CheckInDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckInDTO_Time(), ecorePackage.getEString(), "time", null, 1, 1, CheckInDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(checkOutDTOEClass, CheckOutDTO.class, "CheckOutDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCheckOutDTO_Time(), ecorePackage.getEString(), "time", null, 1, 1, CheckOutDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckOutDTO_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, CheckOutDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCheckOutDTO_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CheckOutDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(copyOf_CheckInDTO_1EClass, CopyOf_CheckInDTO_1.class, "CopyOf_CheckInDTO_1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCopyOf_CheckInDTO_1_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CopyOf_CheckInDTO_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_1_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, CopyOf_CheckInDTO_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_1_Time(), ecorePackage.getEString(), "time", null, 1, 1, CopyOf_CheckInDTO_1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(copyOf_CheckInDTO_2EClass, CopyOf_CheckInDTO_2.class, "CopyOf_CheckInDTO_2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCopyOf_CheckInDTO_2_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CopyOf_CheckInDTO_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_2_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, CopyOf_CheckInDTO_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_2_Time(), ecorePackage.getEString(), "time", null, 1, 1, CopyOf_CheckInDTO_2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(copyOf_CheckInDTO_3EClass, CopyOf_CheckInDTO_3.class, "CopyOf_CheckInDTO_3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCopyOf_CheckInDTO_3_BookingId(), ecorePackage.getEInt(), "bookingId", null, 1, 1, CopyOf_CheckInDTO_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_3_RoomNumber(), ecorePackage.getEInt(), "roomNumber", null, 1, 1, CopyOf_CheckInDTO_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCopyOf_CheckInDTO_3_Time(), ecorePackage.getEString(), "time", null, 1, 1, CopyOf_CheckInDTO_3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookingStartupProvidesEClass, IBookingStartupProvides.class, "IBookingStartupProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIBookingStartupProvides__RemoveAllBookings(), null, "removeAllBookings", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(bookingStatusEEnum, BookingStatus.class, "BookingStatus");
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.INITIATED);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CONFIRMED);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CANCELLED);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CHECKED_IN);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.CHECKED_OUT);
		addEEnumLiteral(bookingStatusEEnum, BookingStatus.PAID);
	}

} //BookingPackagePackageImpl
