/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Customer;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.ICustomer;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.util.DateUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getId <em>Id</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getEndDate <em>End Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingImpl#getCustomer <em>Customer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final int ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected int id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected Date startDate = START_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date END_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected Date endDate = END_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final BookingStatus STATUS_EDEFAULT = BookingStatus.INITIATED;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected BookingStatus status = STATUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReservations() <em>Reservations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservations()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomReservation> reservations;

	/**
	 * The cached value of the '{@link #getCustomer() <em>Customer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomer()
	 * @generated
	 * @ordered
	 */
	protected Customer customer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingImpl() {
		super();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingImpl(int id, Date startDate, Date endDate, String firstName, String lastName) {
		super();
		setId(id);
		setStatus(BookingStatus.INITIATED);
		setStartDate(startDate);
		setEndDate(endDate);
		setCustomer(new CustomerImpl(firstName, lastName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackagePackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(int newId) {
		int oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartDate(Date newStartDate) {
		Date oldStartDate = startDate;
		startDate = newStartDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING__START_DATE, oldStartDate, startDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndDate(Date newEndDate) {
		Date oldEndDate = endDate;
		endDate = newEndDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING__END_DATE, oldEndDate, endDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(BookingStatus newStatus) {
		BookingStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomReservation> getReservations() {
		if (reservations == null) {
			reservations = new EObjectResolvingEList<RoomReservation>(RoomReservation.class, this, BookingPackagePackage.BOOKING__RESERVATIONS);
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer getCustomer() {
		if (customer != null && customer.eIsProxy()) {
			InternalEObject oldCustomer = (InternalEObject)customer;
			customer = (Customer)eResolveProxy(oldCustomer);
			if (customer != oldCustomer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackagePackage.BOOKING__CUSTOMER, oldCustomer, customer));
			}
		}
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Customer basicGetCustomer() {
		return customer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomer(Customer newCustomer) {
		Customer oldCustomer = customer;
		customer = newCustomer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING__CUSTOMER, oldCustomer, customer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns all reservations as IRoomReservation
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomReservation> getIReservations() {
		EList<IRoomReservation> iRoomReservations = new BasicEList<>();
		for (RoomReservation reservation : getReservations()) {
			iRoomReservations.add(reservation);
		}
		return iRoomReservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the customer as an ICustomer
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ICustomer getICustomer() {
		return getCustomer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a reservation of a given type to a booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addReservation(IRoomType roomType) {
		getReservations().add(new RoomReservationImpl(roomType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes the first reservation of the given type from a booking.
	 * @return true if a reservation was removed (i.e. the booking had a reservation of the given type), otherwise false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeReservation(String roomType) {
		EList<RoomReservation> reservations = getReservations();
		
		for (RoomReservation reservation : reservations) {
			if (reservation.getRoomType().getName().equalsIgnoreCase(roomType)) {
				reservations.remove(reservation);
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the first not checked in reservation of a given type from this booking, or null if there is
	 * no such reservation.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomReservation getNotCheckedInReservationOfType(String roomType) {
		for (RoomReservation reservation : getReservations()){
			if (reservation.getRoomType().getName().equalsIgnoreCase(roomType) && reservation.getCheckedIn() == null) {
				return reservation;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all not checked in reservations for this booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomReservation> getNotCheckedInReservations() {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		
		for (RoomReservation reservation : getReservations()){
			if (reservation.getCheckedIn() == null) {
				reservations.add(reservation);
			}
		}
		
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks out a reservation for the given room number.
	 * @return the price of the checked out reservation, or -1 if no reservation was found for the given room number
	 *         for this booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double checkOutReservation(int roomNumber) {
		for (RoomReservation reservation : getReservations()){
			if (reservation.getRoom().getNumber() == roomNumber && reservation.getCheckedIn() != null && reservation.getCheckedOut() == null){
				double price = reservation.checkOut();
				if (allReservationsCheckedOut()) {
					setStatus(BookingStatus.CHECKED_OUT);
				}
				return price;
			}
		}
		return -1;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Determines whether or not all reservations have been checked out for this booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private boolean allReservationsCheckedOut(){
		for (RoomReservation r : getReservations()){
			if (r.getCheckedOut() == null)
				return false;
		}
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Checks out all not checked out reservations for this booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CheckingOutDTO checkOutReservations() {
		double total = 0;
		EList<Integer> roomList = new BasicEList<Integer>();
		
		for (RoomReservation r : getReservations()) {
			int roomNumber = r.getRoom().getNumber();
			double price = checkOutReservation(roomNumber);
			if (price > -1) {
				total += price;
				roomList.add(roomNumber);
			}
		}
		
		setStatus(BookingStatus.CHECKED_OUT);
		return new CheckingOutDTOImpl(total, roomList);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Cancels this booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancel() {
		if (status == BookingStatus.INITIATED || status == BookingStatus.CONFIRMED) {
			getReservations().clear();
			setStatus(BookingStatus.CANCELLED);
			return true;
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all rooms that are occupied by this booking for a specified time period. 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> getOccupiedRooms(Date startDate, Date endDate) {
		EList<IRoom> rooms = new BasicEList<IRoom>();
		
		for (RoomReservation reservation : getReservations()) {
			if (reservation.getRoom().getStatus() == RoomStatus.OCCUPIED
					&& DateUtil.datesIntersect(reservation.getCheckedIn(), reservation.getCheckedOut(), startDate, endDate)) {
				rooms.add(reservation.getRoom());
			}
		}
		
		return rooms;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all reservations for this booking that was checked in between the given dates.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomReservation> getCheckIns(Date startDate, Date endDate) {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		
		for (RoomReservation reservation : getReservations()){
			if (DateUtil.isBetween(reservation.getCheckedIn(), startDate, endDate)) {
				reservations.add(reservation);
			}
		}
		
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all reservations for this booking that was checked out between the given dates.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomReservation> getCheckOuts(Date startDate, Date endDate) {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		
		for (RoomReservation reservation : getReservations()){
			if (DateUtil.isBetween(reservation.getCheckedOut(), startDate, endDate)) {
				reservations.add(reservation);
			}
		}
		
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds an extra to a reservation with the given room number.
	 * @return false if this booking has no reservation with the given room number.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtra(int roomNumber, String description, double price) {
		for (RoomReservation reservation : getReservations()) {
			if (reservation.getRoom().getNumber() == roomNumber){
				reservation.addExtra(description, price);
				return true;
			}
		}
		
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Sets the status of this booking to CONFIRMED. This can only be done if the status of the booking
	 * is INITIATED, and the booking has at least one reservation.
	 * @return true if all conditions are met, otherwise false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirm() {
		if (getStatus() != BookingStatus.INITIATED || getReservations().size() <= 0) {
			return false;
		}
		
		setStatus(BookingStatus.CONFIRMED);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks in this booking with the given rooms.
	 * @return false if the types of the rooms don't match the types of this bookings reservations.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkIn(EList<IRoom> rooms) {
		Map<IRoomType, Integer> roomTypeCounts = new HashMap<>();
		
		EList<RoomReservation> reservations = getNotCheckedInReservations();
		
		for (RoomReservation reservation : reservations) {
			int count = roomTypeCounts.getOrDefault(reservation.getRoomType(), 0);
			roomTypeCounts.put(reservation.getRoomType(), count + 1);
		}
		
		for (IRoom room : rooms) {
			int count = roomTypeCounts.getOrDefault(room.getType(), 0);
			
			if (count == 0) {
				return false;
			}
			
			roomTypeCounts.put(room.getType(), count - 1);
		}
		
		for (int count : roomTypeCounts.values()) {
			if (count != 0) {
				return false;
			}
		}
		
		for (RoomReservation reservation : reservations) {
			for (IRoom room : rooms) {
				if (room.getType().equals(reservation.getRoomType())) {
					reservation.checkIn(room);
				}
			}
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all unique room types that this booking has reserved.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomType> getReservedRoomTypes() {
		EList<IRoomType> roomTypes = new BasicEList<>();
		
		for (RoomReservation reservation : getReservations()) {
			IRoomType roomType = reservation.getRoomType();
			if (!roomTypes.contains(roomType)) {
				roomTypes.add(roomType);
			}
		}
		
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING__ID:
				return getId();
			case BookingPackagePackage.BOOKING__START_DATE:
				return getStartDate();
			case BookingPackagePackage.BOOKING__END_DATE:
				return getEndDate();
			case BookingPackagePackage.BOOKING__STATUS:
				return getStatus();
			case BookingPackagePackage.BOOKING__RESERVATIONS:
				return getReservations();
			case BookingPackagePackage.BOOKING__CUSTOMER:
				if (resolve) return getCustomer();
				return basicGetCustomer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING__ID:
				setId((Integer)newValue);
				return;
			case BookingPackagePackage.BOOKING__START_DATE:
				setStartDate((Date)newValue);
				return;
			case BookingPackagePackage.BOOKING__END_DATE:
				setEndDate((Date)newValue);
				return;
			case BookingPackagePackage.BOOKING__STATUS:
				setStatus((BookingStatus)newValue);
				return;
			case BookingPackagePackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				getReservations().addAll((Collection<? extends RoomReservation>)newValue);
				return;
			case BookingPackagePackage.BOOKING__CUSTOMER:
				setCustomer((Customer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING__ID:
				setId(ID_EDEFAULT);
				return;
			case BookingPackagePackage.BOOKING__START_DATE:
				setStartDate(START_DATE_EDEFAULT);
				return;
			case BookingPackagePackage.BOOKING__END_DATE:
				setEndDate(END_DATE_EDEFAULT);
				return;
			case BookingPackagePackage.BOOKING__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case BookingPackagePackage.BOOKING__RESERVATIONS:
				getReservations().clear();
				return;
			case BookingPackagePackage.BOOKING__CUSTOMER:
				setCustomer((Customer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING__ID:
				return id != ID_EDEFAULT;
			case BookingPackagePackage.BOOKING__START_DATE:
				return START_DATE_EDEFAULT == null ? startDate != null : !START_DATE_EDEFAULT.equals(startDate);
			case BookingPackagePackage.BOOKING__END_DATE:
				return END_DATE_EDEFAULT == null ? endDate != null : !END_DATE_EDEFAULT.equals(endDate);
			case BookingPackagePackage.BOOKING__STATUS:
				return status != STATUS_EDEFAULT;
			case BookingPackagePackage.BOOKING__RESERVATIONS:
				return reservations != null && !reservations.isEmpty();
			case BookingPackagePackage.BOOKING__CUSTOMER:
				return customer != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackagePackage.BOOKING___GET_IRESERVATIONS:
				return getIReservations();
			case BookingPackagePackage.BOOKING___GET_ICUSTOMER:
				return getICustomer();
			case BookingPackagePackage.BOOKING___ADD_RESERVATION__IROOMTYPE:
				addReservation((IRoomType)arguments.get(0));
				return null;
			case BookingPackagePackage.BOOKING___REMOVE_RESERVATION__STRING:
				return removeReservation((String)arguments.get(0));
			case BookingPackagePackage.BOOKING___GET_NOT_CHECKED_IN_RESERVATION_OF_TYPE__STRING:
				return getNotCheckedInReservationOfType((String)arguments.get(0));
			case BookingPackagePackage.BOOKING___GET_NOT_CHECKED_IN_RESERVATIONS:
				return getNotCheckedInReservations();
			case BookingPackagePackage.BOOKING___CHECK_OUT_RESERVATION__INT:
				return checkOutReservation((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING___CHECK_OUT_RESERVATIONS:
				return checkOutReservations();
			case BookingPackagePackage.BOOKING___CANCEL:
				return cancel();
			case BookingPackagePackage.BOOKING___GET_OCCUPIED_ROOMS__DATE_DATE:
				return getOccupiedRooms((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackagePackage.BOOKING___GET_CHECK_INS__DATE_DATE:
				return getCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackagePackage.BOOKING___GET_CHECK_OUTS__DATE_DATE:
				return getCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackagePackage.BOOKING___ADD_EXTRA__INT_STRING_DOUBLE:
				return addExtra((Integer)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2));
			case BookingPackagePackage.BOOKING___CONFIRM:
				return confirm();
			case BookingPackagePackage.BOOKING___CHECK_IN__ELIST:
				return checkIn((EList<IRoom>)arguments.get(0));
			case BookingPackagePackage.BOOKING___GET_RESERVED_ROOM_TYPES:
				return getReservedRoomTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", startDate: ");
		result.append(startDate);
		result.append(", endDate: ");
		result.append(endDate);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
