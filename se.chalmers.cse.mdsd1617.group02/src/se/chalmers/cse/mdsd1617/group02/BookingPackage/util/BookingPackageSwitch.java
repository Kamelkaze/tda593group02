/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage
 * @generated
 */
public class BookingPackageSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BookingPackagePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingPackageSwitch() {
		if (modelPackage == null) {
			modelPackage = BookingPackagePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BookingPackagePackage.ROOM_RESERVATION: {
				RoomReservation roomReservation = (RoomReservation)theEObject;
				T result = caseRoomReservation(roomReservation);
				if (result == null) result = caseIRoomReservation(roomReservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.EXTRA: {
				Extra extra = (Extra)theEObject;
				T result = caseExtra(extra);
				if (result == null) result = caseIExtra(extra);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IEXTRA: {
				IExtra iExtra = (IExtra)theEObject;
				T result = caseIExtra(iExtra);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IROOM_RESERVATION: {
				IRoomReservation iRoomReservation = (IRoomReservation)theEObject;
				T result = caseIRoomReservation(iRoomReservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.BOOKING_MANAGER: {
				BookingManager bookingManager = (BookingManager)theEObject;
				T result = caseBookingManager(bookingManager);
				if (result == null) result = caseIBookingCustomerProvides(bookingManager);
				if (result == null) result = caseIBookingStartupProvides(bookingManager);
				if (result == null) result = caseIBookingReceptionistProvides(bookingManager);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.BOOKING: {
				Booking booking = (Booking)theEObject;
				T result = caseBooking(booking);
				if (result == null) result = caseIBooking(booking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.CUSTOMER: {
				Customer customer = (Customer)theEObject;
				T result = caseCustomer(customer);
				if (result == null) result = caseICustomer(customer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.ICUSTOMER: {
				ICustomer iCustomer = (ICustomer)theEObject;
				T result = caseICustomer(iCustomer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IBOOKING: {
				IBooking iBooking = (IBooking)theEObject;
				T result = caseIBooking(iBooking);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.CHECKING_OUT_DTO: {
				CheckingOutDTO checkingOutDTO = (CheckingOutDTO)theEObject;
				T result = caseCheckingOutDTO(checkingOutDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IBOOKING_CUSTOMER_PROVIDES: {
				IBookingCustomerProvides iBookingCustomerProvides = (IBookingCustomerProvides)theEObject;
				T result = caseIBookingCustomerProvides(iBookingCustomerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES: {
				IBookingReceptionistProvides iBookingReceptionistProvides = (IBookingReceptionistProvides)theEObject;
				T result = caseIBookingReceptionistProvides(iBookingReceptionistProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.OCCUPIED_ROOM_DTO: {
				OccupiedRoomDTO occupiedRoomDTO = (OccupiedRoomDTO)theEObject;
				T result = caseOccupiedRoomDTO(occupiedRoomDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.CHECK_IN_DTO: {
				CheckInDTO checkInDTO = (CheckInDTO)theEObject;
				T result = caseCheckInDTO(checkInDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.CHECK_OUT_DTO: {
				CheckOutDTO checkOutDTO = (CheckOutDTO)theEObject;
				T result = caseCheckOutDTO(checkOutDTO);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_1: {
				CopyOf_CheckInDTO_1 copyOf_CheckInDTO_1 = (CopyOf_CheckInDTO_1)theEObject;
				T result = caseCopyOf_CheckInDTO_1(copyOf_CheckInDTO_1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_2: {
				CopyOf_CheckInDTO_2 copyOf_CheckInDTO_2 = (CopyOf_CheckInDTO_2)theEObject;
				T result = caseCopyOf_CheckInDTO_2(copyOf_CheckInDTO_2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.COPY_OF_CHECK_IN_DTO_3: {
				CopyOf_CheckInDTO_3 copyOf_CheckInDTO_3 = (CopyOf_CheckInDTO_3)theEObject;
				T result = caseCopyOf_CheckInDTO_3(copyOf_CheckInDTO_3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BookingPackagePackage.IBOOKING_STARTUP_PROVIDES: {
				IBookingStartupProvides iBookingStartupProvides = (IBookingStartupProvides)theEObject;
				T result = caseIBookingStartupProvides(iBookingStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Room Reservation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Room Reservation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoomReservation(RoomReservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extra</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extra</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtra(Extra object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IExtra</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IExtra</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIExtra(IExtra object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IRoom Reservation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IRoom Reservation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIRoomReservation(IRoomReservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Booking Manager</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Booking Manager</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBookingManager(BookingManager object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Booking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Booking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooking(Booking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Customer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Customer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomer(Customer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ICustomer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ICustomer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseICustomer(ICustomer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IBooking</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IBooking</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIBooking(IBooking object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Checking Out DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Checking Out DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckingOutDTO(CheckingOutDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IBooking Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IBooking Customer Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIBookingCustomerProvides(IBookingCustomerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IBooking Receptionist Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IBooking Receptionist Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIBookingReceptionistProvides(IBookingReceptionistProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Occupied Room DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Occupied Room DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOccupiedRoomDTO(OccupiedRoomDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Check In DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Check In DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckInDTO(CheckInDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Check Out DTO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Check Out DTO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckOutDTO(CheckOutDTO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCopyOf_CheckInDTO_1(CopyOf_CheckInDTO_1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCopyOf_CheckInDTO_2(CopyOf_CheckInDTO_2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Copy Of Check In DTO 3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCopyOf_CheckInDTO_3(CopyOf_CheckInDTO_3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IBooking Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IBooking Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIBookingStartupProvides(IBookingStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BookingPackageSwitch
