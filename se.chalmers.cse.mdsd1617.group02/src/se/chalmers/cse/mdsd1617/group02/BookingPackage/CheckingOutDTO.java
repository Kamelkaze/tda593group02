/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Checking Out DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getTotalPrice <em>Total Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getRoomNumbers <em>Room Numbers</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getCheckingOutDTO()
 * @model
 * @generated
 */
public interface CheckingOutDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Total Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Price</em>' attribute.
	 * @see #setTotalPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getCheckingOutDTO_TotalPrice()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getTotalPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO#getTotalPrice <em>Total Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Price</em>' attribute.
	 * @see #getTotalPrice()
	 * @generated
	 */
	void setTotalPrice(double value);

	/**
	 * Returns the value of the '<em><b>Room Numbers</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Numbers</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Numbers</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getCheckingOutDTO_RoomNumbers()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Integer> getRoomNumbers();

} // CheckingOutDTO
