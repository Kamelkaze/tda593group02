/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getIRoomReservation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomReservation extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	Date getCheckedIn();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	Date getCheckedOut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IExtra> getIExtras();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	IRoomType getRoomType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	IRoom getRoom();

} // IRoomReservation
