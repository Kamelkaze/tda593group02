/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage;

import org.eclipse.emf.ecore.EFactory;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage
 * @generated
 */
public interface BookingPackageFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackageFactory eINSTANCE = se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingPackageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Room Reservation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Room Reservation</em>'.
	 * @generated
	 */
	RoomReservation createRoomReservation();

	/**
	 * Returns a new object of class '<em>Extra</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extra</em>'.
	 * @generated
	 */
	Extra createExtra();

	/**
	 * Returns a new object of class '<em>Booking Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Booking Manager</em>'.
	 * @generated
	 */
	BookingManager createBookingManager();
	
	/**
	 * Returns a new object of class '<em>Booking Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Booking Manager</em>'.
	 * @generated NOT
	 */
	BookingManager createBookingManager(IRoomTypeBookingManagerProvides roomTypeManager, IRoomBookingManagerProvides roomManager);

	/**
	 * Returns a new object of class '<em>Booking</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Booking</em>'.
	 * @generated
	 */
	Booking createBooking();

	/**
	 * Returns a new object of class '<em>Customer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Customer</em>'.
	 * @generated
	 */
	Customer createCustomer();

	/**
	 * Returns a new object of class '<em>Checking Out DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Checking Out DTO</em>'.
	 * @generated
	 */
	CheckingOutDTO createCheckingOutDTO();

	/**
	 * Returns a new object of class '<em>Occupied Room DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Occupied Room DTO</em>'.
	 * @generated
	 */
	OccupiedRoomDTO createOccupiedRoomDTO();

	/**
	 * Returns a new object of class '<em>Check In DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Check In DTO</em>'.
	 * @generated
	 */
	CheckInDTO createCheckInDTO();

	/**
	 * Returns a new object of class '<em>Check Out DTO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Check Out DTO</em>'.
	 * @generated
	 */
	CheckOutDTO createCheckOutDTO();

	/**
	 * Returns a new object of class '<em>Copy Of Check In DTO 1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Of Check In DTO 1</em>'.
	 * @generated
	 */
	CopyOf_CheckInDTO_1 createCopyOf_CheckInDTO_1();

	/**
	 * Returns a new object of class '<em>Copy Of Check In DTO 2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Of Check In DTO 2</em>'.
	 * @generated
	 */
	CopyOf_CheckInDTO_2 createCopyOf_CheckInDTO_2();

	/**
	 * Returns a new object of class '<em>Copy Of Check In DTO 3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Of Check In DTO 3</em>'.
	 * @generated
	 */
	CopyOf_CheckInDTO_3 createCopyOf_CheckInDTO_3();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BookingPackagePackage getBookingPackagePackage();

} //BookingPackageFactory
