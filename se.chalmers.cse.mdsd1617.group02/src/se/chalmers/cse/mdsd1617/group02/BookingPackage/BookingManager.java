/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage;

import org.eclipse.emf.common.util.EList;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getBookings <em>Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomTypeManager <em>Room Type Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getBookingManager()
 * @model
 * @generated
 */
public interface BookingManager extends IBookingCustomerProvides, IBookingStartupProvides, IBookingReceptionistProvides {
	/**
	 * Returns the value of the '<em><b>Bookings</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookings</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getBookingManager_Bookings()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> getBookings();

	/**
	 * Returns the value of the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Manager</em>' reference.
	 * @see #setRoomManager(IRoomBookingManagerProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getBookingManager_RoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomBookingManagerProvides getRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomManager <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Manager</em>' reference.
	 * @see #getRoomManager()
	 * @generated
	 */
	void setRoomManager(IRoomBookingManagerProvides value);

	/**
	 * Returns the value of the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Manager</em>' reference.
	 * @see #setRoomTypeManager(IRoomTypeBookingManagerProvides)
	 * @see se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage#getBookingManager_RoomTypeManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeBookingManagerProvides getRoomTypeManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager#getRoomTypeManager <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Manager</em>' reference.
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	void setRoomTypeManager(IRoomTypeBookingManagerProvides value);

} // BookingManager
