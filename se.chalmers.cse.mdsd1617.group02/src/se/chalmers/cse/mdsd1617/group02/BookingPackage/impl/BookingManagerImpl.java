/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Booking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckingOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeBookingManagerProvides;
import se.chalmers.cse.mdsd1617.group02.util.DateUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl#getBookings <em>Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.BookingManagerImpl#getRoomTypeManager <em>Room Type Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingManagerImpl extends MinimalEObjectImpl.Container implements BookingManager {
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> bookings;
	
	/**
	 * The next booking id to assign to a new booking.
	 * @generated NOT
	 */
	private int currentBookingId = 0;

	/**
	 * The cached value of the '{@link #getRoomManager() <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomBookingManagerProvides roomManager;

	/**
	 * The cached value of the '{@link #getRoomTypeManager() <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeBookingManagerProvides roomTypeManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingManagerImpl() {
		super();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingManagerImpl(IRoomTypeBookingManagerProvides roomTypeManager, IRoomBookingManagerProvides roomManager) {
		super();
		setRoomTypeManager(roomTypeManager);
		setRoomManager(roomManager);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackagePackage.Literals.BOOKING_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackagePackage.BOOKING_MANAGER__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomBookingManagerProvides getRoomManager() {
		if (roomManager != null && roomManager.eIsProxy()) {
			InternalEObject oldRoomManager = (InternalEObject)roomManager;
			roomManager = (IRoomBookingManagerProvides)eResolveProxy(oldRoomManager);
			if (roomManager != oldRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER, oldRoomManager, roomManager));
			}
		}
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomBookingManagerProvides basicGetRoomManager() {
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomManager(IRoomBookingManagerProvides newRoomManager) {
		IRoomBookingManagerProvides oldRoomManager = roomManager;
		roomManager = newRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER, oldRoomManager, roomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeBookingManagerProvides getRoomTypeManager() {
		if (roomTypeManager != null && roomTypeManager.eIsProxy()) {
			InternalEObject oldRoomTypeManager = (InternalEObject)roomTypeManager;
			roomTypeManager = (IRoomTypeBookingManagerProvides)eResolveProxy(oldRoomTypeManager);
			if (roomTypeManager != oldRoomTypeManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
			}
		}
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeBookingManagerProvides basicGetRoomTypeManager() {
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeManager(IRoomTypeBookingManagerProvides newRoomTypeManager) {
		IRoomTypeBookingManagerProvides oldRoomTypeManager = roomTypeManager;
		roomTypeManager = newRoomTypeManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Initiates a booking. firstName and lastName can't be empty, and startDate must be before endDate.
	 * @return the ID of the new booking, or -1 if parameters are not valid.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, String startDate, String endDate) {
		Date sDate = DateUtil.parseDate(startDate);
		Date eDate = DateUtil.parseDate(endDate);
		
		if (firstName == null || lastName == null || firstName.isEmpty() || lastName.isEmpty() || sDate == null || eDate == null || sDate.after(eDate)) {
			return -1;
		}
		
		currentBookingId++;
		
		getBookings().add(new BookingImpl(currentBookingId, sDate, eDate, firstName, lastName));
		
		return currentBookingId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all room reservations during a given time period.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomReservation> getReservations(Date startDate, Date endDate) {
		EList<IRoomReservation> reservations = new BasicEList<IRoomReservation>();
		
		for (Booking booking : getBookings()) {
			if (DateUtil.datesIntersect(booking.getStartDate(), booking.getEndDate(), startDate, endDate)) {
				reservations.addAll(booking.getReservations());
			}
		}
		
		return reservations;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Adds a room of a given type to a booking.
	 * @return false if no booking with the given id exists, if no room type with the given name exists,
	 *         if there are no free rooms during the booking's time period, or if the booking has been confirmed already.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomBeforeConfirmation(int bookingId, String roomType) {
		Booking b = getBooking(bookingId);
		
		if (b == null || b.getStatus() != BookingStatus.INITIATED || getNumberOfFreeRoomsOfType(roomType, b.getStartDate(), b.getEndDate()) <= 0) {
			return false;
		}
		
		IRoomType rType = roomTypeManager.getRoomType(roomType);
		
		if (rType == null) {
			return false;
		}
		
		b.addReservation(rType);
		
		return true;
	}

	private EList<RoomReservation> getReservationsOfType(String type, Date startDate, Date endDate) {
		EList<RoomReservation> reservations = new BasicEList<RoomReservation>();
		
		for (Booking booking : getBookings()) {
			if (DateUtil.datesIntersect(booking.getStartDate(), booking.getEndDate(), startDate, endDate)) {
				for (RoomReservation reservation : booking.getReservations()) {
					if (reservation.getRoomType().getName().equalsIgnoreCase(type)) {
						reservations.add(reservation);						
					}
				}
			}
		}
		
		return reservations;
	}
	
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Booking getBooking(int bookingId){
		for(Booking b : getBookings())
			if(b.getId() == bookingId)
				return b;
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int bookingId, String roomType) {
		Booking b = getBooking(bookingId);
		
		if (b == null || getNumberOfFreeRoomsOfType(roomType, b.getStartDate(), b.getEndDate()) <= 0) {
			return false;
		}
		
		IRoomType rType = roomTypeManager.getRoomType(roomType);
		
		if (rType == null) {
			return false;
		}
		
		b.addReservation(rType);
		
		return true;
	}
	
	private int getNumberOfFreeRoomsOfType(String roomType, Date startDate, Date endDate) {
		int totalNumberOfRooms = getRoomManager().getNumberOfBookableRoomsOfType(roomType);
		
		EList<RoomReservation> reservationsOfType = getReservationsOfType(roomType, startDate, endDate);
		
		return totalNumberOfRooms - reservationsOfType.size();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Confirms a booking. A booking can only be confirmed if its current status is INITIATED,
	 * and it has at least one added reservation.
	 * @return true if all conditions are met, otherwise false.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingId) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		
		return booking.confirm();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks in a room for a booking. The first free room of the given type is chosen.
	 * @return false if booking doesn't exist, it doesn't have any not checked in reservations of the
	 *         given room type or if the room type doesn't exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingId, String roomType) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return -1;
		}
		
		RoomReservation reservation = booking.getNotCheckedInReservationOfType(roomType);
		
		if (reservation == null) {
			return -1;
		}
		
		IRoom room = roomManager.occupyRoomOfType(roomType);
		
		if (room == null) {
			return -1;
		}
		
		reservation.checkIn(room);
		
		if (booking.getNotCheckedInReservations().size() == 0) {
			booking.setStatus(BookingStatus.CHECKED_IN);
		}

		return room.getNumber();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks out a room with a given room number from the booking with the given id.
	 * @return The price of the room, or -1 if there is no booking with the given id, or if the booking with the given id
	 *         doesn't have a not checked in room with the given room number.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double checkOutRoom(int bookingId, int roomNumber) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return -1;
		}
		
		return booking.checkOutReservation(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks out all reservations for a booking. This also marks all reserved rooms as free.
	 * @return The total price of the booking, or -1 if no booking with the given id exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double checkOutBooking(int bookingId) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return -1;
		}
		
		CheckingOutDTO checkOutInfo = booking.checkOutReservations();
		
		for (int roomNumber : checkOutInfo.getRoomNumbers()) {
			getRoomManager().markRoomAsFree(roomNumber);
		}
		
		return checkOutInfo.getTotalPrice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Simply marks a booking as paid.
	 * @return false if no booking with the given id exists, or if the booking with the given id
	 *         doesn't have status CHECKED_OUT.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean markBookingAsPaid(int bookingId) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null || booking.getStatus() != BookingStatus.CHECKED_OUT) {
			return false;
		}
		
		booking.setStatus(BookingStatus.PAID);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes all bookings from the system.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeAllBookings() {
		getBookings().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks in all reservations for a booking into the rooms with the provided room numbers.
	 * @return false if no booking with the given id exists, if any of the given rooms aren't free,
	 *         or the types of the rooms don't match the reservations for the booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingId, EList<Integer> roomNumbers) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		
		EList<IRoom> rooms = getRoomManager().getRooms(roomNumbers);
		
		for (IRoom room : rooms) {
			if (room.getStatus() != RoomStatus.FREE) {
				return false;
			}
		}
		
		boolean isCheckedIn = booking.checkIn(rooms);
		
		if (!isCheckedIn) {
			return false;
		}
		
		booking.setStatus(BookingStatus.CHECKED_IN);
		
		getRoomManager().occupyRooms(roomNumbers);
		
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all room types that are reserved for a booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomType> getReservedRoomTypes(int bookingId) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return new BasicEList<>();
		}
		
		return booking.getReservedRoomTypes();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes a room of a given type from a booking.
	 * @return false if no booking with the given id exists, or the booking with the given id doesn't have a room
	 *         of the given room type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int bookingId, String roomType) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		
		return booking.removeReservation(roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Updates the time period for a booking. Assumes that startDate is before endDate.
	 * @return false if no booking with the given id exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateTimePeriod(int bookingId, Date startDate, Date endDate) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		
		booking.setStartDate(startDate);
		booking.setEndDate(endDate);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Cancels a booking.
	 * @return false if no booking with the given id exists, or if the booking has been confirmed.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingId) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		return booking.cancel();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all confirmed bookings.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IBooking> getConfirmedBookings() {
		EList<IBooking> confirmedBookings = new BasicEList<>();
		
		for (Booking booking : getBookings()) {
			if (booking.getStatus() == BookingStatus.CONFIRMED) {
				confirmedBookings.add(booking);
			}
		}
		
		return confirmedBookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all rooms that are occupied during the given date.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<OccupiedRoomDTO> listOccupiedRooms(Date date) {
		EList<OccupiedRoomDTO> occupiedRoomDTOs = new BasicEList<>();
		
		for (Booking booking : getBookings()) {
			EList<IRoom> occupiedRooms = booking.getOccupiedRooms(date, date);
			for (IRoom room : occupiedRooms) {
				occupiedRoomDTOs.add(new OccupiedRoomDTOImpl(room.getNumber(), booking.getId()));
			}
		}
		
		return occupiedRoomDTOs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Lists all check ins that were made between two dates.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckInDTO> listCheckIns(String startDate, String endDate) {
		Date sDate = DateUtil.parseDate(startDate);
		Date eDate = DateUtil.parseDate(endDate);
		
		EList<CheckInDTO> checkIns = new BasicEList<>();
		
		for (Booking booking : getBookings()) {
			EList<RoomReservation> reservations = booking.getCheckIns(sDate, eDate);
			
			for (RoomReservation reservation : reservations) {
				CheckInDTO dto = new CheckInDTOImpl(
						booking.getId(),
						reservation.getRoom().getNumber(),
						DateUtil.dateToString(reservation.getCheckedIn())
				); 
				checkIns.add(dto);
			}
		}
		
		return checkIns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Lists all check out that were made between two dates.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOutDTO> listCheckOuts(String startDate, String endDate) {
		Date sDate = DateUtil.parseDate(startDate);
		Date eDate = DateUtil.parseDate(endDate);
		
		EList<CheckOutDTO> checkOuts = new BasicEList<>();
		
		for (Booking booking : getBookings()) {
			EList<RoomReservation> reservations = booking.getCheckOuts(sDate, eDate);
			
			for (RoomReservation reservation : reservations) {
				CheckOutDTO dto = new CheckOutDTOImpl(
						booking.getId(),
						reservation.getRoom().getNumber(),
						DateUtil.dateToString(reservation.getCheckedIn())
				); 
				checkOuts.add(dto);
			}
		}
		
		return checkOuts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds an extra cost to a booking and a room.
	 * @return false if no booking with the given id exists or if booking with given id doesn't have any
	 *         checked in reservations for the given room number.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCost(int bookingId, int roomNumber, String description, double price) {
		Booking booking = getBooking(bookingId);
		
		if (booking == null) {
			return false;
		}
		
		return booking.addExtra(roomNumber, description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING_MANAGER__BOOKINGS:
				return getBookings();
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER:
				if (resolve) return getRoomManager();
				return basicGetRoomManager();
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER:
				if (resolve) return getRoomTypeManager();
				return basicGetRoomTypeManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends Booking>)newValue);
				return;
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER:
				setRoomManager((IRoomBookingManagerProvides)newValue);
				return;
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeBookingManagerProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				return;
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER:
				setRoomManager((IRoomBookingManagerProvides)null);
				return;
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeBookingManagerProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.BOOKING_MANAGER__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_MANAGER:
				return roomManager != null;
			case BookingPackagePackage.BOOKING_MANAGER__ROOM_TYPE_MANAGER:
				return roomTypeManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IBookingStartupProvides.class) {
			switch (baseOperationID) {
				case BookingPackagePackage.IBOOKING_STARTUP_PROVIDES___REMOVE_ALL_BOOKINGS: return BookingPackagePackage.BOOKING_MANAGER___REMOVE_ALL_BOOKINGS;
				default: return -1;
			}
		}
		if (baseClass == IBookingReceptionistProvides.class) {
			switch (baseOperationID) {
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST: return BookingPackagePackage.BOOKING_MANAGER___CHECK_IN_BOOKING__INT_ELIST;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___GET_RESERVED_ROOM_TYPES__INT: return BookingPackagePackage.BOOKING_MANAGER___GET_RESERVED_ROOM_TYPES__INT;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING: return BookingPackagePackage.BOOKING_MANAGER___ADD_ROOM__INT_STRING;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING: return BookingPackagePackage.BOOKING_MANAGER___REMOVE_ROOM__INT_STRING;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_DATE_DATE: return BookingPackagePackage.BOOKING_MANAGER___UPDATE_TIME_PERIOD__INT_DATE_DATE;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT: return BookingPackagePackage.BOOKING_MANAGER___CANCEL_BOOKING__INT;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___GET_CONFIRMED_BOOKINGS: return BookingPackagePackage.BOOKING_MANAGER___GET_CONFIRMED_BOOKINGS;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__DATE: return BookingPackagePackage.BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__DATE;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING: return BookingPackagePackage.BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING: return BookingPackagePackage.BOOKING_MANAGER___LIST_CHECK_OUTS__STRING_STRING;
				case BookingPackagePackage.IBOOKING_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE: return BookingPackagePackage.BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackagePackage.BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackagePackage.BOOKING_MANAGER___GET_RESERVATIONS__DATE_DATE:
				return getReservations((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___ADD_ROOM_BEFORE_CONFIRMATION__INT_STRING:
				return addRoomBeforeConfirmation((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___MARK_BOOKING_AS_PAID__INT:
				return markBookingAsPaid((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___REMOVE_ALL_BOOKINGS:
				removeAllBookings();
				return null;
			case BookingPackagePackage.BOOKING_MANAGER___CHECK_IN_BOOKING__INT_ELIST:
				return checkInBooking((Integer)arguments.get(0), (EList<Integer>)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___GET_RESERVED_ROOM_TYPES__INT:
				return getReservedRoomTypes((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___REMOVE_ROOM__INT_STRING:
				return removeRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___UPDATE_TIME_PERIOD__INT_DATE_DATE:
				return updateTimePeriod((Integer)arguments.get(0), (Date)arguments.get(1), (Date)arguments.get(2));
			case BookingPackagePackage.BOOKING_MANAGER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___GET_CONFIRMED_BOOKINGS:
				return getConfirmedBookings();
			case BookingPackagePackage.BOOKING_MANAGER___LIST_OCCUPIED_ROOMS__DATE:
				return listOccupiedRooms((Date)arguments.get(0));
			case BookingPackagePackage.BOOKING_MANAGER___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___LIST_CHECK_OUTS__STRING_STRING:
				return listCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackagePackage.BOOKING_MANAGER___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE:
				return addExtraCost((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingManagerImpl
