/**
 */
package se.chalmers.cse.mdsd1617.group02.BookingPackage.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackagePackage;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.Extra;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.RoomReservation;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.util.DateUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl#getCheckedIn <em>Checked In</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl#getCheckedOut <em>Checked Out</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl#getExtras <em>Extras</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingPackage.impl.RoomReservationImpl#getRoomType <em>Room Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomReservationImpl extends MinimalEObjectImpl.Container implements RoomReservation {
	/**
	 * The default value of the '{@link #getCheckedIn() <em>Checked In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedIn()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECKED_IN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckedIn() <em>Checked In</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedIn()
	 * @generated
	 * @ordered
	 */
	protected Date checkedIn = CHECKED_IN_EDEFAULT;

	/**
	 * The default value of the '{@link #getCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected static final Date CHECKED_OUT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected Date checkedOut = CHECKED_OUT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExtras() <em>Extras</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtras()
	 * @generated
	 * @ordered
	 */
	protected EList<Extra> extras;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected IRoom room;

	/**
	 * The cached value of the '{@link #getRoomType() <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomType()
	 * @generated
	 * @ordered
	 */
	protected IRoomType roomType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomReservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomReservationImpl(IRoomType roomtype) {
		super();
		setRoomType(roomtype);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackagePackage.Literals.ROOM_RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckedIn() {
		return checkedIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckedIn(Date newCheckedIn) {
		Date oldCheckedIn = checkedIn;
		checkedIn = newCheckedIn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.ROOM_RESERVATION__CHECKED_IN, oldCheckedIn, checkedIn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCheckedOut() {
		return checkedOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckedOut(Date newCheckedOut) {
		Date oldCheckedOut = checkedOut;
		checkedOut = newCheckedOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.ROOM_RESERVATION__CHECKED_OUT, oldCheckedOut, checkedOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extra> getExtras() {
		if (extras == null) {
			extras = new EObjectResolvingEList<Extra>(Extra.class, this, BookingPackagePackage.ROOM_RESERVATION__EXTRAS);
		}
		return extras;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoom getRoom() {
		if (room != null && room.eIsProxy()) {
			InternalEObject oldRoom = (InternalEObject)room;
			room = (IRoom)eResolveProxy(oldRoom);
			if (room != oldRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackagePackage.ROOM_RESERVATION__ROOM, oldRoom, room));
			}
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoom basicGetRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(IRoom newRoom) {
		IRoom oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.ROOM_RESERVATION__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType getRoomType() {
		if (roomType != null && roomType.eIsProxy()) {
			InternalEObject oldRoomType = (InternalEObject)roomType;
			roomType = (IRoomType)eResolveProxy(oldRoomType);
			if (roomType != oldRoomType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE, oldRoomType, roomType));
			}
		}
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType basicGetRoomType() {
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomType(IRoomType newRoomType) {
		IRoomType oldRoomType = roomType;
		roomType = newRoomType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE, oldRoomType, roomType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns the extras as a list of IExtra
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IExtra> getIExtras() {
		EList<IExtra> iExtras = new BasicEList<>();
		for (Extra extra : getExtras()) {
			iExtras.add(extra);
		}
		return iExtras;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double checkOut() {
		setCheckedOut(new Date());
		long days = DateUtil.daysBetween(getCheckedIn(), getCheckedOut());
		double price = roomType.getPrice() * days;
		for (Extra e : getExtras()){
			price += e.getPrice();
		}
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds an extra to this reservation.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtra(String description, double price) {
		getExtras().add(new ExtraImpl(description, price));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkIn(IRoom room) {
		setRoom(room);
		setCheckedIn(new Date());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_IN:
				return getCheckedIn();
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_OUT:
				return getCheckedOut();
			case BookingPackagePackage.ROOM_RESERVATION__EXTRAS:
				return getExtras();
			case BookingPackagePackage.ROOM_RESERVATION__ROOM:
				if (resolve) return getRoom();
				return basicGetRoom();
			case BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE:
				if (resolve) return getRoomType();
				return basicGetRoomType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_IN:
				setCheckedIn((Date)newValue);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_OUT:
				setCheckedOut((Date)newValue);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__EXTRAS:
				getExtras().clear();
				getExtras().addAll((Collection<? extends Extra>)newValue);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__ROOM:
				setRoom((IRoom)newValue);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE:
				setRoomType((IRoomType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_IN:
				setCheckedIn(CHECKED_IN_EDEFAULT);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_OUT:
				setCheckedOut(CHECKED_OUT_EDEFAULT);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__EXTRAS:
				getExtras().clear();
				return;
			case BookingPackagePackage.ROOM_RESERVATION__ROOM:
				setRoom((IRoom)null);
				return;
			case BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE:
				setRoomType((IRoomType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_IN:
				return CHECKED_IN_EDEFAULT == null ? checkedIn != null : !CHECKED_IN_EDEFAULT.equals(checkedIn);
			case BookingPackagePackage.ROOM_RESERVATION__CHECKED_OUT:
				return CHECKED_OUT_EDEFAULT == null ? checkedOut != null : !CHECKED_OUT_EDEFAULT.equals(checkedOut);
			case BookingPackagePackage.ROOM_RESERVATION__EXTRAS:
				return extras != null && !extras.isEmpty();
			case BookingPackagePackage.ROOM_RESERVATION__ROOM:
				return room != null;
			case BookingPackagePackage.ROOM_RESERVATION__ROOM_TYPE:
				return roomType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackagePackage.ROOM_RESERVATION___GET_IEXTRAS:
				return getIExtras();
			case BookingPackagePackage.ROOM_RESERVATION___CHECK_OUT:
				return checkOut();
			case BookingPackagePackage.ROOM_RESERVATION___ADD_EXTRA__STRING_DOUBLE:
				addExtra((String)arguments.get(0), (Double)arguments.get(1));
				return null;
			case BookingPackagePackage.ROOM_RESERVATION___CHECK_IN__IROOM:
				checkIn((IRoom)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checkedIn: ");
		result.append(checkedIn);
		result.append(", checkedOut: ");
		result.append(checkedOut);
		result.append(')');
		return result.toString();
	}

} //RoomReservationImpl
