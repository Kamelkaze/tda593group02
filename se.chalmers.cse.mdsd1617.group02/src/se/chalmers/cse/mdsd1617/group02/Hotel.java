/**
 */
package se.chalmers.cse.mdsd1617.group02;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.Hotel#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.Hotel#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.Hotel#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotel()
 * @model
 * @generated
 */
public interface Hotel extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * The singleton instance of the hotel.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	Hotel INSTANCE = Group02Factory.eINSTANCE.createHotel();
	
	/**
	 * Returns the value of the '<em><b>Room Type Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Manager</em>' reference.
	 * @see #setRoomTypeManager(RoomTypeManager)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotel_RoomTypeManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomTypeManager getRoomTypeManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.Hotel#getRoomTypeManager <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Manager</em>' reference.
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	void setRoomTypeManager(RoomTypeManager value);

	/**
	 * Returns the value of the '<em><b>Room Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Manager</em>' reference.
	 * @see #setRoomManager(RoomManager)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotel_RoomManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomManager getRoomManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.Hotel#getRoomManager <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Manager</em>' reference.
	 * @see #getRoomManager()
	 * @generated
	 */
	void setRoomManager(RoomManager value);

	/**
	 * Returns the value of the '<em><b>Booking Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Manager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Manager</em>' reference.
	 * @see #setBookingManager(BookingManager)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getHotel_BookingManager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingManager getBookingManager();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.Hotel#getBookingManager <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Manager</em>' reference.
	 * @see #getBookingManager()
	 * @generated
	 */
	void setBookingManager(BookingManager value);

} // Hotel
