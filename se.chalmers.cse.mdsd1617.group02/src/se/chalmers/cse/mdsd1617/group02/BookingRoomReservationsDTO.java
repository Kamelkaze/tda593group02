/**
 */
package se.chalmers.cse.mdsd1617.group02;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking Room Reservations DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getBookingRoomReservationsDTO()
 * @model
 * @generated
 */
public interface BookingRoomReservationsDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' attribute.
	 * @see #setRoomType(String)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getBookingRoomReservationsDTO_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO#getRoomType <em>Room Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' attribute.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(String value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(int)
	 * @see se.chalmers.cse.mdsd1617.group02.Group02Package#getBookingRoomReservationsDTO_Count()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getCount();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(int value);

} // BookingRoomReservationsDTO
