package se.chalmers.cse.mdsd1617.group02.test;

import static org.junit.Assert.*;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group02.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group02.Group02Factory;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;

public class HotelCustomerProvidesImplTest {
	
	private HotelAdministratorProvides administrator;
	private HotelCustomerProvides customer;
	private HotelStartupProvides startup;
	private HotelReceptionistProvides receptionist;

	@Before
	public void initializeTests() {
		RoomTypeManager roomTypeManager = RoomTypePackageFactory.eINSTANCE.createRoomTypeManager();
		RoomManager roomManager = RoomPackageFactory.eINSTANCE.createRoomManager(roomTypeManager);
		BookingManager bookingManager = BookingPackageFactory.eINSTANCE.createBookingManager(roomTypeManager, roomManager);
		Hotel.INSTANCE.setRoomTypeManager(roomTypeManager);
		Hotel.INSTANCE.setRoomManager(roomManager);
		Hotel.INSTANCE.setBookingManager(bookingManager);
		
		administrator = Group02Factory.eINSTANCE.createHotelAdministratorProvides();
		customer = Group02Factory.eINSTANCE.createHotelCustomerProvides();
		startup = Group02Factory.eINSTANCE.createHotelStartupProvides();
		receptionist = Group02Factory.eINSTANCE.createHotelReceptionistProvides();
	}
	
	@Test
	public void testMakeABooking() {
		startup.startup(1);
		String startDate = "2016-12-15";
		String endDate = "2016-12-17";
		int bookingId = customer.initiateBooking("Firstname", startDate, endDate, "Lastname");
		EList<FreeRoomTypesDTO> freeRooms = customer.getFreeRooms(2, startDate, endDate);
		FreeRoomTypesDTO addRoom = freeRooms.get(0);
		customer.addRoomToBooking(addRoom.getRoomTypeDescription(), bookingId);
		assertTrue (customer.confirmBooking(bookingId));
	}
	
	@Test
	public void testSearchForFreeRooms() {
		int numberOfRooms = 5;
		startup.startup(numberOfRooms);
		String startDate = "2016-12-15";
		String endDate = "2016-12-17";
		EList<FreeRoomTypesDTO> freeRooms = customer.getFreeRooms(2, startDate, endDate);
		assertTrue (freeRooms.get(0).getNumFreeRooms() == numberOfRooms);
	}
	
	@Test
	public void testCheckOutBooking(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		
		int roomNumber2 = 2;
		String roomType2 = "RoomType2";
		int numberOfBeds2 = 3;
		double price2 = 20;
		
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoomType(roomType2, numberOfBeds2, price2, null);
		administrator.addRoom(roomNumber, roomType);
		administrator.addRoom(roomNumber2, roomType2);
		String firstName = "a", lastName = "b", startDate = "20160101", endDate = "20190102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		receptionist.addRoomToBooking(roomType2, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		roomNumbers.add(roomNumber2);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		receptionist.checkInBooking(bookingID, roomNumbers);
		
		String extraDescr = "Cookies";
		double extraPrice = 5;
		receptionist.addExtraCost(bookingID, roomNumber, extraDescr, extraPrice);
		
		double p = receptionist.initiateCheckout(bookingID);
		assertTrue(p == price + price2 + extraPrice);
		
		String ccNumber = "0213";
		String ccv = "111";
		int expiryMonth = 2;
		int expiryYear = 17;
		se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bank = null;
		try {
			bank = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bank.addCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bank.makeDeposit(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, p);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(receptionist.payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName));
		
		assertTrue(b.getStatus().equals(BookingStatus.CHECKED_OUT));
		
		assertTrue(b.getIReservations().get(0).getRoom().getStatus().equals(RoomStatus.FREE));
		assertTrue(b.getIReservations().get(1).getRoom().getStatus().equals(RoomStatus.FREE));
	}
	
	@Test
	public void testCheckInRoom() {
		startup.startup(1);
		String startDate = "2016-12-15";
		String endDate = "2016-12-17";
		int bookingId = customer.initiateBooking("Firstname", startDate, endDate, "Lastname");
		EList<FreeRoomTypesDTO> freeRooms = customer.getFreeRooms(2, startDate, endDate);
		FreeRoomTypesDTO addRoom = freeRooms.get(0);
		customer.addRoomToBooking(addRoom.getRoomTypeDescription(), bookingId);
		customer.confirmBooking(bookingId);
		int roomNumber = customer.checkInRoom(addRoom.getRoomTypeDescription(), bookingId);
		assertTrue(roomNumber == 1);
	}
	
	@Test
	public void testCheckoutAndPayRoom() {
		startup.startup(1);
		String firstName = "Firstname";
		String lastName = "Lastname";
		String startDate = "2016-12-15";
		String endDate = "2019-12-17";
		int bookingId = customer.initiateBooking(firstName, startDate, endDate, lastName);
		EList<FreeRoomTypesDTO> freeRooms = customer.getFreeRooms(2, startDate, endDate);
		FreeRoomTypesDTO addRoom = freeRooms.get(0);
		customer.addRoomToBooking(addRoom.getRoomTypeDescription(), bookingId);
		customer.confirmBooking(bookingId);
		int roomNumber = customer.checkInRoom(addRoom.getRoomTypeDescription(), bookingId);
		
		double price = customer.initiateRoomCheckout(roomNumber, bookingId);
		
		String ccNumber = "0200";
		String ccv = "111";
		int expiryMonth = 2;
		int expiryYear = 17;
		se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bank = null;
		try {
			bank = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bank.addCreditCard(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			bank.makeDeposit(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean paid = customer.payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		
		assertTrue(paid);
		
	}
	
	@Test
	public void testCheckoutAndPayRoom2() {
		startup.startup(1);
		
		int bookingId = customer.initiateBooking("First name", "20170101", "20170102", "Last name");
		assertEquals(bookingId, 1);
		assertTrue(customer.addRoomToBooking("Standard", bookingId));
		assertTrue(customer.confirmBooking(bookingId));
		int roomNumber = customer.checkInRoom("Standard", bookingId);
		assertEquals(roomNumber, 1);
		double price = customer.initiateRoomCheckout(roomNumber, bookingId);
		assertEquals(price, 100.0, 0.01);
	}
}
