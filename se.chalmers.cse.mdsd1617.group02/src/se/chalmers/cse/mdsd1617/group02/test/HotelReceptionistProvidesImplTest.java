package se.chalmers.cse.mdsd1617.group02.test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group02.BookingDTO;
import se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO;
import se.chalmers.cse.mdsd1617.group02.Group02Factory;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingStatus;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IExtra;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;

public class HotelReceptionistProvidesImplTest {
	
	private HotelAdministratorProvides administrator;
	private HotelStartupProvides startup;
	private HotelReceptionistProvides receptionist;

	@Before
	public void initializeTests() {
		RoomTypeManager roomTypeManager = RoomTypePackageFactory.eINSTANCE.createRoomTypeManager();
		RoomManager roomManager = RoomPackageFactory.eINSTANCE.createRoomManager(roomTypeManager);
		BookingManager bookingManager = BookingPackageFactory.eINSTANCE.createBookingManager(roomTypeManager, roomManager);
		Hotel.INSTANCE.setRoomTypeManager(roomTypeManager);
		Hotel.INSTANCE.setRoomManager(roomManager);
		Hotel.INSTANCE.setBookingManager(bookingManager);
		
		administrator = Group02Factory.eINSTANCE.createHotelAdministratorProvides();
		startup = Group02Factory.eINSTANCE.createHotelStartupProvides();
		receptionist = Group02Factory.eINSTANCE.createHotelReceptionistProvides();
	}
	


	@Test
	public void testConfirmBooking() {
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = "20140101", endDate = "20140102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		DateFormat f = new SimpleDateFormat("YYYYMMDD");
		
		assertTrue(b.getId() == bookingID);
		assertTrue(b.getICustomer().getFirstName().contentEquals(firstName));
		assertTrue(b.getICustomer().getLastName().contentEquals(lastName));
		assertTrue(b.getIReservations().get(0).getRoomType().getPrice() == price);
		assertTrue(b.getIReservations().get(0).getRoomType().getName().contentEquals(roomType));
		assertTrue(b.getIReservations().get(0).getRoomType().getNumberOfBeds() == numberOfBeds);	
		assertTrue(f.format(b.getStartDate()).contentEquals(startDate));
		assertTrue(f.format(b.getEndDate()).contentEquals(endDate));
	}
	
	@Test
	public void testCheckInBooking(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = "20160101", endDate = "20190102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		receptionist.checkInBooking(bookingID, roomNumbers);
		
		assertTrue(b.getStatus().equals(BookingStatus.CHECKED_IN));
		assertTrue(b.getIReservations().get(0).getRoom().getStatus().equals(RoomStatus.OCCUPIED));
	}
	
	@Test
	public void testEditBooking(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		
		int roomNumber2 = 2;
		String roomType2 = "RoomType2";
		int numberOfBeds2 = 3;
		double price2 = 20;
		
		int roomNumber3 = 3;
		
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoomType(roomType2, numberOfBeds2, price2, null);
		administrator.addRoom(roomNumber, roomType);
		administrator.addRoom(roomNumber3, roomType);
		administrator.addRoom(roomNumber2, roomType2);
		String firstName = "a", lastName = "b", startDate = "20150101", endDate = "20150102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		DateFormat f = new SimpleDateFormat("YYYYMMDD");
		
		assertTrue(b.getId() == bookingID);
		assertTrue(b.getICustomer().getFirstName().contentEquals(firstName));
		assertTrue(b.getICustomer().getLastName().contentEquals(lastName));
		assertTrue(b.getIReservations().get(0).getRoomType().getPrice() == price);
		assertTrue(b.getIReservations().get(0).getRoomType().getName().contentEquals(roomType));
		assertTrue(b.getIReservations().get(0).getRoomType().getNumberOfBeds() == numberOfBeds);
		assertTrue(f.format(b.getStartDate()).contentEquals(startDate));
		assertTrue(f.format(b.getEndDate()).contentEquals(endDate));	
		
		String newStartDate = "20140101", newEndDate = "20140102";
		
		receptionist.updateTimePeriod(bookingID, newStartDate, newEndDate);
		
		assertTrue(f.format(b.getStartDate()).contentEquals(newStartDate));
		assertTrue(f.format(b.getEndDate()).contentEquals(newEndDate));
		
		receptionist.removeRoom(bookingID, roomType);
		receptionist.addRoom(bookingID, roomType2);
		
		assertTrue(b.getIReservations().get(0).getRoomType().getName().contentEquals(roomType2));
		
		receptionist.addRoom(bookingID, roomType);
		receptionist.addRoom(bookingID, roomType);
		int count = 0;
		for (IRoomReservation r : b.getIReservations()){
			if (r.getRoomType().getName().contentEquals(roomType))
				count++;
		}
		assertTrue(count == 2);
	}
	
	@Test
	public void testListBookings() {
		startup.startup(2);
		administrator.addRoomType("Suite", 4, 200, null);
		administrator.addRoom(10, "Suite");
		
		String booking1StartDate = "20161218";
		String booking1EndDate = "20161220";
		
		int bookingId1 = receptionist.initiateBooking("FName1", booking1StartDate, booking1EndDate, "LName1");
		receptionist.addRoomToBooking("Suite", bookingId1);
		receptionist.addRoomToBooking("Standard", bookingId1);
		receptionist.confirmBooking(bookingId1);
		
		String booking2StartDate = "20161219";
		String booking2EndDate = "20161224";
		
		int bookingId2 = receptionist.initiateBooking("FName2", booking2StartDate, booking2EndDate, "LName2");
		receptionist.addRoomToBooking("Standard", bookingId2);
		receptionist.confirmBooking(bookingId2);
		
		EList<BookingDTO> bookings = receptionist.listBookings();
		
		assertTrue(bookings.size() == 2);
		
		assertTrue(bookings.get(0).getBookingId() == bookingId1 &&
			   bookings.get(0).getStartDate().equals(booking1StartDate) &&
			   bookings.get(0).getEndDate().equals(booking1EndDate) &&
			   bookings.get(0).getReservations().get(0).getCount() == 1 &&
			   bookings.get(0).getReservations().get(1).getCount() == 1);
		
		boolean containsSuite = false, containsStandard = false;
		
		for (BookingRoomReservationsDTO r : bookings.get(0).getReservations()){
			if (r.getRoomType().contentEquals("Suite"))
				containsSuite = true;
			else if (r.getRoomType().contentEquals("Standard"))
				containsStandard = true;
		}
		
		assertTrue(containsSuite);
		assertTrue(containsStandard);
			   
		assertTrue(bookings.get(1).getBookingId() == bookingId2 &&
				bookings.get(1).getStartDate().equals(booking2StartDate) &&
				bookings.get(1).getEndDate().equals(booking2EndDate) &&
				bookings.get(1).getReservations().get(0).getRoomType().equalsIgnoreCase("Standard") &&
				bookings.get(1).getReservations().get(0).getCount() == 1);
	}	
	
	public void testListOccupiedRoomsForSpecifiedDay() {
		startup.startup(0);
		administrator.addRoom(1, "Standard");
		administrator.addRoom(2, "Standard");
		administrator.addRoom(3, "Standard");

		String booking1StartDate = "20161218";
		String booking1EndDate = "20161220";
		
		int bookingId1 = receptionist.initiateBooking("FName1", booking1StartDate, booking1EndDate, "LName1");
		receptionist.addRoomToBooking("Standard", bookingId1);
		receptionist.addRoomToBooking("Standard", bookingId1);
		receptionist.confirmBooking(bookingId1);
		
		String booking2StartDate = "20161219";
		String booking2EndDate = "20161224";
		
		int bookingId2 = receptionist.initiateBooking("FName2", booking2StartDate, booking2EndDate, "LName2");
		receptionist.addRoomToBooking("Standard", bookingId2);
		receptionist.confirmBooking(bookingId2);
		
		EList<OccupiedRoomDTO> occupiedRooms = receptionist.listOccupiedRooms("20161218");
		
		assertTrue(occupiedRooms.size() == 2 &&
				occupiedRooms.get(0).getBookingId() == bookingId1 &&
				occupiedRooms.get(0).getRoomNumber() == 1 &&
				occupiedRooms.get(1).getBookingId() == bookingId2 &&
				occupiedRooms.get(1).getRoomNumber() == 2);
	}
	
	@Test
	public void testCancelBooking(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = "20140101", endDate = "20140102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		receptionist.cancelBooking(bookingID);
		
		assertTrue(receptionist.getBookingManager().getConfirmedBookings().size() == 0);
		assertTrue(b.getStatus().equals(BookingStatus.CANCELLED));
		assertTrue(b.getIReservations().size() == 0);
		
		int bookingID2 = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID2);
		receptionist.confirmBooking(bookingID2);
		boolean bool = receptionist.checkInBooking(bookingID2, roomNumbers);
		assertTrue(bool);
	}
	
	@Test
	public void testListCheckIns(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		DateFormat f = new SimpleDateFormat("yyyyMMdd");
		Date bDate = new Date();
		Date eDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(eDate);
		cal.add(Calendar.DATE, 1);
		eDate = cal.getTime();
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = f.format(bDate), endDate = f.format(eDate);
		int bookingID = receptionist.initiateBooking(firstName, "20160101", "20190101", lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		receptionist.checkInBooking(bookingID, roomNumbers);
		CheckInDTO checkIn = receptionist.listCheckIns(startDate, endDate).get(0);
		
		assertTrue(checkIn.getBookingId() == bookingID);
		assertTrue(checkIn.getRoomNumber() == roomNumber);
	}
	
	@Test
	public void testListCheckOuts(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		DateFormat f = new SimpleDateFormat("yyyyMMdd");
		Date bDate = new Date();
		Date eDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(eDate);
		cal.add(Calendar.DATE, 1);
		eDate = cal.getTime();
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = f.format(bDate), endDate = f.format(eDate);
		int bookingID = receptionist.initiateBooking(firstName, "20160101", "20190101", lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		receptionist.checkInBooking(bookingID, roomNumbers);
		receptionist.initiateCheckout(bookingID);
		CheckOutDTO checkOut = receptionist.listCheckOuts(startDate, endDate).get(0);
		
		assertTrue(checkOut.getBookingId() == bookingID);
		assertTrue(checkOut.getRoomNumber() == roomNumber);
	}
	
	@Test
	public void testAddExtra(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		String firstName = "a", lastName = "b", startDate = "20160101", endDate = "20190102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber);
		receptionist.confirmBooking(bookingID);
		IBooking b = receptionist.getBookingManager().getConfirmedBookings().get(0);
		receptionist.checkInBooking(bookingID, roomNumbers);
		String extraDescr = "Cookies";
		double extraPrice = 5;
		receptionist.addExtraCost(bookingID, roomNumber, extraDescr, extraPrice);
		IExtra e = b.getIReservations().get(0).getIExtras().get(0);
		
		assertTrue(b.getIReservations().get(0).getRoom().getNumber() == roomNumber);
		assertTrue(e.getDescription().contentEquals(extraDescr));
		assertTrue(e.getPrice() == extraPrice);
	}

}
