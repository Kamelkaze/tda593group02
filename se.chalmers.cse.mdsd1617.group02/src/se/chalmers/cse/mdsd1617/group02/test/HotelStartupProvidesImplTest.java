package se.chalmers.cse.mdsd1617.group02.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group02.Group02Factory;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;

public class HotelStartupProvidesImplTest {
	
	private HotelStartupProvides startup;
	
	@Before
	public void initializeTests() {
		RoomTypeManager roomTypeManager = RoomTypePackageFactory.eINSTANCE.createRoomTypeManager();
		RoomManager roomManager = RoomPackageFactory.eINSTANCE.createRoomManager(roomTypeManager);
		BookingManager bookingManager = BookingPackageFactory.eINSTANCE.createBookingManager(roomTypeManager, roomManager);
		Hotel.INSTANCE.setRoomTypeManager(roomTypeManager);
		Hotel.INSTANCE.setRoomManager(roomManager);
		Hotel.INSTANCE.setBookingManager(bookingManager);
		
		startup = Group02Factory.eINSTANCE.createHotelStartupProvides();
	}

	@Test
	public void testStartup() {
		startup.startup(5);
		assertTrue(Hotel.INSTANCE.getRoomManager().getRooms().size() == 5 &&
				   Hotel.INSTANCE.getRoomTypeManager().getRoomTypes().size() == 1 &&
				   Hotel.INSTANCE.getRoomTypeManager().getRoomTypes().get(0).getName() == "Standard" &&
				   Hotel.INSTANCE.getBookingManager().getBookings().isEmpty());
	}

}
