package se.chalmers.cse.mdsd1617.group02.test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group02.Group02Factory;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides;
import se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomStatus;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;

public class HotelAdministratorProvidesImplTest {
	
	private HotelAdministratorProvides administrator;
	private HotelCustomerProvides customer;
	private HotelStartupProvides startup;
	private HotelReceptionistProvides receptionist;
	
	@Before
	public void initializeTests() {
		RoomTypeManager roomTypeManager = RoomTypePackageFactory.eINSTANCE.createRoomTypeManager();
		RoomManager roomManager = RoomPackageFactory.eINSTANCE.createRoomManager(roomTypeManager);
		BookingManager bookingManager = BookingPackageFactory.eINSTANCE.createBookingManager(roomTypeManager, roomManager);
		Hotel.INSTANCE.setRoomTypeManager(roomTypeManager);
		Hotel.INSTANCE.setRoomManager(roomManager);
		Hotel.INSTANCE.setBookingManager(bookingManager);
		
		administrator = Group02Factory.eINSTANCE.createHotelAdministratorProvides();
		customer = Group02Factory.eINSTANCE.createHotelCustomerProvides();
		startup = Group02Factory.eINSTANCE.createHotelStartupProvides();
		receptionist = Group02Factory.eINSTANCE.createHotelReceptionistProvides();
	}
	
	@Test
	public void testAddRoomType(){
		String name = "RoomTypeTest";
		int numberOfBeds = 2;
		double price = 10;
		EList<String> features = new BasicEList<String>();
		features.add("Feature1");
		features.add("Feature2");
		Boolean bool = administrator.addRoomType(name, numberOfBeds, price, features);
		IRoomType roomType = Hotel.INSTANCE.getRoomTypeManager().getRoomType(name);
		
		Boolean bool2 = administrator.addRoomType(name, 5, 50, null);
		
		assertTrue(bool);
		assertTrue(roomType.getName().equalsIgnoreCase(name));
		assertTrue(roomType.getNumberOfBeds() == numberOfBeds);
		assertTrue(roomType.getPrice() == price);
		assertTrue(roomType.getFeatures().equals(features));
		assertFalse(bool2);
	}
	
	@Test
	public void testUpdateRoomType(){
		String name = "RoomTypeTest";
		int numberOfBeds = 2;
		double price = 10;
		EList<String> features = new BasicEList<String>();
		features.add("Feature1");
		features.add("Feature2");
		administrator.addRoomType(name, numberOfBeds, price, features);
		IRoomType roomType = Hotel.INSTANCE.getRoomTypeManager().getRoomType(name);
		
		assertTrue(roomType.getName().equalsIgnoreCase(name));
		assertTrue(roomType.getNumberOfBeds() == numberOfBeds);
		assertTrue(roomType.getPrice() == price);
		assertTrue(roomType.getFeatures().equals(features));
		
		String newName = "RoomTypeTest2";
		numberOfBeds = 3;
		price = 20;
		features.clear();
		features.add("Feature3");
		features.add("Feature4");
		administrator.updateRoomType(name, newName, numberOfBeds, price, features);
		roomType = Hotel.INSTANCE.getRoomTypeManager().getRoomType(newName);

		assertTrue(roomType.getName().equalsIgnoreCase(newName));
		assertTrue(roomType.getNumberOfBeds() == numberOfBeds);
		assertTrue(roomType.getPrice() == price);
		assertTrue(roomType.getFeatures().equals(features));
		assertTrue(Hotel.INSTANCE.getRoomTypeManager().getRoomType(name) == null);
	}
	
	@Test
	public void testRemoveRoomType(){
		String name = "RoomTypeTest";
		int numberOfBeds = 2;
		double price = 10;
		EList<String> features = new BasicEList<String>();
		features.add("Feature1");
		features.add("Feature2");
		administrator.addRoomType(name, numberOfBeds, price, features);
		IRoomType roomType = Hotel.INSTANCE.getRoomTypeManager().getRoomType(name);
		
		assertTrue(roomType.getName().equalsIgnoreCase(name));
		assertTrue(roomType.getNumberOfBeds() == numberOfBeds);
		assertTrue(roomType.getPrice() == price);
		assertTrue(roomType.getFeatures().equals(features));
		
		administrator.removeRoomType(name);
		roomType = Hotel.INSTANCE.getRoomTypeManager().getRoomType(name);
		assertTrue(roomType == null);
	}
	
	
	@Test
	public void testHotelStartup() {
		String roomTypeName = "RoomTypeTest";
		int numberOfBeds = 2;
		double price = 10;
		EList<String> features = new BasicEList<String>();
		features.add("Feature1");
		features.add("Feature2");
		administrator.addRoomType(roomTypeName, numberOfBeds, price, features);
		int roomNumber1 = 1;
		int roomNumber2 = 2;
		administrator.addRoom(roomNumber1, roomTypeName);
		administrator.addRoom(roomNumber2, roomTypeName);
		
		assertEquals(administrator.getRoomTypeManager().getAllRoomTypes().size(), 1);
		assertEquals(customer.getHotelRoomManager().getAllRooms().size(), 2);
		
		//there are now 2 room types and 2 rooms
		
		startup.startup(3);
		
		assertTrue(administrator.getRoomTypeManager().getAllRoomTypes().size() == 1);
		assertTrue(Hotel.INSTANCE.getRoomTypeManager().getRoomType("Standard") != null);
		assertTrue(customer.getHotelRoomManager().getAllRooms().size() == 3);
		
	}
	
	@Test
	public void testAddRoom(){
		int roomNumber = 1, roomNumber2 = 2;
		String roomType = "RoomType1", roomType2 = "RoomType2";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoomType(roomType2, numberOfBeds, price, null);
		Boolean b1 = administrator.addRoom(roomNumber, roomType);
		Boolean b2 = administrator.addRoom(roomNumber2, roomType2);
		Boolean b3 = administrator.addRoom(roomNumber2, roomType);
		
		assertTrue(b1);
		assertTrue(b2);
		assertFalse(b3);
		assertTrue(administrator.getRoomManager().roomOfRoomTypeExists(roomType));
		assertTrue(administrator.getRoomManager().roomOfRoomTypeExists(roomType2));
	}
	
	@Test
	public void testChangeRoomTypeOfRoom(){
		int roomNumber = 1;
		String roomType = "RoomType1", roomType2 = "RoomType2";;
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoomType(roomType2, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		IRoom room = Hotel.INSTANCE.getRoomManager().getRooms().get(0);
		
		assertTrue(room.getNumber() == roomNumber);
		assertTrue(room.getType().getName().contentEquals(roomType));

		administrator.changeRoomTypeOfRoom(roomNumber, roomType2);

		assertTrue(room.getType().getName().contentEquals(roomType2));
	}
	
	@Test
	public void testRemoveRoom(){
		int roomNumber = 1, roomNumber2 = 2;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		administrator.addRoom(roomNumber2, roomType);
		
		IRoom room = null;
		for (IRoom r : Hotel.INSTANCE.getRoomManager().getRooms())
			if (r.getNumber() == roomNumber)
				room = r;
		
		assertTrue(room.getNumber() == roomNumber);
				
		Boolean b = administrator.removeRoom(roomNumber);
		
		assertTrue(b);

		room = null;
		for (IRoom r : Hotel.INSTANCE.getRoomManager().getRooms())
			if (r.getNumber() == roomNumber)
				room = r;
		
		assertTrue(room == null);
		
		
		String firstName = "a", lastName = "b", startDate = "20160101", endDate = "20190102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		receptionist.addRoomToBooking(roomType, bookingID);
		EList<Integer> roomNumbers = new BasicEList<Integer>();
		roomNumbers.add(roomNumber2);
		receptionist.confirmBooking(bookingID);
		receptionist.checkInBooking(bookingID, roomNumbers);
		
		Boolean b2 = administrator.removeRoom(roomNumber2);
		assertFalse(b2);
	}
	
	@Test
	public void testBlockRoom(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		IRoom room = receptionist.getRoomManager().getFreeRoomsOfType(roomType).get(0);
		
		assertTrue(room.getStatus().equals(RoomStatus.FREE));
		
		administrator.blockRoom(roomNumber);
		
		assertTrue(room.getStatus().equals(RoomStatus.BLOCKED));
		
		String firstName = "a", lastName = "b", startDate = "20160101", endDate = "20190102";
		int bookingID = receptionist.initiateBooking(firstName, startDate, endDate, lastName);
		Boolean b = receptionist.addRoomToBooking(roomType, bookingID);
		
		assertFalse(b);
	}
	
	@Test
	public void testUnblockRoom(){
		int roomNumber = 1;
		String roomType = "RoomType1";
		int numberOfBeds = 2;
		double price = 10;
		administrator.addRoomType(roomType, numberOfBeds, price, null);
		administrator.addRoom(roomNumber, roomType);
		IRoom room = receptionist.getRoomManager().getFreeRoomsOfType(roomType).get(0);
		
		assertTrue(room.getStatus().equals(RoomStatus.FREE));
		
		administrator.blockRoom(roomNumber);
		
		assertTrue(room.getStatus().equals(RoomStatus.BLOCKED));
		
		administrator.unblockRoom(roomNumber);
		
		assertTrue(room.getStatus().equals(RoomStatus.FREE));
	}
}
