/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.Group02Package;
import se.chalmers.cse.mdsd1617.group02.Hotel;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelImpl#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelImpl#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelImpl#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelImpl extends MinimalEObjectImpl.Container implements Hotel {
	/**
	 * The cached value of the '{@link #getRoomTypeManager() <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeManager()
	 * @generated
	 * @ordered
	 */
	protected RoomTypeManager roomTypeManager;

	/**
	 * The cached value of the '{@link #getRoomManager() <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomManager()
	 * @generated
	 * @ordered
	 */
	protected RoomManager roomManager;

	/**
	 * The cached value of the '{@link #getBookingManager() <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingManager()
	 * @generated
	 * @ordered
	 */
	protected BookingManager bookingManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelImpl() {
		super();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelImpl(RoomTypeManager roomTypeManager, RoomManager roomManager, BookingManager bookingManager) {
		super();
		setRoomTypeManager(roomTypeManager);
		setRoomManager(roomManager);
		setBookingManager(bookingManager);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group02Package.Literals.HOTEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeManager getRoomTypeManager() {
		if (roomTypeManager != null && roomTypeManager.eIsProxy()) {
			InternalEObject oldRoomTypeManager = (InternalEObject)roomTypeManager;
			roomTypeManager = (RoomTypeManager)eResolveProxy(oldRoomTypeManager);
			if (roomTypeManager != oldRoomTypeManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
			}
		}
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeManager basicGetRoomTypeManager() {
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeManager(RoomTypeManager newRoomTypeManager) {
		RoomTypeManager oldRoomTypeManager = roomTypeManager;
		roomTypeManager = newRoomTypeManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager getRoomManager() {
		if (roomManager != null && roomManager.eIsProxy()) {
			InternalEObject oldRoomManager = (InternalEObject)roomManager;
			roomManager = (RoomManager)eResolveProxy(oldRoomManager);
			if (roomManager != oldRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL__ROOM_MANAGER, oldRoomManager, roomManager));
			}
		}
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager basicGetRoomManager() {
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomManager(RoomManager newRoomManager) {
		RoomManager oldRoomManager = roomManager;
		roomManager = newRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL__ROOM_MANAGER, oldRoomManager, roomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager getBookingManager() {
		if (bookingManager != null && bookingManager.eIsProxy()) {
			InternalEObject oldBookingManager = (InternalEObject)bookingManager;
			bookingManager = (BookingManager)eResolveProxy(oldBookingManager);
			if (bookingManager != oldBookingManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL__BOOKING_MANAGER, oldBookingManager, bookingManager));
			}
		}
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager basicGetBookingManager() {
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingManager(BookingManager newBookingManager) {
		BookingManager oldBookingManager = bookingManager;
		bookingManager = newBookingManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL__BOOKING_MANAGER, oldBookingManager, bookingManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group02Package.HOTEL__ROOM_TYPE_MANAGER:
				if (resolve) return getRoomTypeManager();
				return basicGetRoomTypeManager();
			case Group02Package.HOTEL__ROOM_MANAGER:
				if (resolve) return getRoomManager();
				return basicGetRoomManager();
			case Group02Package.HOTEL__BOOKING_MANAGER:
				if (resolve) return getBookingManager();
				return basicGetBookingManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group02Package.HOTEL__ROOM_TYPE_MANAGER:
				setRoomTypeManager((RoomTypeManager)newValue);
				return;
			case Group02Package.HOTEL__ROOM_MANAGER:
				setRoomManager((RoomManager)newValue);
				return;
			case Group02Package.HOTEL__BOOKING_MANAGER:
				setBookingManager((BookingManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL__ROOM_TYPE_MANAGER:
				setRoomTypeManager((RoomTypeManager)null);
				return;
			case Group02Package.HOTEL__ROOM_MANAGER:
				setRoomManager((RoomManager)null);
				return;
			case Group02Package.HOTEL__BOOKING_MANAGER:
				setBookingManager((BookingManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL__ROOM_TYPE_MANAGER:
				return roomTypeManager != null;
			case Group02Package.HOTEL__ROOM_MANAGER:
				return roomManager != null;
			case Group02Package.HOTEL__BOOKING_MANAGER:
				return bookingManager != null;
		}
		return super.eIsSet(featureID);
	}

} //HotelImpl
