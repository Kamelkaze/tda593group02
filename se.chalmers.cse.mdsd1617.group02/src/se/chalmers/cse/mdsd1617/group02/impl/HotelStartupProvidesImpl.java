/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingStartupProvides;

import se.chalmers.cse.mdsd1617.group02.Group02Package;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelStartupProvides;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomStartupProvides;

import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeStartupProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelStartupProvidesImpl#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelStartupProvidesImpl#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelStartupProvidesImpl#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	
	/**
	 * The cached value of the '{@link #getRoomTypeManager() <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeStartupProvides roomTypeManager;

	/**
	 * The cached value of the '{@link #getRoomManager() <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomStartupProvides roomManager;

	/**
	 * The cached value of the '{@link #getBookingManager() <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingManager()
	 * @generated
	 * @ordered
	 */
	protected IBookingStartupProvides bookingManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelStartupProvidesImpl() {
		super();
		setRoomTypeManager(Hotel.INSTANCE.getRoomTypeManager());
		setRoomManager(Hotel.INSTANCE.getRoomManager());
		setBookingManager(Hotel.INSTANCE.getBookingManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group02Package.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeStartupProvides getRoomTypeManager() {
		if (roomTypeManager != null && roomTypeManager.eIsProxy()) {
			InternalEObject oldRoomTypeManager = (InternalEObject)roomTypeManager;
			roomTypeManager = (IRoomTypeStartupProvides)eResolveProxy(oldRoomTypeManager);
			if (roomTypeManager != oldRoomTypeManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
			}
		}
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeStartupProvides basicGetRoomTypeManager() {
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeManager(IRoomTypeStartupProvides newRoomTypeManager) {
		IRoomTypeStartupProvides oldRoomTypeManager = roomTypeManager;
		roomTypeManager = newRoomTypeManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomStartupProvides getRoomManager() {
		if (roomManager != null && roomManager.eIsProxy()) {
			InternalEObject oldRoomManager = (InternalEObject)roomManager;
			roomManager = (IRoomStartupProvides)eResolveProxy(oldRoomManager);
			if (roomManager != oldRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
			}
		}
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomStartupProvides basicGetRoomManager() {
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomManager(IRoomStartupProvides newRoomManager) {
		IRoomStartupProvides oldRoomManager = roomManager;
		roomManager = newRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingStartupProvides getBookingManager() {
		if (bookingManager != null && bookingManager.eIsProxy()) {
			InternalEObject oldBookingManager = (InternalEObject)bookingManager;
			bookingManager = (IBookingStartupProvides)eResolveProxy(oldBookingManager);
			if (bookingManager != oldBookingManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER, oldBookingManager, bookingManager));
			}
		}
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingStartupProvides basicGetBookingManager() {
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingManager(IBookingStartupProvides newBookingManager) {
		IBookingStartupProvides oldBookingManager = bookingManager;
		bookingManager = newBookingManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER, oldBookingManager, bookingManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * RoomStartupProvides: removeAllRooms & addRoom
	 * RoomTypeStartupProvides: removeAllRoomTypes & addRoomType
	 * 		you must add roomtype before you add a room
	 * BookingStartupProvides: removeAllBookings
	 * 
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		
		getBookingManager().removeAllBookings();
		getRoomManager().removeAllRooms();
		getRoomTypeManager().removeAllRoomTypes();
		
		getRoomTypeManager().addRoomType("Standard", 2, 100, new BasicEList<String>());
		
		for (int i = 1; i <= numRooms; i++) {
			getRoomManager().addRoom(i, "Standard");
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER:
				if (resolve) return getRoomTypeManager();
				return basicGetRoomTypeManager();
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER:
				if (resolve) return getRoomManager();
				return basicGetRoomManager();
			case Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER:
				if (resolve) return getBookingManager();
				return basicGetBookingManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeStartupProvides)newValue);
				return;
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomStartupProvides)newValue);
				return;
			case Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER:
				setBookingManager((IBookingStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeStartupProvides)null);
				return;
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomStartupProvides)null);
				return;
			case Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER:
				setBookingManager((IBookingStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_TYPE_MANAGER:
				return roomTypeManager != null;
			case Group02Package.HOTEL_STARTUP_PROVIDES__ROOM_MANAGER:
				return roomManager != null;
			case Group02Package.HOTEL_STARTUP_PROVIDES__BOOKING_MANAGER:
				return bookingManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group02Package.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
