/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group02.*;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingManager;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.BookingPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomManager;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.RoomPackageFactory;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypeManager;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.RoomTypePackageFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Group02FactoryImpl extends EFactoryImpl implements Group02Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Group02Factory init() {
		try {
			Group02Factory theGroup02Factory = (Group02Factory)EPackage.Registry.INSTANCE.getEFactory(Group02Package.eNS_URI);
			if (theGroup02Factory != null) {
				return theGroup02Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Group02FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group02FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Group02Package.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			case Group02Package.ROOM_TYPE_DTO: return createRoomTypeDTO();
			case Group02Package.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			case Group02Package.AVAILABLE_ROOM_DTO: return createAvailableRoomDTO();
			case Group02Package.BOOKING_DTO: return createBookingDTO();
			case Group02Package.BOOKING_ROOM_RESERVATIONS_DTO: return createBookingRoomReservationsDTO();
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES: return createHotelReceptionistProvides();
			case Group02Package.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES: return createHotelAdministratorProvides();
			case Group02Package.HOTEL: return createHotel();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Hotel createHotel() {
		RoomTypeManager roomTypeManager = RoomTypePackageFactory.eINSTANCE.createRoomTypeManager();
		RoomManager roomManager = RoomPackageFactory.eINSTANCE.createRoomManager(roomTypeManager);
		BookingManager bookingManager = BookingPackageFactory.eINSTANCE.createBookingManager(roomTypeManager, roomManager);
		HotelImpl hotel = new HotelImpl(roomTypeManager, roomManager, bookingManager);
		return hotel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeDTO createRoomTypeDTO() {
		RoomTypeDTOImpl roomTypeDTO = new RoomTypeDTOImpl();
		return roomTypeDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AvailableRoomDTO createAvailableRoomDTO() {
		AvailableRoomDTOImpl availableRoomDTO = new AvailableRoomDTOImpl();
		return availableRoomDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingDTO createBookingDTO() {
		BookingDTOImpl bookingDTO = new BookingDTOImpl();
		return bookingDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingRoomReservationsDTO createBookingRoomReservationsDTO() {
		BookingRoomReservationsDTOImpl bookingRoomReservationsDTO = new BookingRoomReservationsDTOImpl();
		return bookingRoomReservationsDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelReceptionistProvides createHotelReceptionistProvides() {
		HotelReceptionistProvidesImpl hotelReceptionistProvides = new HotelReceptionistProvidesImpl();
		return hotelReceptionistProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		HotelCustomerProvidesImpl hotelCustomerProvides = new HotelCustomerProvidesImpl();
		return hotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelAdministratorProvides createHotelAdministratorProvides() {
		HotelAdministratorProvidesImpl hotelAdministratorProvides = new HotelAdministratorProvidesImpl();
		return hotelAdministratorProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group02Package getGroup02Package() {
		return (Group02Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Group02Package getPackage() {
		return Group02Package.eINSTANCE;
	}

} //Group02FactoryImpl
