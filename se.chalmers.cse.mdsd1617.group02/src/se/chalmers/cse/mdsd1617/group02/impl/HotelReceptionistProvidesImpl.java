/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import se.chalmers.cse.mdsd1617.group02.AvailableRoomDTO;
import se.chalmers.cse.mdsd1617.group02.BookingDTO;
import se.chalmers.cse.mdsd1617.group02.BookingRoomReservationsDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckInDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.CheckOutDTO;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBooking;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.OccupiedRoomDTO;
import se.chalmers.cse.mdsd1617.group02.Group02Package;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.IHotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelReceptionistProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.util.DateUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelReceptionistProvidesImpl#getRoomManager <em>Room Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelReceptionistProvidesImpl#getBookingManager <em>Booking Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelReceptionistProvidesImpl extends HotelCustomerProvidesImpl implements HotelReceptionistProvides {
	/**
	 * The cached value of the '{@link #getRoomManager() <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomHotelReceptionistProvides roomManager;

	/**
	 * The cached value of the '{@link #getBookingManager() <em>Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingManager()
	 * @generated
	 * @ordered
	 */
	protected IBookingReceptionistProvides bookingManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelReceptionistProvidesImpl() {
		super();
		setRoomManager(Hotel.INSTANCE.getRoomManager());
		setBookingManager(Hotel.INSTANCE.getBookingManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group02Package.Literals.HOTEL_RECEPTIONIST_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHotelReceptionistProvides getRoomManager() {
		if (roomManager != null && roomManager.eIsProxy()) {
			InternalEObject oldRoomManager = (InternalEObject)roomManager;
			roomManager = (IRoomHotelReceptionistProvides)eResolveProxy(oldRoomManager);
			if (roomManager != oldRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
			}
		}
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHotelReceptionistProvides basicGetRoomManager() {
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomManager(IRoomHotelReceptionistProvides newRoomManager) {
		IRoomHotelReceptionistProvides oldRoomManager = roomManager;
		roomManager = newRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingReceptionistProvides getBookingManager() {
		if (bookingManager != null && bookingManager.eIsProxy()) {
			InternalEObject oldBookingManager = (InternalEObject)bookingManager;
			bookingManager = (IBookingReceptionistProvides)eResolveProxy(oldBookingManager);
			if (bookingManager != oldBookingManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER, oldBookingManager, bookingManager));
			}
		}
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingReceptionistProvides basicGetBookingManager() {
		return bookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingManager(IBookingReceptionistProvides newBookingManager) {
		IBookingReceptionistProvides oldBookingManager = bookingManager;
		bookingManager = newBookingManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER, oldBookingManager, bookingManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns all available physical rooms for all room types that are reserved by the given booking ID.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<AvailableRoomDTO> getAvailableRooms(int bookingId) {
		EList<IRoomType> roomTypes = getBookingManager().getReservedRoomTypes(bookingId);
		EList<AvailableRoomDTO> availableRooms = new BasicEList<>();
		for (IRoomType roomType : roomTypes) {
			EList<IRoom> freeRoomsOfType = getRoomManager().getFreeRoomsOfType(roomType.getName());
			for (IRoom room : freeRoomsOfType) {
				AvailableRoomDTO dto = new AvailableRoomDTOImpl();
				dto.setRoomNumber(room.getNumber());
				dto.setRoomType(roomType.getName());
				availableRooms.add(dto);
			}
		}
		return availableRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Marks the given rooms as occupied and links them to the given booking
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingid, EList<Integer> roomNumbers) {
		return getBookingManager().checkInBooking(bookingid, roomNumbers);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Lists all occupied rooms for a given date
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<OccupiedRoomDTO> listOccupiedRooms(String date) {
		Date dDate = DateUtil.parseDate(date);
		
		if (dDate == null) {
			return new BasicEList<>();
		}
		
		return getBookingManager().listOccupiedRooms(dDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Marks the rooms in the given booking as free during the booking's time interval and marks the booking as cancelled
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingId) {
		return getBookingManager().cancelBooking(bookingId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Display a list of all check ins made during the specified time interval
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckInDTO> listCheckIns(String startDate, String endDate) {
		return getBookingManager().listCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Display a list of all check outs made during the specified time interval
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CheckOutDTO> listCheckOuts(String startDate, String endDate) {
		return getBookingManager().listCheckOuts(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Update the time period of a booking.
	 * @return false if any of the dates are invalid dates, or if startDate is before endDate.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateTimePeriod(int bookingId, String startDate, String endDate) {
		Date sDate = DateUtil.parseDate(startDate);
		Date eDate = DateUtil.parseDate(endDate);
		if (sDate == null || eDate == null || sDate.compareTo(eDate) > 0) {
			return false;
		}
		return getBookingManager().updateTimePeriod(bookingId, sDate, eDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes a reserved room type from a booking.
	 * @return false if no booking with the given id exists, if no room type with the given name exists, or if
	 *         the booking has no room reservations of the given room type.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int bookingId, String roomType) {
		return getBookingManager().removeRoom(bookingId, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds an extra cost with a given description and price to a given room
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCost(int bookingId, int roomNumber, String description, double price) {
		return getBookingManager().addExtraCost(bookingId, roomNumber, description, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Add a room of a given type to a given booking
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int bookingId, String roomType) {
		return getBookingManager().addRoom(bookingId, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Shows all confirmed bookings
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<BookingDTO> listBookings() {
		
		// Get list of confirmed bookings
		EList<IBooking> bookings = getBookingManager().getConfirmedBookings();
		EList<BookingDTO> bookingDtos = new BasicEList<>();
		
		// Creates a BookingDTO for each booking in list of confirmed bookings and adds it to list of BookingDTO's
		for (IBooking booking : bookings) {
			
			BookingDTO bookingDto = new BookingDTOImpl();
			EList<BookingRoomReservationsDTO> resDtos = reservationsToDtos(booking.getIReservations());

			bookingDto.setBookingId(booking.getId());
			bookingDto.setReservations(resDtos);
			bookingDto.setStartDate(DateUtil.dateToString(booking.getStartDate()));
			bookingDto.setEndDate(DateUtil.dateToString(booking.getEndDate()));
			
			bookingDtos.add(bookingDto);
		}
		return bookingDtos;
	}
	
	/**
	 * Converts a list of reservations to a list of BookingRoomReservationsDTO
	 * 
	 * @param reservations a list of reservations
	 * @return a list of BookingRoomReservationsDTO
	 */
	private EList<BookingRoomReservationsDTO> reservationsToDtos(EList<IRoomReservation> reservations) {
		
		EList<BookingRoomReservationsDTO> resDtos = new BasicEList<>();
		Map<IRoomType, Integer> roomsPerType = getNumberOfRoomsPerType(reservations);
		
		// Creates a BookingRoomReservationDTO for each entry in the map and adds it to the list
		for (Entry<IRoomType, Integer> entry : roomsPerType.entrySet()) {
			BookingRoomReservationsDTO resDto = new BookingRoomReservationsDTOImpl();
			resDto.setRoomType(entry.getKey().getName());
			resDto.setCount(entry.getValue());
			resDtos.add(resDto);
		}
		
		return resDtos;
	}
	
	/**
	 * Calculates the number of rooms per room type in a list of reservations
	 * 
	 * @param reservations the list of reservations
	 * @return a map containing room types as keys and the number of rooms of that type as value
	 */
	private Map<IRoomType, Integer> getNumberOfRoomsPerType(EList<IRoomReservation> reservations) {
		
		Map<IRoomType, Integer> roomsPerType = new HashMap<>();
		
		// Increments number of rooms of type if type already exists in map.
		// Otherwise adds the type to the map and sets number of rooms to 1.
		for (IRoomReservation res : reservations) {
			IRoomType roomType = res.getRoomType();
			if (roomsPerType.containsKey(roomType)) {
				int numberOfRooms = roomsPerType.get(roomType);
				roomsPerType.put(roomType, numberOfRooms+1);
			} else {
				roomsPerType.put(roomType, 1);
			}
		}
		
		return roomsPerType;
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER:
				if (resolve) return getRoomManager();
				return basicGetRoomManager();
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER:
				if (resolve) return getBookingManager();
				return basicGetBookingManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomHotelReceptionistProvides)newValue);
				return;
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER:
				setBookingManager((IBookingReceptionistProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomHotelReceptionistProvides)null);
				return;
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER:
				setBookingManager((IBookingReceptionistProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__ROOM_MANAGER:
				return roomManager != null;
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES__BOOKING_MANAGER:
				return bookingManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IHotelReceptionistProvides.class) {
			switch (baseOperationID) {
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___GET_AVAILABLE_ROOMS__INT: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___GET_AVAILABLE_ROOMS__INT;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_STRING_STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_STRING_STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING;
				case Group02Package.IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS: return Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___GET_AVAILABLE_ROOMS__INT:
				return getAvailableRooms((Integer)arguments.get(0));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT_ELIST:
				return checkInBooking((Integer)arguments.get(0), (EList<Integer>)arguments.get(1));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED_ROOMS__STRING:
				return listOccupiedRooms((String)arguments.get(0));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING:
				return listCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___UPDATE_TIME_PERIOD__INT_STRING_STRING:
				return updateTimePeriod((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM__INT_STRING:
				return removeRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST__INT_INT_STRING_DOUBLE:
				return addExtraCost((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS:
				return listBookings();
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelReceptionistProvidesImpl
