/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group02.BookingPackage.IBookingCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.BookingPackage.IRoomReservation;
import se.chalmers.cse.mdsd1617.group02.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group02.Group02Package;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoom;
import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomHotelCustomerProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.util.DateUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelCustomerProvidesImpl#getHotelBookingManager <em>Hotel Booking Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelCustomerProvidesImpl#getHotelRoomManager <em>Hotel Room Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements HotelCustomerProvides {
	
	protected double currentPrice;
	protected Map<Integer, Double> roomPrices = new HashMap<>();
	
	protected se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bankComponent;
	
	/**
	 * The cached value of the '{@link #getHotelBookingManager() <em>Hotel Booking Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHotelBookingManager()
	 * @generated
	 * @ordered
	 */
	protected IBookingCustomerProvides hotelBookingManager;

	/**
	 * The cached value of the '{@link #getHotelRoomManager() <em>Hotel Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHotelRoomManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomHotelCustomerProvides hotelRoomManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelCustomerProvidesImpl() {
		super();
		setHotelRoomManager(Hotel.INSTANCE.getRoomManager());
		setHotelBookingManager(Hotel.INSTANCE.getBookingManager());
		try {
			bankComponent = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group02Package.Literals.HOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingCustomerProvides getHotelBookingManager() {
		if (hotelBookingManager != null && hotelBookingManager.eIsProxy()) {
			InternalEObject oldHotelBookingManager = (InternalEObject)hotelBookingManager;
			hotelBookingManager = (IBookingCustomerProvides)eResolveProxy(oldHotelBookingManager);
			if (hotelBookingManager != oldHotelBookingManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER, oldHotelBookingManager, hotelBookingManager));
			}
		}
		return hotelBookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingCustomerProvides basicGetHotelBookingManager() {
		return hotelBookingManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHotelBookingManager(IBookingCustomerProvides newHotelBookingManager) {
		IBookingCustomerProvides oldHotelBookingManager = hotelBookingManager;
		hotelBookingManager = newHotelBookingManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER, oldHotelBookingManager, hotelBookingManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHotelCustomerProvides getHotelRoomManager() {
		if (hotelRoomManager != null && hotelRoomManager.eIsProxy()) {
			InternalEObject oldHotelRoomManager = (InternalEObject)hotelRoomManager;
			hotelRoomManager = (IRoomHotelCustomerProvides)eResolveProxy(oldHotelRoomManager);
			if (hotelRoomManager != oldHotelRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER, oldHotelRoomManager, hotelRoomManager));
			}
		}
		return hotelRoomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHotelCustomerProvides basicGetHotelRoomManager() {
		return hotelRoomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHotelRoomManager(IRoomHotelCustomerProvides newHotelRoomManager) {
		IRoomHotelCustomerProvides oldHotelRoomManager = hotelRoomManager;
		hotelRoomManager = newHotelRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER, oldHotelRoomManager, hotelRoomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Calculates the number of free rooms of a given type during a given time interval
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		Date dStartDate = DateUtil.parseDate(startDate);
		Date dEndDate = DateUtil.parseDate(endDate);
		
		if (dEndDate.before(dStartDate)) {
			return new BasicEList<>();
		}
		
		EList<IRoom> allRooms = getHotelRoomManager().getAllRooms();
		EList<IRoom> filteredRooms = filterRooms(allRooms, numBeds);
		
		Map<IRoomType, Integer> allRoomsPerType = getNumberOfRoomsPerType(filteredRooms);
		
		EList<IRoomReservation> reservations = getHotelBookingManager().getReservations(dStartDate, dEndDate);
		
		Map<IRoomType, Integer> allReservationsPerType = getNumberOfReservationsPerType(reservations);
		
		Map<IRoomType, Integer> freeRoomsPerType = getNumberOfFreeRoomsPerType(allRoomsPerType, allReservationsPerType);
		
		EList<FreeRoomTypesDTO> freeRoomsDTOs = new BasicEList<>();
		
		for (Entry<IRoomType, Integer> entry : freeRoomsPerType.entrySet()) {
			FreeRoomTypesDTO dto = new FreeRoomTypesDTOImpl();
			dto.setRoomTypeDescription(entry.getKey().getName());
			dto.setNumBeds(entry.getKey().getNumberOfBeds());
			dto.setPricePerNight(entry.getKey().getPrice());
			dto.setNumFreeRooms(entry.getValue());
			freeRoomsDTOs.add(dto);
		}
		
		return freeRoomsDTOs;
	}
	
	/**
	 * Filtering a list of rooms with respect to a minimum number of rooms
	 * 
	 * @param rooms
	 * @param numberOfBeds
	 * @return
	 */
	private EList<IRoom> filterRooms(EList<IRoom> rooms, int numberOfBeds) {
		EList<IRoom> filteredRooms = new BasicEList<>();
		for (IRoom room : rooms) {
			if (room.getType().getNumberOfBeds() >= numberOfBeds) {
				filteredRooms.add(room);
			}
		}
		return filteredRooms;
	}
	
	/**
	 * Calculates the number of rooms of each type given a list of rooms
	 * 
	 * @param rooms
	 * @param numberOfBeds
	 * @return
	 */
	private Map<IRoomType, Integer> getNumberOfRoomsPerType (EList<IRoom> rooms) {
		
		Map<IRoomType, Integer> roomsPerType = new HashMap<>();
		
		for (IRoom room : rooms) {
			IRoomType roomType = room.getType();
			if (roomsPerType.containsKey(roomType)) {
				int numberOfRooms = roomsPerType.get(roomType);
				roomsPerType.put(roomType, numberOfRooms+1);
			} else {
				roomsPerType.put(roomType, 1);
			}
		}
		
		return roomsPerType;
	}
	
	
	/**
	 * Calculates the number of rooms per room type in a list of reservations
	 * 
	 * @param reservations the list of reservations
	 * @return a map containing room types as keys and the number of rooms of that type as value
	 */
	private Map<IRoomType, Integer> getNumberOfReservationsPerType(EList<IRoomReservation> reservations) {
		
		Map<IRoomType, Integer> resPerType = new HashMap<>();
		
		// Increments number of rooms of type if type already exists in map.
		// Otherwise adds the type to the map and sets number of rooms to 1.
		for (IRoomReservation res : reservations) {
			IRoomType roomType = res.getRoomType();
			if (resPerType.containsKey(roomType)) {
				int numberOfRooms = resPerType.get(roomType);
				resPerType.put(roomType, numberOfRooms+1);
			} else {
				resPerType.put(roomType, 1);
			}
		}
		
		return resPerType;
	}
	
	/**
	 * Calculates the number of free rooms of each type given a list of all rooms and a list of reserved rooms
	 * 
	 * @param allRooms
	 * @param reservedRooms
	 * @return
	 */
	private Map<IRoomType, Integer> getNumberOfFreeRoomsPerType(Map<IRoomType, Integer> allRooms, Map<IRoomType, Integer> reservedRooms) {
		
		Map<IRoomType, Integer> freeRoomsPerType = new HashMap<>();
		
		for (Entry<IRoomType, Integer> allRoomsEntry : allRooms.entrySet()) {
			int numberOfRooms = allRoomsEntry.getValue();
			for (Entry<IRoomType, Integer> resRoomsEntry : reservedRooms.entrySet()) {
				if (resRoomsEntry.getKey() == allRoomsEntry.getKey()) {
					numberOfRooms--;
				}
			}
			freeRoomsPerType.put(allRoomsEntry.getKey(), numberOfRooms);
		}
		
		return freeRoomsPerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Initiates a booking
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return getHotelBookingManager().initiateBooking(firstName, lastName, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room to a not confirmed booking
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return getHotelBookingManager().addRoomBeforeConfirmation(bookingID, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Confirms a booking
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return getHotelBookingManager().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Initiates a check out of a booking, returning the price
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		double price = getHotelBookingManager().checkOutBooking(bookingID);
		currentPrice = price;
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Pays for a booking during checkout using the bank component
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			return bankComponent.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, currentPrice);
		} catch (SOAPException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Checks in a room
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		return getHotelBookingManager().checkInRoom(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Initiates a check out of a room, returning the price
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingID) {
		double price = getHotelBookingManager().checkOutRoom(bookingID, roomNumber);
		
		if (price != -1) {
			roomPrices.put(roomNumber, price);
		}
		
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Pays for a room during checkout using the bank component
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		if (roomPrices.containsKey(roomNumber)) {
			double price = roomPrices.get(roomNumber);
			try {
				return bankComponent.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, price);
			} catch (SOAPException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER:
				if (resolve) return getHotelBookingManager();
				return basicGetHotelBookingManager();
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER:
				if (resolve) return getHotelRoomManager();
				return basicGetHotelRoomManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER:
				setHotelBookingManager((IBookingCustomerProvides)newValue);
				return;
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER:
				setHotelRoomManager((IRoomHotelCustomerProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER:
				setHotelBookingManager((IBookingCustomerProvides)null);
				return;
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER:
				setHotelRoomManager((IRoomHotelCustomerProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_BOOKING_MANAGER:
				return hotelBookingManager != null;
			case Group02Package.HOTEL_CUSTOMER_PROVIDES__HOTEL_ROOM_MANAGER:
				return hotelRoomManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case Group02Package.HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelCustomerProvidesImpl
