/**
 */
package se.chalmers.cse.mdsd1617.group02.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group02.Group02Package;
import se.chalmers.cse.mdsd1617.group02.Hotel;
import se.chalmers.cse.mdsd1617.group02.HotelAdministratorProvides;

import se.chalmers.cse.mdsd1617.group02.RoomPackage.IRoomAdministrationProvides;
import se.chalmers.cse.mdsd1617.group02.RoomTypeDTO;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomType;
import se.chalmers.cse.mdsd1617.group02.RoomTypePackage.IRoomTypeAdministratorProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelAdministratorProvidesImpl#getRoomTypeManager <em>Room Type Manager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group02.impl.HotelAdministratorProvidesImpl#getRoomManager <em>Room Manager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelAdministratorProvidesImpl extends MinimalEObjectImpl.Container implements HotelAdministratorProvides {
	/**
	 * The cached value of the '{@link #getRoomTypeManager() <em>Room Type Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeAdministratorProvides roomTypeManager;

	/**
	 * The cached value of the '{@link #getRoomManager() <em>Room Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomManager()
	 * @generated
	 * @ordered
	 */
	protected IRoomAdministrationProvides roomManager;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelAdministratorProvidesImpl() {
		super();
		setRoomTypeManager(Hotel.INSTANCE.getRoomTypeManager());
		setRoomManager(Hotel.INSTANCE.getRoomManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group02Package.Literals.HOTEL_ADMINISTRATOR_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeAdministratorProvides getRoomTypeManager() {
		if (roomTypeManager != null && roomTypeManager.eIsProxy()) {
			InternalEObject oldRoomTypeManager = (InternalEObject)roomTypeManager;
			roomTypeManager = (IRoomTypeAdministratorProvides)eResolveProxy(oldRoomTypeManager);
			if (roomTypeManager != oldRoomTypeManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
			}
		}
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeAdministratorProvides basicGetRoomTypeManager() {
		return roomTypeManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeManager(IRoomTypeAdministratorProvides newRoomTypeManager) {
		IRoomTypeAdministratorProvides oldRoomTypeManager = roomTypeManager;
		roomTypeManager = newRoomTypeManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER, oldRoomTypeManager, roomTypeManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomAdministrationProvides getRoomManager() {
		if (roomManager != null && roomManager.eIsProxy()) {
			InternalEObject oldRoomManager = (InternalEObject)roomManager;
			roomManager = (IRoomAdministrationProvides)eResolveProxy(oldRoomManager);
			if (roomManager != oldRoomManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
			}
		}
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomAdministrationProvides basicGetRoomManager() {
		return roomManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomManager(IRoomAdministrationProvides newRoomManager) {
		IRoomAdministrationProvides oldRoomManager = roomManager;
		roomManager = newRoomManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER, oldRoomManager, roomManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes the room type with the given name from the system.
	 * @return false if no room type with the given name exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String name) {
		if (getRoomManager().roomOfRoomTypeExists(name)) {
			return false;
		} else {
			return getRoomTypeManager().removeRoomType(name);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns a list of all room types currently added to the system.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomTypeDTO> getAllRoomTypes() {
		EList<IRoomType> roomTypes = getRoomTypeManager().getAllRoomTypes();
		EList<RoomTypeDTO> roomTypeDTOs = new BasicEList<>();
		for (IRoomType roomType : roomTypes) {
			roomTypeDTOs.add(new RoomTypeDTOImpl(roomType));
		}
		return roomTypeDTOs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room type to the system.
	 * @return false if a room type with the given name already exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, int numberOfBeds, double price, EList<String> features) {
		return getRoomTypeManager().addRoomType(name, numberOfBeds, price, features);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Adds a room with the given number and type to the system.
	 * @return false if a room with the given number already exists or if the room type with 
	 *         the given name doesn't exist.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomNumber, String roomType) {
		return getRoomManager().addRoom(roomNumber, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Blocks a room with a given room number.
	 * @return false if no room with the given room number exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNumber) {
		return getRoomManager().blockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Unblocks a room with a given room number.
	 * @return false if no room with the given room number exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNumber) {
		return getRoomManager().unblockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Changes the room type of a room.
	 * @return false if no room with the given room number exists, or if no room type with the given
	 *         name exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean changeRoomTypeOfRoom(int roomNumber, String roomType) {
		return getRoomManager().changeRoomTypeOfRoom(roomNumber, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Removes a room from the system.
	 * @return false if no room with the given room number exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		return getRoomManager().removeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Updates a room type with new attributes.
	 * @return false if no room type with the given name exists.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String name, String newName, int numberOfBeds, double price, EList<String> features) {
		return getRoomTypeManager().updateRoomType(name, newName, numberOfBeds, price, features);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER:
				if (resolve) return getRoomTypeManager();
				return basicGetRoomTypeManager();
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER:
				if (resolve) return getRoomManager();
				return basicGetRoomManager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeAdministratorProvides)newValue);
				return;
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomAdministrationProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER:
				setRoomTypeManager((IRoomTypeAdministratorProvides)null);
				return;
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER:
				setRoomManager((IRoomAdministrationProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_TYPE_MANAGER:
				return roomTypeManager != null;
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES__ROOM_MANAGER:
				return roomManager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___GET_ALL_ROOM_TYPES:
				return getAllRoomTypes();
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_DOUBLE_ELIST:
				return addRoomType((String)arguments.get(0), (Integer)arguments.get(1), (Double)arguments.get(2), (EList<String>)arguments.get(3));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING:
				return changeRoomTypeOfRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case Group02Package.HOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_DOUBLE_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Double)arguments.get(3), (EList<String>)arguments.get(4));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelAdministratorProvidesImpl
