package se.chalmers.cse.mdsd1617.group02.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Contains static methods for handling dates.
 * @author mats
 */
public class DateUtil {
	public static boolean datesIntersect(Date startDate1, Date endDate1, Date startDate2, Date endDate2) {
		return isBetween(startDate1, startDate2, endDate2) || isBetween(endDate1, startDate2, endDate2)
				   || isBefore(startDate1, startDate2) && isAfter(endDate1, endDate2);
	}
	
	public static boolean isBetween(Date date, Date start, Date end) {
		if (date == null) {
			return false;
		}
		
		return isAfter(date,  start) && isBefore(date, end);
	}
	
	public static long daysBetween(Date date1, Date date2) {
		long diff = date2.getTime() - date1.getTime();
		long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		if (days == 0) {
			return 1;
		} else {
			return days;
		}
	}
	
	public static boolean isAfter(Date date, Date after) {
		if (date == null) {
			return false;
		}
		
		return date.compareTo(after) >= 0;
	}
	
	public static boolean isBefore(Date date, Date before) {
		if (date == null) {
			return false;
		}
		
		return date.compareTo(before) <= 0;
	}
	
	public static Date parseDate(String dateString) {
		DateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
		try {
			Date date = dateformat.parse(dateString);
			return date;
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String dateToString(Date date) {
		String dateString = new SimpleDateFormat("yyyyMMdd").format(date);
		return dateString;
	}
}
